//
//  DrawView.swift
//  
//
//  Created by 陳雨晴 on 2016/6/14.
//
//

import Foundation
import UIKit

class DrawingView: UIView {
    

    override func drawRect(rect: CGRect) {
        // Drawing code
        
        var context:CGContextRef =  UIGraphicsGetCurrentContext();
        
        
        CGContextSetAllowsAntialiasing(context, true) //抗锯齿设置
        
        //畫點
        //CGContextSetLineWidth(context, 50);
        CGContextFillEllipseInRect(context, CGRectMake(75, 75, 50, 50))
        
        CGContextSetLineWidth(context, 5) //畫筆寬度
        
        //畫線
        CGContextMoveToPoint(context, 10, 20);
        CGContextAddLineToPoint(context, 100, 100);
        CGContextStrokePath(context)
        
        //畫圓
        CGContextAddEllipseInRect(context, CGRectMake(50,50,100,100));
        
        CGContextStrokePath(context)
    }
}
