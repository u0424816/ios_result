//
//  FaceIt.swift
//  test_0608
//
//  Created by nkfust03 on 2016/6/8.
//  Copyright © 2016年 u0324004. All rights reserved.
//

import UIKit

class FaceIt: UIView {
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        var skullRadius = min(bounds.size.width , bounds.size.height) / 2
        var skullCenter = CGPoint(x: bounds.midX , y: bounds.midY)
        var skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0, endAngle: CGFloat(2 * M_PI), clockwise: false)
        skull.lineWidth = 5.0
        
        skullRadius = min(bounds.size.width , bounds.size.height) / 2
        skullCenter = CGPoint(x: bounds.midX , y: bounds.midY)
        skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0, endAngle: CGFloat(2 * M_PI), clockwise: false)
        UIColor.blueColor().set()
        skull.fill()
        skull.stroke()
        skullRadius = min(bounds.size.width , bounds.size.height) / 8
        skullCenter = CGPoint(x: bounds.midX - skullRadius - skullRadius/2 , y: bounds.midY - skullRadius)
        skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0, endAngle: CGFloat(2 * M_PI), clockwise: false)
        UIColor.whiteColor().set()
        skull.fill()
        skull.stroke()
        skullCenter = CGPoint(x: bounds.midX - skullRadius , y: bounds.midY - skullRadius)
        skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius/2, startAngle: 0.0, endAngle: CGFloat(2 * M_PI), clockwise: false)
        UIColor.redColor().set()
        skull.fill()
        skull.stroke()
        skullCenter = CGPoint(x: bounds.midX + skullRadius + skullRadius/2 , y: bounds.midY - skullRadius)
        skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0, endAngle: CGFloat(2 * M_PI), clockwise: false)
        skull.stroke()
        UIColor.whiteColor().set()
        skull.fill()
        skullCenter = CGPoint(x: bounds.midX + skullRadius*2 , y: bounds.midY - skullRadius)
        skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius/2, startAngle: 0.0, endAngle: CGFloat(2 * M_PI), clockwise: false)
        UIColor.redColor().set()
        skull.fill()
        skull.stroke()

        
    }
    
}
