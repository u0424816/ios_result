//
//  FaceIt.swift
//  test0608
//
//  Created by 陳昱蓁 on 2016/6/8.
//  Copyright © 2016年 陳昱蓁. All rights reserved.
//

import UIKit

class FaceIt: UIView {

    override func drawRect(rect: CGRect) {
        // Drawing code
        let skullRadius = min(bounds.size.width, bounds.size.height) / 2 //半徑
        let skullCenter = CGPoint(x: bounds.midX, y: bounds.midY) //中點
        let circleRadius = min(bounds.size.width, bounds.size.height) / 8 //半徑
        let circleCenter = CGPoint(x: bounds.midX / 0.9 , y: bounds.midY / 1.1) //中點
        let circle2Center = CGPoint(x: bounds.midX / 1.8 , y: bounds.midY / 1.1) //中點
        let circle3Center = CGPoint(x: bounds.midX / 0.6 , y: bounds.midY / 1.1) //中點
        let circle4Center = CGPoint(x: bounds.midX / 0.72 , y: bounds.midY / 0.93) //中點
        let circle5Center = CGPoint(x: bounds.midX / 1.18 , y: bounds.midY / 0.93) //中點
        
        let skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        skull.lineWidth = 5.0
        UIColor.blueColor().set()
       // skull.stroke()
        
        let line = UIBezierPath()
        line.moveToPoint(CGPointMake(40, 0))
        line.addLineToPoint(CGPointMake(40, 100))
        line.lineWidth = 10.0
        UIColor.redColor().set()
        //line.stroke()
       
        let circle2 = UIBezierPath(arcCenter: circleCenter, radius: circleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        circle2.lineWidth = 5.0
        UIColor.blackColor().set()
        circle2.stroke()
        
        let circle = UIBezierPath(arcCenter: circle2Center, radius: circleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        circle.lineWidth = 5.0
        UIColor.blueColor().set()
        circle.stroke()
        
        let circle3 = UIBezierPath(arcCenter: circle3Center, radius: circleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        circle3.lineWidth = 5.0
        UIColor.redColor().set()
        circle3.stroke()
        
        let circle4 = UIBezierPath(arcCenter: circle4Center, radius: circleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        circle4.lineWidth = 5.0
        UIColor.greenColor().set()
        circle4.stroke()
        
        let circle5 = UIBezierPath(arcCenter: circle5Center, radius: circleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        circle5.lineWidth = 5.0
        UIColor.yellowColor().set()
        circle5.stroke()
        
    }

}
