//
//  Facelt.swift
//  test0324033
//
//  Created by nkfust09 on 2016/6/8.
//  Copyright © 2016年 nkfust09. All rights reserved.
//

import UIKit

class Facelt: UIView {
    
    override func drawRect(rect: CGRect) {
        
        func drawcircle(width: CGFloat, height: CGFloat, midX:CGFloat, midY:CGFloat, start:CGFloat, end:Double, wise: Bool, R:Float, G:Float, B:Float, alpha:Float , line:CGFloat) {
            
            let skullRadius = min(width,height)/2
            let skull = UIBezierPath(arcCenter: CGPoint( x: midX, y: midY), radius: skullRadius , startAngle: start, endAngle:CGFloat(end), clockwise: wise)
            skull.lineWidth = line
            UIColor.init(colorLiteralRed: R, green: G, blue: B, alpha: alpha).setFill()
            skull.stroke()
        }
        func oval(x:CGFloat, y:CGFloat , width: CGFloat, height: CGFloat, R:Float, G:Float, B:Float, alpha:Float, linewidth:CGFloat) {
            
            let oval = UIBezierPath(ovalInRect: CGRect(x: x, y: y, width: width, height: height))
            oval.lineWidth = linewidth
            UIColor.init(colorLiteralRed: R, green: G, blue: B, alpha: alpha).setFill()
            oval.stroke()
            oval.fill()
        }
        func curve(moveX:CGFloat,moveY:CGFloat,endX:CGFloat,endY:CGFloat,cp1X:CGFloat,cp1Y:CGFloat,cp2X:CGFloat,cp2Y:CGFloat, R:Float, G:Float, B:Float, alpha:Float, linewidth:CGFloat ) {
            let head=UIBezierPath()
            head.moveToPoint(CGPointMake(moveX,moveY))
            head.addCurveToPoint(CGPointMake(endX,endY), controlPoint1: CGPointMake(cp1X,cp1Y), controlPoint2: CGPointMake(cp2X,cp2Y))
            head.lineWidth = linewidth
            UIColor.init(colorLiteralRed: R, green: G, blue: B, alpha: alpha).setFill()
            head.stroke()
            head.fill()
        }
        func line(moveX:CGFloat,moveY:CGFloat,endX:CGFloat,endY:CGFloat, R:Float, G:Float, B:Float, alpha:Float, linewidth:CGFloat ) {
            let head=UIBezierPath()
            head.moveToPoint(CGPointMake(moveX,moveY))
            head.addLineToPoint(CGPointMake(endX, endY))
            head.lineWidth = linewidth
            UIColor.init(colorLiteralRed: R, green: G, blue: B, alpha: alpha).setStroke()
            head.stroke()
        }
        func curve1(moveX:CGFloat,moveY:CGFloat,endX:CGFloat,endY:CGFloat,cp1X:CGFloat,cp1Y:CGFloat, linewidth:CGFloat ) {
            let head=UIBezierPath()
            head.moveToPoint(CGPointMake(moveX,moveY))
            head.addQuadCurveToPoint(CGPointMake(endX, endY), controlPoint: CGPointMake(cp1X, cp1Y))
            head.lineWidth = linewidth
            UIColor.init(red: 0, green: 0, blue: 0, alpha: 1).setStroke()
            head.stroke()
        }
        func square(x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat, R:Float, G:Float, B:Float, alpha:Float, linewidth:CGFloat, radius:CGFloat) {
            let x=UIBezierPath(roundedRect: CGRect(x: x, y: y, width: width, height: height),cornerRadius: radius)
            x.lineWidth=linewidth
            UIColor.init(colorLiteralRed: R, green: G, blue: B, alpha: alpha).setFill()
            x.stroke()
            x.fill()
        }
        
        func fivecircle(x:CGFloat, y:CGFloat , width: CGFloat, height: CGFloat,SR:Float,SG:Float,SB:Float, R:Float, G:Float, B:Float, Salpha:Float,alpha: Float, linewidth:CGFloat) {
            let oval = UIBezierPath(ovalInRect: CGRect(x: x, y: y, width: width, height: height))
            oval.lineWidth = linewidth
            UIColor.init(colorLiteralRed: SR, green: SG, blue: SB, alpha: Salpha).setStroke()
            UIColor.init(colorLiteralRed: R, green: G, blue: B, alpha: alpha).setFill()
            oval.stroke()
            oval.fill()
        }
   
        curve(bounds.midX-100, moveY: bounds.midY, endX: bounds.midX+100, endY: bounds.midY, cp1X: bounds.midX-315, cp1Y: bounds.midY-350, cp2X: bounds.midX+315, cp2Y: bounds.midY-350, R: 0, G: 0.6, B: 1, alpha: 1, linewidth: 3.0)//外臉
        curve(bounds.midX-85, moveY: bounds.midY, endX: bounds.midX+85, endY: bounds.midY, cp1X: bounds.midX-285, cp1Y: bounds.midY-275, cp2X: bounds.midX+285, cp2Y: bounds.midY-275, R: 1, G: 1, B: 1, alpha: 1, linewidth: 3.0)//內臉
        
        curve1(bounds.midX-100, moveY: bounds.midY-100, endX: bounds.midX+100, endY: bounds.midY-100, cp1X: bounds.midX, cp1Y: bounds.midY-15, linewidth: 1)//嘴
        line(bounds.midX, moveY: bounds.midY-58, endX: bounds.midX, endY: bounds.midY-160, R: 0, G: 0, B: 0, alpha: 1, linewidth: 1)//鼻中線
        line(bounds.midX-40, moveY: bounds.midY-140, endX: bounds.midX-100, endY: bounds.midY-150, R: 0, G: 0, B: 0, alpha: 1, linewidth: 1)//左上鬍鬚
        line(bounds.midX-40, moveY: bounds.midY-125, endX: bounds.midX-100, endY: bounds.midY-130, R: 0, G: 0, B: 0, alpha: 1, linewidth: 1)//左中鬍鬚
        line(bounds.midX-40, moveY: bounds.midY-110, endX: bounds.midX-100, endY: bounds.midY-108, R: 0, G: 0, B: 0, alpha: 1, linewidth: 1)//左下鬍鬚
        line(bounds.midX+40, moveY: bounds.midY-140, endX: bounds.midX+100, endY: bounds.midY-150, R: 0, G: 0, B: 0, alpha: 1, linewidth: 1)//右上鬍鬚
        line(bounds.midX+40, moveY: bounds.midY-125, endX: bounds.midX+100, endY: bounds.midY-130, R: 0, G: 0, B: 0, alpha: 1, linewidth: 1)//右中鬍鬚
        line(bounds.midX+40, moveY: bounds.midY-110, endX: bounds.midX+100, endY: bounds.midY-108, R: 0, G: 0, B: 0, alpha: 1, linewidth: 1)//右下鬍鬚
        oval( bounds.midX-70, y: bounds.midY-245, width: 70, height: 80,R: 1,G: 1,B: 1,alpha: 1,linewidth: 2.0)//左眼框
        oval( bounds.midX+1, y: bounds.midY-245, width: 70, height: 80,R: 1,G: 1,B: 1,alpha: 1,linewidth: 2.0)//右眼框
        oval(bounds.midX-25, y: bounds.midY-195, width: 15, height: 15,R: 0,G: 0,B: 0,alpha: 1,linewidth: 2.0)//左眼球
        oval(bounds.midX+12, y: bounds.midY-195, width: 15, height: 15,R: 0,G: 0,B: 0,alpha: 1,linewidth: 2.0)//右眼球
        oval(bounds.midX-10, y: bounds.midY-178, width: 20, height: 20,R: 1,G: 0,B: 0,alpha: 1,linewidth: 2.0)//鼻子
        oval(bounds.midX-5, y: bounds.midY-175, width: 5, height: 5,R: 1,G: 1,B: 1,alpha: 1,linewidth: 0.0)//鼻子白點
        
        curve(bounds.midX-100, moveY: bounds.midY+10, endX: bounds.midX-101, endY: bounds.midY+90, cp1X: bounds.midX-130, cp1Y: bounds.midY+40, cp2X: bounds.midX-180, cp2Y: bounds.midY+120, R: 0, G: 0.6, B: 1, alpha: 1, linewidth: 3.0)//左手臂
        curve(bounds.midX+100, moveY: bounds.midY+10, endX: bounds.midX+101, endY: bounds.midY+90, cp1X: bounds.midX+130, cp1Y: bounds.midY+40, cp2X: bounds.midX+180, cp2Y: bounds.midY+120, R: 0, G: 0.6, B: 1, alpha: 1, linewidth: 3.0)//右手臂
        
        oval(bounds.midX-160, y: bounds.midY+80, width: 40, height: 40,R: 1,G: 1,B: 1,alpha: 1,linewidth: 3.0)//左拳頭
        oval(bounds.midX+115, y: bounds.midY+80, width: 40, height: 40,R: 1,G: 1,B: 1,alpha: 1,linewidth: 3.0)//右拳手
        square(bounds.midX-100, y: bounds.midY, width: 200, height: 200, R: 0, G: 0.6, B: 1, alpha: 1, linewidth: 3.0, radius: 0.0)//身體
        square(bounds.midX-115, y: bounds.midY+200, width: 230, height: 25, R: 1, G: 1, B: 1, alpha: 1, linewidth: 3.0, radius: 15.0)//腳
        oval(bounds.midX-90, y: bounds.midY, width: 180, height: 160,R: 1,G: 1,B: 1,alpha: 1,linewidth: 3.0)//身體白
        square(bounds.midX-100, y: bounds.midY, width: 200, height: 10, R: 1, G: 0, B: 0, alpha: 1, linewidth: 3.0, radius: 0.0)//項圈
        oval(bounds.midX-18, y: bounds.midY+5, width: 35, height: 35,R: 1,G: 0.9,B:0,alpha: 1,linewidth: 3.0)//鈴鐺
        oval(bounds.midX-5, y: bounds.midY+20, width: 10, height: 10,R: 0,G: 0,B: 0,alpha: 1,linewidth: 3.0)//鈴鐺黑點
        line(bounds.midX, moveY: bounds.midY+30, endX: bounds.midX, endY: bounds.midY+40, R: 0, G: 0, B: 0, alpha: 1, linewidth: 3.0)//鈴鐺中線
        drawcircle(125 , height:125 , midX: bounds.midX, midY: bounds.midY+80, start: 0, end: M_PI, wise: true, R: 1, G: 1, B: 1, alpha: 1,line: 1.0)//袋子底
        line(bounds.midX-63, moveY: bounds.midY+80, endX: bounds.midX+63, endY: bounds.midY+80, R: 0, G: 0, B: 0, alpha: 1, linewidth: 1.0)//袋子上面的線
        line(bounds.midX, moveY: bounds.midY+200, endX: bounds.midX, endY: bounds.midY+225, R: 0, G: 0, B: 0, alpha: 1, linewidth: 2.0)//腳中間的線
        line(bounds.midX-100, moveY: bounds.midY+11, endX: bounds.midX-100, endY: bounds.midY+60, R: 0, G: 0.6, B: 1, alpha: 1, linewidth: 4.0)//蓋左邊黑線
        line(bounds.midX+100, moveY: bounds.midY+11, endX: bounds.midX+100, endY: bounds.midY+60, R: 0, G: 0.6, B: 1, alpha: 1, linewidth: 4.0)//蓋右邊黑線
        curve(bounds.midX+50, moveY: bounds.midY-170, endX: bounds.midX+50, endY: bounds.midY-170, cp1X: bounds.midX+25, cp1Y: bounds.midY-100, cp2X: bounds.midX+75, cp2Y: bounds.midY-100, R: 0, G: 1, B: 1, alpha: 0.5, linewidth: 0)//淚水滴
        
        
        fivecircle(bounds.midX-50, y: bounds.midY+85, width: 30, height: 30, SR: 0, SG: 0.6, SB: 1, R: 1, G: 1, B: 1, Salpha: 1, alpha: 0, linewidth: 3.0)//青色圓環
        fivecircle(bounds.midX-15, y: bounds.midY+85, width: 30, height: 30, SR: 0, SG: 0, SB: 0, R: 1, G: 1, B: 1, Salpha: 1, alpha: 0, linewidth: 3.0)//黑色圓環
        fivecircle(bounds.midX+20, y: bounds.midY+85, width: 30, height: 30, SR: 1, SG: 0, SB: 0, R: 1, G: 1, B: 1, Salpha: 1, alpha: 0, linewidth: 3.0)//紅色圓環
        fivecircle(bounds.midX-33, y: bounds.midY+100, width: 30, height: 30, SR: 1, SG: 0.9, SB: 0, R: 1, G: 1, B: 1, Salpha: 1, alpha: 0, linewidth: 3.0)//黃色圓環
        fivecircle(bounds.midX+3, y: bounds.midY+100, width: 30, height: 30, SR: 0, SG: 1, SB: 0, R: 1, G: 1, B: 1, Salpha: 1, alpha: 0, linewidth: 3.0)//綠色圓環
        
        fivecircle(bounds.midX-15, y: bounds.midY+45, width: 30, height: 30, SR: 0, SG: 0, SB: 0, R: 0, G: 0, B: 0, Salpha: 1, alpha: 0, linewidth: 2.0)//綠色圓環
        fivecircle(bounds.midX-6, y: bounds.midY+50, width: 5, height: 5, SR: 0, SG: 0, SB: 0, R: 0, G: 0, B: 0, Salpha: 1, alpha: 1, linewidth: 2.0)//綠色圓環
        fivecircle(bounds.midX+2, y: bounds.midY+50, width: 5, height: 5, SR: 0, SG: 0, SB: 0, R: 0, G: 0, B: 0, Salpha: 1, alpha: 1, linewidth: 2.0)//綠色圓環
    }
}
