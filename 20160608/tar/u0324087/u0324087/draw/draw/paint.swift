//
//  paint.swift
//  draw
//
//  Created by 凃翰穎 on 2016/6/8.
//  Copyright © 2016年 凃翰穎. All rights reserved.
//

import UIKit

class paint: UIView{
    var doraemon = UIColor(red: 0.0/255.0, green: 120.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    let tear = UIColor(red: 40.0/255.0, green: 245.0/255.0, blue: 240.0/255.0, alpha: 1.0)
    
    func circle(Px:Double,Py:Double,r:Double,width:Double,lineColor:UIColor,fillColor:UIColor,fill:BooleanType){
        let located = CGPoint(x: Px, y: Py)
        let radius = CGFloat(r)
        let bz = UIBezierPath(arcCenter: located, radius: radius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        
        bz.lineWidth = CGFloat(width)
        if fill{
            fillColor.setFill()
            bz.fill()
        }
        lineColor.set()
        bz.stroke()    }
    func arc(Px:Double,Py:Double,r:Double,width:Double,lineColor:UIColor,fillColor:UIColor,startDegree:Double,endDegree:Double,closePath:BooleanType){
        let located = CGPoint(x: Px, y: Py)
        let radius = CGFloat(r)
        let bz = UIBezierPath(arcCenter: located, radius: radius, startAngle: CGFloat(startDegree * M_PI / 180.0), endAngle: CGFloat(endDegree * M_PI / 180.0), clockwise: true)
        
        bz.lineWidth = CGFloat(width)
        if(closePath){
            bz.closePath()
        }
        lineColor.set()
        fillColor.setFill()
        bz.fill()
        bz.stroke()
    }
    func rect(Px:Double,Py:Double,rectWidth:Double,rectHeight:Double,width:Double,lineColor:UIColor,fillColor:UIColor,arc:BooleanType){
        var bz:UIBezierPath
        if(arc){
            bz = UIBezierPath(ovalInRect: CGRectMake(CGFloat(Px), CGFloat(Py), CGFloat(rectWidth), CGFloat(rectHeight)))
        }else{
            let rectsize = CGRect(x: Px,y: Py ,width: rectWidth ,height: rectHeight)
            bz = UIBezierPath(rect: rectsize)
        }
        bz.lineWidth = CGFloat(width)
        lineColor.set()
        fillColor.setFill()
        bz.fill()
        bz.stroke()
    }
    func line(startX:Double,startY:Double,endX:Double,endY:Double,width:Double,lineColor:UIColor){
        let start = CGPoint(x: startX, y: startY)
        let end = CGPoint(x: endX, y: endY)
        let bz = UIBezierPath()
        bz.moveToPoint(start)
        bz.addLineToPoint(end)
        bz.lineWidth = CGFloat(width)
        bz.closePath()
        lineColor.set()
        bz.stroke()
    }
    func quadrilateral(firstX:Double,firstY:Double,secondX:Double,secondY:Double,thirdX:Double,thirdY:Double,fourthX:Double,fourthY:Double,width:Double,lineColor:UIColor,fillColor:UIColor){
        let first = CGPoint(x: firstX, y: firstY)
        let second = CGPoint(x: secondX, y: secondY)
        let third = CGPoint(x: thirdX, y: thirdY)
        let fourth = CGPoint(x: fourthX, y: fourthY)
        let bz = UIBezierPath()
        bz.moveToPoint(first)
        bz.addLineToPoint(second)
        bz.addLineToPoint(third)
        bz.addLineToPoint(fourth)
        bz.closePath()
        bz.lineWidth = CGFloat(width)
        lineColor.set()
        fillColor.setFill()
        bz.fill()
        bz.stroke()
    }
    func tearCurve(){
        let bz = UIBezierPath()
        bz.moveToPoint(CGPoint(x: 170, y: 130))
        bz.addQuadCurveToPoint(CGPoint(x: 170,y:160), controlPoint: CGPoint(x: 165, y: 158))
        bz.addQuadCurveToPoint(CGPoint(x: 170,y:130), controlPoint: CGPoint(x: 175, y: 158))
        UIColor.blackColor().set()
        tear.setFill()
        bz.fill()
        bz.stroke()
    }
    func armCurve(){
        let bz = UIBezierPath()
        bz.moveToPoint(CGPoint(x: 120, y: 300))
        bz.addQuadCurveToPoint(CGPoint(x: 110,y:320), controlPoint: CGPoint(x: 110, y: 320))
        bz.addQuadCurveToPoint(CGPoint(x: 30,y:230), controlPoint: CGPoint(x: 70, y: 325))
        bz.addQuadCurveToPoint(CGPoint(x: 50,y:210), controlPoint: CGPoint(x: 50, y: 210))
        bz.addQuadCurveToPoint(CGPoint(x: 120,y:300), controlPoint: CGPoint(x: 120, y: 300))
        UIColor.blackColor().set()
        doraemon.setFill()
        bz.fill()
        bz.stroke()
    }
    var change = true
    func drawDoraemon(){
        if change{
            doraemon = UIColor(red: 0.0/255.0, green: 120.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        }else{
            doraemon = UIColor(red: 250.0/255.0, green: 120.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        }
        change = !change
        rect(0.0, Py: 0.0, rectWidth: Double(bounds.size.width), rectHeight: Double(bounds.size.height), width: 0, lineColor: UIColor.whiteColor(), fillColor: UIColor.whiteColor(), arc: false)
        rect(110,Py: 360,rectWidth: 30,rectHeight: 40,width: 2,lineColor: UIColor.blackColor(),fillColor: doraemon,arc: false)//left-leg
        rect(160,Py: 360,rectWidth: 30,rectHeight: 40,width: 2,lineColor: UIColor.blackColor(),fillColor: doraemon,arc: false)//right-leg
        rect(95,Py: 390,rectWidth: 50,rectHeight: 25,width: 2,lineColor: UIColor.blackColor(),fillColor: UIColor.whiteColor(),arc: true)//left-foot
        rect(155,Py: 390,rectWidth: 50,rectHeight: 25,width: 2,lineColor: UIColor.blackColor(),fillColor: UIColor.whiteColor(),arc: true)//right-foot
        quadrilateral(120, firstY: 300, secondX: 110, secondY: 320, thirdX: 30, thirdY: 230, fourthX: 50, fourthY: 210, width: 2, lineColor: UIColor.blackColor(), fillColor: doraemon)//left-arm
        quadrilateral(180, firstY: 300, secondX: 190, secondY: 320, thirdX: 270, thirdY: 230, fourthX: 250, fourthY: 210, width: 2, lineColor: UIColor.blackColor(), fillColor: doraemon)//right-arm
        circle( 40,  Py: 220, r: 20, width: 2, lineColor: UIColor.blackColor(), fillColor: UIColor.whiteColor(),fill: true)//left-hand
        circle( 260,  Py: 220, r: 20, width: 2, lineColor: UIColor.blackColor(), fillColor: UIColor.whiteColor(),fill: true)//right-hand
        circle( 150,  Py: 310, r: 80, width: 2, lineColor: UIColor.blackColor(), fillColor: doraemon,fill: true)//body-blue
        circle( 150,  Py: 310, r: 60, width: 2, lineColor: UIColor.blackColor(), fillColor: UIColor.whiteColor(),fill: true)//body-white
        arc( 150,  Py: 390, r: 10, width: 2, lineColor: UIColor.blackColor(), fillColor: UIColor.whiteColor(), startDegree: 180, endDegree: 360,closePath: false)//body-buttom-white
        line(140, startY: 390, endX: 160, endY: 390, width: 2, lineColor: UIColor.whiteColor())
        arc(150, Py: 310, r: 50, width: 2, lineColor: UIColor.blackColor(), fillColor: UIColor.whiteColor(), startDegree: 0, endDegree: 180,closePath: true)//bag
        circle( 150,  Py: 150, r: 100, width: 2, lineColor: UIColor.blackColor(), fillColor: doraemon,fill: true)//head-blue
        circle( 150,  Py: 170, r: 80, width: 2, lineColor: UIColor.blackColor(), fillColor: UIColor.whiteColor(),fill: true)//head-white
        rect(100,Py: 240,rectWidth:100,rectHeight: 10,width: 2,lineColor: UIColor.blackColor(),fillColor: UIColor.redColor(),arc: true)//leck
        circle( 150,  Py: 250, r: 10, width: 2,lineColor:UIColor.blackColor() ,fillColor: UIColor.yellowColor(),fill: true)//bell
        circle( 125,  Py: 100, r: 25, width: 2,lineColor:UIColor.blackColor() ,fillColor: UIColor.whiteColor(),fill: true)//eyes-left-white
        circle( 175,  Py: 100, r: 25, width: 2,lineColor: UIColor.blackColor(),fillColor: UIColor.whiteColor(),fill: true)//eyes-right-white
        circle( 130,  Py: 110, r: 8, width: 7,lineColor: UIColor.blackColor(),fillColor: UIColor.whiteColor(),fill: true)//eyes-left-black
        circle( 170,  Py: 110, r: 8, width: 7,lineColor: UIColor.blackColor(),fillColor: UIColor.whiteColor(),fill: true)//eyes-right-black
        arc(150, Py: 130, r: 80, width: 2, lineColor: UIColor.blackColor(), fillColor: UIColor.whiteColor(), startDegree: 25, endDegree: 155,closePath: false)//mouse
        circle( 150,  Py: 130, r: 15, width: 2,lineColor: UIColor.blackColor(),fillColor: UIColor.redColor(),fill: true)//nose
        circle( 145,  Py: 125, r: 5, width: 2,lineColor: UIColor.whiteColor(),fillColor: UIColor.whiteColor(),fill: true)//nose-light
        line(130, startY: 150, endX: 90, endY: 130,width: 1, lineColor: UIColor.blackColor())//beard-lelf-1
        line(170, startY: 150, endX: 210, endY: 130,width: 1, lineColor: UIColor.blackColor())//beard-right-1
        line(130, startY: 160, endX: 90, endY: 160,width: 1, lineColor: UIColor.blackColor())//beard-lelf-2
        line(170, startY: 160, endX: 210, endY: 160,width: 1, lineColor: UIColor.blackColor())//beard-right-2
        line(130, startY: 170, endX: 90, endY: 190,width: 1, lineColor: UIColor.blackColor())//beard-lelf-3
        line(170, startY: 170, endX: 210, endY: 190,width: 1, lineColor: UIColor.blackColor())//beard-right-3
        line(150, startY: 145, endX: 150, endY: 210,width: 1, lineColor: UIColor.blackColor())//beard-center
        circle(125, Py: 330, r: 10, width: 2, lineColor: UIColor.blueColor(), fillColor: UIColor.whiteColor(),fill: false)
        circle(150, Py: 330, r: 10, width: 2, lineColor: UIColor.blackColor(), fillColor: UIColor.whiteColor(),fill: false)
        circle(175, Py: 330, r: 10, width: 2, lineColor: UIColor.redColor(), fillColor: UIColor.whiteColor(),fill: false)
        circle(138, Py: 340, r: 10, width: 2, lineColor: UIColor.yellowColor(), fillColor: UIColor.whiteColor(),fill: false)
        circle(162, Py: 340, r: 10, width: 2, lineColor: UIColor.greenColor(), fillColor: UIColor.whiteColor(),fill: false)
        circle( 150,  Py: 285, r: 26, width: 1,lineColor: UIColor.blackColor(),fillColor: UIColor.whiteColor(),fill: true)
        circle( 162,  Py: 277, r: 10, width: 1,lineColor: UIColor.blackColor(),fillColor: UIColor.whiteColor(),fill: true)
        circle( 138,  Py: 277, r: 10, width: 1,lineColor: UIColor.blackColor(),fillColor: UIColor.whiteColor(),fill: true)
    }
    override func drawRect(arect: CGRect) {
        drawDoraemon()
        tearCurve()
    }
    
    
}