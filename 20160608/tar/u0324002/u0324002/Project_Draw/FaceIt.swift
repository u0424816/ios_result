//
//  FaceIt.swift
//  Project_Draw
//
//  Created by nkfust02 on 2016/6/8.
//  Copyright © 2016年 Dien-Ju, Lin. All rights reserved.
//

import UIKit

class FaceIt: UIView {

	
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
	
    override func drawRect(rect: CGRect) {
        // Drawing code
		let skullRadius = min(bounds.size.width, bounds.size.height) / 2
		let skullCenter = CGPoint(x: bounds.midX, y: bounds.midY)
		
		let skull = UIBezierPath(arcCenter: skullCenter,
		                         radius: skullRadius,
		                         startAngle: 0,
		                         endAngle: CGFloat(M_PI * 2),
		                         clockwise: false)
		UIColor.blackColor().set()
		skull.lineWidth = 1.0
		UIColor.yellowColor().setFill()
		skull.fill()
		skull.stroke()
		
		
		var mouth = UIBezierPath(arcCenter: CGPoint(x: bounds.midX, y: bounds.midY - skullRadius / 12 * 9),
		                         radius: skullRadius / 2 * 3,
		                         startAngle: CGFloat(M_PI * 2 / 6),
		                         endAngle: CGFloat(M_PI * 2 / 36 * 12),
		                         clockwise: true)
		UIColor.blackColor().set()
		mouth.lineWidth = 3.0
		mouth.stroke()
		
		var eyes = UIBezierPath(arcCenter: CGPoint(x: bounds.midX - 70, y: bounds.midY - skullRadius / 3),
		                        radius: 10,
		                        startAngle: 0,
		                        endAngle: CGFloat(M_PI * 2),
		                        clockwise: false)
		UIColor.blackColor().setFill()
		eyes.fill()
		eyes.stroke()
		
		eyes = UIBezierPath(arcCenter: CGPoint(x: bounds.midX + 70, y: bounds.midY - skullRadius / 3),
		                        radius: 10,
		                        startAngle: 0,
		                        endAngle: CGFloat(M_PI * 2),
		                        clockwise: false)
		UIColor.blackColor().setFill()
		eyes.fill()
		eyes.stroke()
    }


}
