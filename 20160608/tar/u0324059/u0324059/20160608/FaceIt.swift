//
//  FaceIt.swift
//  0324059
//
//  Created by nkfust09_2 on 2016/6/8.
//  Copyright © 2016年 nkfust09_2. All rights reserved.
//

import UIKit

class FaceIt: UIView {
    
    override func drawRect(rect: CGRect)
    {
        let skullRadius = min(bounds.size.width ,bounds.size.height) / 2 

        let RedRadius = skullRadius/3
        let RedCenter = CGPoint(x: bounds.midX, y: bounds.midY-(RedRadius/5*4))
        let Red = UIBezierPath(arcCenter: RedCenter, radius: RedRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        UIColor.redColor().set()
        Red.lineWidth = 5.0
        Red.stroke()
        
        let BlueCenter = CGPoint(x: bounds.midX-(RedRadius*3/2), y: bounds.midY-(RedRadius/5*4))
        let Blue = UIBezierPath(arcCenter: BlueCenter, radius: RedRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        UIColor.blueColor().set()
        Blue.lineWidth = 5.0
        Blue.stroke()
        
        let BlackCenter = CGPoint(x: bounds.midX+(RedRadius*3/2), y: bounds.midY-(RedRadius/5*4))
        let Black = UIBezierPath(arcCenter: BlackCenter, radius: RedRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        UIColor.blackColor().set()
        Black.lineWidth = 5.0
        Black.stroke()
        
        let YellowCenter = CGPoint(x: ((bounds.midX-(RedRadius*3/2))+bounds.midX)/2, y: bounds.midY-(RedRadius/5*4)+RedRadius)
        let Yellow = UIBezierPath(arcCenter: YellowCenter, radius: RedRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        UIColor.yellowColor().set()
        Yellow.lineWidth = 5.0
        Yellow.stroke()
        
        let GreenCenter = CGPoint(x: ((bounds.midX+(RedRadius*3/2))+bounds.midX)/2, y: bounds.midY-(RedRadius/5*4)+RedRadius)
        let Green = UIBezierPath(arcCenter: GreenCenter, radius: RedRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        UIColor.greenColor().set()
        Green.lineWidth = 5.0
        Green.stroke()
    }
}
