//
//  FaceIt.swift
//  test0608
//
//  Created by nkfust08 on 2016/6/8.
//  Copyright © 2016年 nkfust. All rights reserved.
//

import UIKit

class FaceIt: UIView {
    override func drawRect(rect: CGRect) {
        let skullRadius = min(bounds.size.width, bounds.size.height)/2
        let skullRadius2 = min(bounds.size.width, bounds.size.height)/8
        let skullCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let skullCenter2 = CGPoint(x: bounds.midX/4*5, y: bounds.midY/3*2)
        let skullCenter3 = CGPoint(x: bounds.midX/3*2, y: bounds.midY/3*2)
        let skullCenter4 = CGPoint(x: skullRadius2, y: skullRadius2)
        let skullCenter5 = CGPoint(x: bounds.maxX - skullRadius2, y: bounds.maxY - skullRadius2)
        let skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2), clockwise: false)
        let skull2 = UIBezierPath(arcCenter: skullCenter2, radius: skullRadius2, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2), clockwise: false)
        let skull3 = UIBezierPath(arcCenter: skullCenter3, radius: skullRadius2, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2), clockwise: false)
        let skull4 = UIBezierPath(arcCenter: skullCenter4, radius: skullRadius2, startAngle: 0.0,
                                  endAngle: CGFloat(M_PI * 2), clockwise: false)
        let skull5 = UIBezierPath(arcCenter: skullCenter5, radius: skullRadius2, startAngle: 0.0,
                                  endAngle: CGFloat(M_PI * 2), clockwise: false)
        
        
        skull.lineWidth = 8.7
        UIColor.blackColor()
        skull.stroke()
        
        skull2.lineWidth = 8.7
        UIColor.darkGrayColor().set()
        skull2.fill()
        
        skull3.lineWidth = 8.7
        UIColor.cyanColor().set()
        skull3.fill()
        
        skull4.lineWidth = 8.7
        UIColor.brownColor().set()
        skull4.stroke()
        
        skull5.lineWidth = 8.7
        UIColor.brownColor().set()
        skull5.stroke()
    
    }
}

