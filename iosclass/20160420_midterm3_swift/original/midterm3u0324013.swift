func fibonacci(num:Int64) -> Int64 {
   if num <= 1{
        return num
    }else{
        return fibonacci(num-1) + fibonacci(num-2)
    }
    
}

var position : Int64 = 43
var number = fibonacci(position)
print("The \(position) Fibonacci number is \(number)")