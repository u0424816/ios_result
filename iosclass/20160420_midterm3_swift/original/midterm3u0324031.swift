func fibonacci(n: UInt) -> UInt {
    var sum: UInt = 1
    var tmp: UInt = 0
    var pre: UInt = 0
    for i in 1...n {
        tmp = sum
        sum += pre
        pre = tmp
    }
    return sum

}

print(fibonacci(92))
