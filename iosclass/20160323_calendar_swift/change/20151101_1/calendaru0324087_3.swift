﻿var args : [String] = ["xyz","20151101","0"]
var date = Array(args[1].characters)
var yyyy : Int! = Int(String(date[0...3]))
var mm : Int! = Int(String(date[4...5]))
var dd : Int! = Int(String(date[6...7]))
var mode : Int = Int(args[2])!%7
var week: [String] = ["日","一","二","三","四","五","六"]
var w :[Int]=[6, 2, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4]
var month : [Int] = [31,(yyyy%4 == 0 && yyyy%100 != 0 || yyyy%400 == 0 ? 29:28),31,30,31,30,31,31,30,31,30,31]
if(yyyy < 0 || mm < 1 || mm > 12 || dd < 1 || dd > month[mm-1]){
    print("error")
}else{
    var temp: String = "\(yyyy)\n\(mm)\n\(dd)\n"
    var dayoftheweek : Int = yyyy+(yyyy/4)-(yyyy/100)+(yyyy/400)
    dayoftheweek += w[mm-1]
    if( yyyy%4 == 0 && mm < 3 ) {
        dayoftheweek-=1
        if((yyyy%100) == 0) {
            dayoftheweek+=1
        }
        if((yyyy%400) == 0){
           dayoftheweek-=1 
        } 
    }
    temp+="星期"+week[(dayoftheweek+dd)%7]+"\n"
    for i in 0...6{
        temp+=week[(i+mode)%7]+"\t"
    }
    dayoftheweek=(dayoftheweek - mode + 7 ) % 7 
    for i in 0...month[mm-1]+dayoftheweek{
        if((i)%7 == 0 && i+dayoftheweek != 6){
            temp+="\n"
        }
        temp += (i <= dayoftheweek) ? "\t" :"\(i-dayoftheweek)\t"
    }
    print(temp)
}
