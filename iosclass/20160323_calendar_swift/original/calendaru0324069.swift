var args : [String] = ["xyz","20160318","0"]
let age : Int! = Int(args[1])
let start :Int! = Int(args[2])
let YYYY = age / 10000
let MM = (age / 100) % 100
let DD = age % 100

var b : Bool = true ;

if ( MM < 1 || MM > 12 || DD < 1 || DD > 31 || start > 6 ){
    print("error")
}else{
    
    switch  MM {
        case 4, 6, 11, 9 : 
            if(DD > 30){
                print("error")
                b = false
                }
            fallthrough
        case 2 :
            if(( YYYY % 400 == 0  || ( YYYY % 4 == 0 && YYYY % 100 != 0 ) )){
                if(DD >= 30){
                    print("error")
                    b = false 
                }
            }else if(  ( YYYY % 400 != 0  && ( YYYY % 4 != 0 || YYYY % 100 == 0 )) && DD > 28 ){
                print("error")
                b = false
            }
            
            if b == false{
                break
            }else{
                fallthrough
            }
            
        default : 
            
            print(YYYY)
            print(MM)
            print(DD)
            
            var day = 0, days = 0 
            for var y in 1..<YYYY {
                if (y % 400 == 0 || (y % 4 == 0 && y % 100 != 0)){
                    day += 366
                }else{
                    day += 365
                }
            }
        
            for var m in 1..<MM {
                switch m {
                    case 4, 6, 9, 11 :
                        day += 30
                    case 2 :
                        day += 28
                    default :
                        day += 31
                }
            }
            
            var NowMM = 0
            for var m in 1...MM {
                switch m {
                    case 4, 6, 9, 11 :
                        NowMM = 30
                    case 2 :
                        NowMM = 28
                    default :
                        NowMM = 31
                }
            }
            
            if((YYYY % 400 == 0 || (YYYY % 4 == 0 && YYYY % 100 != 0)) && MM > 2){
                day += 1
            }
            if((YYYY % 400 == 0 || (YYYY % 4 == 0 && YYYY % 100 != 0)) && MM == 2){
                NowMM += 1
            }
            
            days = day + DD
            switch days % 7 {
                case 1 :
                    print("星期一")
                case 2 :
                    print("星期二")
                case 3 :
                    print("星期三")
                case 4 :
                    print("星期四")
                case 5 :
                    print("星期五")
                case 6 :
                    print("星期六")
                default :
                    print("星期日")
            }
            
            var space : Int = 0
            switch start {
                case 0 :
                    print("日\t一\t二\t三\t四\t五\t六")
                    space = ( day % 7 ) + 1
                case 1 :
                    print("一\t二\t三\t四\t五\t六\t日")
                    space = ( day % 7 ) + 7
                case 2 :
                    print("二\t三\t四\t五\t六\t日\t一")
                    space = ( day % 7 ) + 6
                case 3 :
                    print("三\t四\t五\t六\t日\t一\t二")
                    space = ( day % 7 ) + 5
                case 4 :
                    print("四\t五\t六\t日\t一\t二\t三")
                    space = ( day % 7 ) + 4
                case 5 :
                    print("五\t六\t日\t一\t二\t三\t四")
                    space = ( day % 7 ) + 3
                default :
                    print("六\t日\t一\t二\t三\t四\t五")
                    space = ( day % 7 ) + 2
            }
            
            if(space != 7){
                for i in 1...space {
                    print("",terminator:"\t")  
                }
            }
            
            for i in 1...NowMM {
                print(i,terminator:"\t")
                if((i % 7 == (7 - space )) ){
                    print("")
                }
            }
    }
}