func factorial(number: Double ) -> Double {
    if number <= 1{
        return 1
    } else {
        return number * factorial(number-1)
    }
}
func factorial(number: Int ) -> Int {
    if number <= 1{
        return 1
    } else {
        return number * factorial(number-1)
    }
}

for counter in 0...21{
    if counter <= 17{
        print("\(counter)! = \(factorial(Int(counter)))")
    }else{
        print("\(counter)! = \(factorial(Double(counter)))")
    }
    
}