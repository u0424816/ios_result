func f(n: Double) -> Double{
    if n <= 1{
        return 1
    }else {
        return n * f(n-1)
    }
}

for counter in 1...21{
    print("\(counter)! = \(f(Double(counter)))")
}