func factorial(number: Double) ->Double{
    if number <= 1{
        return 1
    }else{
        return number * factorial(number-1)
    }
}
for counter in 0...21{
    print("\(counter)! =\(factorial(Double(counter)))")
}