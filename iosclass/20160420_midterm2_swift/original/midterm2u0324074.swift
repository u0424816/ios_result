import Foundation
var n = 1
while(n<=25){
    var block = 1000000000
    var blockDigit:NSString = "%09d"
    var ans = [Int](count:1,repeatedValue:1)
    ans[ans.count-1]=1
    for(var i=2;i<=n;i++){
        var temp=0;
        for(var j=0;j<ans.count;j++){
            ans[j]*=i
            ans[j]+=temp
            temp=ans[j]/block
            if(temp != 0 && j+1>=ans.count){
                ans.append(0)
            }
            ans[j]%=block
        }
    }
    print("\(n) : ",terminator:"")
    print(ans[ans.count-1],terminator:"")
    for var i=ans.count-2;i>=0;i-- {
        print(NSString(format:blockDigit,ans[i]),terminator:"")
    }
    print("")
    n++
}