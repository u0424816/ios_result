var n = 21
var b : Bool = true

func nf(n : Int) -> Int {
    if n <= 1 {
        return 1
    }else{
        return n * nf(n - 1)
    } 
}

var a = nf(20)
var j = ""

for i in 1...n {
    if(i <= 20){
        print("\(i)! =  \(nf(i))")
    }else{
        a =  nf(20)
        while b {
            if (a % 10 == 0){
                j += "0"
                a /= 10
                if(a % 10 != 0){
                    b = false
                }
            }
        }
        a *= i
        print("\(i)! =  " + String(a)+j)
    }
}
  