func f(num: Float80) -> Float80 {
    if num <= 1 {
        return 1
    } else{
    return num * f(num-1)
    }
}

for counter in 0...25{
    print("\(counter)! = \(f(Float80(counter)))")
}