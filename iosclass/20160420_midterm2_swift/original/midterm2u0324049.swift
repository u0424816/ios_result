func factorial(number: Int) -> (Int) {
    if (number <= 1) {
        return 1
    }
        
    return number * factorial(number - 1)
}

var fact = factorial(20)
print("21! = \(fact/10000*21)0000")
