var strInput = ["____", "1+2*3-4"]

import Foundation

func strtocha(input:[String])->[String]{
    var restr:[String] = [""]
    var chartemp: [Character] = Array(input[1].characters)
    var index:Int=0
    
    for i in 0...chartemp.count-1{
        switch chartemp[i]{
            case "+","-","*","/","(",")":
                if(restr[index]==""){
                    restr[index]=String(chartemp[i])
                    restr.append("")
                    index+=1
                }else{
                    restr.append(String(chartemp[i]))
                    restr.append("")
                    index+=2
                }
                break
            default :
                restr[index] += String(chartemp[i])
                break
        }
    }
    if(restr[restr.count-1]==""){
        restr.removeLast()
    }
    return restr
}

func topostfix(input:[String])->[String]{   
    var stack:[String]=[""]
    var output:[String]=[""]
    for i in input{
        switch i{
            case "+","-":
                var templast:String = stack[stack.count-1]
                while(((templast=="*")||(templast=="/")||(templast=="+")||(templast=="-"))){
                    output.append(stack.removeLast())
                    templast=stack[stack.count-1]
                }
                stack.append(i)
                break
            case "*","/":
                var templast:String = stack[stack.count-1]
                while(((templast=="*")||(templast=="/"))){
                    output.append(stack.removeLast())
                    templast=stack[stack.count-1]
                }
                stack.append(i)
                break
            case "(":
                stack.append(i)
                break
            case ")":
                var templast:String = stack[stack.count-1]
                while(templast != "("){
                    output.append(stack.removeLast())
                    templast=stack[stack.count-1]
                }
                stack.removeLast()
                break
            default:
                output.append(i)
                break
        }
    }
    for(var index=stack.count-1;index>=0;index-=1){
        output.append(stack[index])
    }
    output.removeLast()
    output.removeAtIndex(0)
    return output
}

func calc1(input:[String])->Double{
    var result:Double = Double(input[0])!
    var index:Int = 0
    for i in input{
        switch i{
            case "+","-","*","/":
                index+=1
                if(i=="+"){
                    result = result + Double(input[index])!
                }else if(i=="-"){
                    result = result - Double(input[index])!
                }else if(i=="*"){
                    result = result * Double(input[index])!
                }else{
                    result = result / Double(input[index])!
                }
            break
            default:
                index+=1
            break
        }
    }
    return result
}

func calc2(input:[String])->Double{
    var temp:Double = 0
    var stack:[Double]=[]
    for i in input{
        switch i{
            case "+":
                temp=stack[stack.count-2]+stack[stack.count-1]
                stack.removeLast()
                stack.removeLast()
                stack.append(temp)
                break
            case "-":
                temp=stack[stack.count-2]-stack[stack.count-1]
                stack.removeLast()
                stack.removeLast()
                stack.append(temp)
                break
            case "*":
                temp=stack[stack.count-2]*stack[stack.count-1]
                stack.removeLast()
                stack.removeLast()
                stack.append(temp)
                break
            case "/":
                temp=stack[stack.count-2]/stack[stack.count-1]
                stack.removeLast()
                stack.removeLast()
                stack.append(temp)
                break
            default:
                stack.append(Double(i)!)
                break
        }
    }
    return stack[0]
}

func main() {   
    var charstr:[String] = []
    var answer1:Double = 0
    var answer2:Double = 0
    print("\(strInput[1])=")
    charstr=strtocha(strInput)
    answer1=calc1(charstr)
    charstr=topostfix(charstr)
    answer2=calc2(charstr)
    
    var mynsdbl:NSNumber = NSNumber(double : answer2)
    var fmt = NSNumberFormatter()

    //fmt.numberStyle = .DecimalStyle
    //fmt.maximumFractionDigits = 2
    mynsdbl = NSNumber(double : answer1)
    print(fmt.stringFromNumber(mynsdbl)!)
    fmt.groupingSeparator = ","
    fmt.groupingSize = 3
    fmt.usesGroupingSeparator=true
    mynsdbl = NSNumber(double : answer2)
    print(fmt.stringFromNumber(mynsdbl)!)
}

main()
