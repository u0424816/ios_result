var strInput = ["____", "0/9"]

func isCorrect(_num:[Int], _op:[String]) -> Bool {
	if _num.count != _op.count + 1 {
		return false
	} else {
		var index = 0
		for n in _num {
			if (index > 0 && n == 0 && _op[index - 1] == "/") {
				return false
			}
			index++
		}
	}
	return true
}

func calc_1(_num:[Int] , _op:[String]) -> Int {
	var num = Array(_num)
	var op = Array(_op)
	var ans:Double = Double(num.removeFirst())
	while (!num.isEmpty){
		var tmp_num:Double = Double(num.removeFirst())
		var tmp_op = op.removeFirst()
		switch tmp_op{
		case "+":
			ans += tmp_num
		case "-":
			ans -= tmp_num
		case "*":
			ans *= tmp_num
		case "/":
			ans /= tmp_num
		default:
			break
		}
	}
	return Int(ans)
}

func op_value(op:String) -> Int {
	if op == "+" || op == "-" {
		return 1
	}else if op == "*" || op == "/" {
		return 2
	}
	return -1
}

func calc_2(_in_order:[String]) -> Int {
	var in_order:[String] = _in_order
	var post_order:[String] = []
	var op:[String] = []
	for item in _in_order{
		switch item {
			case "+", "-", "*", "/":
				if op.isEmpty {
					op.append(item)
				} else {
					var tmp:String = op.removeLast()

					if op_value(tmp) < op_value(item) {
						op.append(tmp)
						op.append(item)
					} else {
						var is_cause_empty = false
						repeat {
							post_order.append(tmp)
							if op.isEmpty { 
								is_cause_empty = true
								break
							}
							tmp = op.removeLast()
						} while op_value(tmp) >= op_value(item)
						if !is_cause_empty {
							op.append(tmp)
						}
						op.append(item)
					}
				}
			default:
				post_order.append(item)
		}
	}
	while !op.isEmpty {
		post_order.append(op.removeLast())
	}
	var stack_num:[Int] = []
	for item in post_order {
		if op_value(item) == -1 {
			stack_num.append(Int(item)!)
		} else {
			if stack_num.count >= 2 {
				var t = stack_num.removeLast()
				switch item {
					case "+":
						stack_num[stack_num.count - 1] += t
					case "-":                            					
						stack_num[stack_num.count - 1] -= t
					case "*":
						stack_num[stack_num.count - 1] *= t
					case "/":
						stack_num[stack_num.count - 1] /= t
					default:
						break
				}
			}
		}
	}
	return stack_num[0]
}

func format(_number:Int) -> String{
	var number = _number
	let m = 1000
	var ans = ""
	while (number / m) > 0 {
		var t = ""
		var i = 3 - Int(String(number % m).characters.count)
		while (i > 0) {
			t += "0"
			i--
		}
		ans = String(number % m) + ans
		number /= m
		if (number > 0) {
			ans = t + ans
		}
		ans = "," + ans
	}
	return String(number) + ans
}

var num = Array<Int>()
var op = Array<String>()

var tmp = ""
for item in strInput[1].characters {
	if item == "+" || item == "-" || item == "*" || item == "/" {
		num += [Int(tmp) == nil ? -1 : Int(tmp)!]
		tmp = ""
		op += [String(item)]
	} else {
		tmp = tmp + String(item)
	}
}
num += [Int(tmp)!]
if (isCorrect(num, _op:op)) {
	print(strInput[1] + "=")
	print(calc_1(num, _op:op))
	var in_order:[String] = []
	var count = 0
	var c = op.count + num.count
	while(count < c){
		in_order += [count % 2 == 0 ? String(num.removeFirst()) : op.removeFirst()]
		count += 1
	}
	print(format(calc_2(in_order)), terminator:"")
}else{
	print("____", terminator:"")
}
