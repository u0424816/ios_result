var strInput = ["____", "12345*0"]
print(strInput[1]+"=")

func split(input : String) -> [String]{
    var tempNum = ""
    var splitArray = [String]()
    for index in input.characters {
        if(index != "+" && index != "-" && index != "*" && index != "/" && index != "(" && index != ")"){
            tempNum+=String(index)
        }else{
            if(tempNum != ""){
                splitArray.append(tempNum)
                tempNum = ""
            }
            splitArray.append(String(index))
        }
    }
    splitArray.append(tempNum)
    return splitArray
}

var input = split(strInput[1])
var ans = Int(input[0])!
var isError = false
for (var i = 1 ; i < input.count ; i++){
    if(input[i] == "+"){
        ans += Int(input[++i])!
    }else if(input[i] == "-"){
        ans -= Int(input[++i])!
    }else if(input[i] == "*"){
        ans *= Int(input[++i])!
    }else if(input[i] == "/"){
        if(input[i+1] != "0"){
            ans /= Int(input[++i])!
        }else{
            isError = true
            break
        }
    }
}
if(isError){
    print(strInput[0])
}else{
if(ans%1 != 0){
    print(ans)
}else{
    var intNum = Int(ans)
        print(intNum)
    }
}


func toPostfix(input : [String]) -> [String]{
    var postfixArray = [String]()
    var operatorTemp = [String]()
    for index in input {
        if(index == ")"){
            while(operatorTemp.count-1>=0 && operatorTemp[operatorTemp.count-1] != "(") {
                postfixArray.append(operatorTemp.removeAtIndex(operatorTemp.count-1))
            }
            operatorTemp.removeAtIndex(operatorTemp.count-1)
        }else if(index == "("){
            operatorTemp.append(index)
        }else if(index == "+" || index == "-"){
            while( operatorTemp.count-1>=0 && operatorTemp[operatorTemp.count-1] != "("){
                postfixArray.append(operatorTemp.removeAtIndex(operatorTemp.count-1))
            }
            
            operatorTemp.append(index)
        }else if(index == "*" || index == "/"){
            while(operatorTemp.count >= 1 && (operatorTemp[operatorTemp.count-1] == "*" || operatorTemp[operatorTemp.count-1] == "/")){
                postfixArray.append(operatorTemp.removeAtIndex(operatorTemp.count-1))
            }
            operatorTemp.append(index)
        }else{
            postfixArray.append(index)
        }
    }
    while(!operatorTemp.isEmpty){
        postfixArray.append(operatorTemp.removeAtIndex(operatorTemp.count-1))
    }
    return postfixArray
}
func postfixCalc(input : [String]) -> [String] {
    var result = input
    var doubleN1 = 0.0
    var doubleN2 = 0.0
    var location = 0
    var temp3th = ""
    var isError2 = false
    while(result.count != 1){
        doubleN1=Double(result[location])!
        doubleN2=Double(result[location+1])!
        temp3th = String(result[location+2])
        switch temp3th {
            case "+" :
                result[location]=String(doubleN1 + doubleN2)
                result.removeAtIndex(location+2)
                result.removeAtIndex(location+1)
                location = 0
                break
            case "-" :
                result[location]=String(doubleN1 - doubleN2)
                result.removeAtIndex(location+2)
                result.removeAtIndex(location+1)
                location = 0
                break
            case "*" :
                result[location]=String(doubleN1 * doubleN2)
                result.removeAtIndex(location+2)
                result.removeAtIndex(location+1)
                location = 0
                break
            case "/" :
                if(doubleN2 == 0 ){
                    isError2 = true
                    break
                }
                result[location]=String(doubleN1 / doubleN2)
                result.removeAtIndex(location+2)
                result.removeAtIndex(location+1)
                location = 0
                break
            default :
                location++
        }
        if(isError2){
            result[0] = "error"
            break
        }
    }
    return result
}
var postfix2 = postfixCalc(toPostfix(split(strInput[1])))
var ans2 = ""

var doubleAns2 : Double
var ansTemp = ""
var ansArray = [String]()
var ansArray2 = [String]()

if(postfix2[0] == "error"){
    print(strInput[0] , terminator : "")
}else{
    doubleAns2 = Double(postfix2[0])!
    if(doubleAns2%1 != 0){
        ansTemp = String(doubleAns2)
    }else{
        var intNum = Int(doubleAns2)
        ansTemp = String(intNum)
    }
    var length = 0
    for i in ansTemp.characters {
        if(i == "."){
            break
        }
        length++
    }    
    for i in ansTemp.characters {
        ansArray.append(String(i))
    }
    var lengthDist = (length-1)%3
    for (var i = 0 ; i < ansArray.count ; i++){
        ansArray2.append(ansArray[i])
        if(i%3 == lengthDist && i < length - 1){
            ansArray2.append(",")
        }
    }
    for index in ansArray2 {
        ans2 += String(index)
    }
    print(ans2 , terminator : "")
}