var strInput = ["____", "844-2+24*24/2"]
var finalOutput = ""

func isInclude(str: String,_ cs: String) -> Bool {
    var counter = 0
    for index in 0...str.characters.count-1 {
        if Array(str.characters)[index] == Array(cs.characters)[counter]{
            if cs.characters.count-1 == counter{
               return true
            }else{
                counter += 1
            }
        }
    }
    return false
}

func calcSplit(str: String) -> [String]{
    var charTmp: [String] = [""]
    for i in str.characters{
        switch i {
            case "0","1","2","3","4","5","6","7","8","9":
                charTmp[charTmp.count-1] += String(i)
            default:
                if charTmp[charTmp.count-1] == "" {
                    charTmp[charTmp.count-1] += String(i)
                    charTmp.append("")
                }else{
                    charTmp.append(String(i))
                    charTmp.append("")
                }
        }
    }
    return charTmp
}

func calcMix(str: [String]) -> String{
    var output = ""
    for i in str{
        output += i
    }
    return output
}

func calcFirst(str: String) -> String{
    if isInclude(str,"*") || isInclude(str,"/"){
        var calcSp = calcSplit(str)
        var str = ""
        var counter = 0
        while (counter < calcSp.count){
            switch calcSp[counter] {
                case "*":
                    calcSp[counter] = String(Int(calcSp[counter-1])! * Int(calcSp[counter+1])!)
                    calcSp[counter-1] = ""
                    calcSp[counter+1] = ""
                case "/":
                    calcSp[counter] = String(Int(calcSp[counter-1])! / Int(calcSp[counter+1])!)
                    calcSp[counter-1] = ""
                    calcSp[counter+1] = ""
                default:
                    break
            }
            str = calcMix(calcSp)
            calcSp = calcSplit(str)
            counter++
        }
        return calcFirst(str)
    }else{
        return str
    }
}

func calcSecond(str: String) -> String{
    var calcSp = calcSplit(str)
    var output = calcSp[0]
    for i in 0...calcSp.count-1 {
        switch calcSp[i] {
            case "+":
                output = String(Int(output)! + Int(calcSp[i+1])!)
            case "-":
                output = String(Int(output)! - Int(calcSp[i+1])!)
            default:
                break
        }
    }
    return output
}

func calcQueue(str: String) -> String{
    var calcSp = calcSplit(str)
    var result = calcSp[0]
    for i in 0...calcSp.count-1 {
        switch calcSp[i] {
            case "+":
                result = String(Int(result)! + Int(calcSp[i+1])!)
            case "-":
                result = String(Int(result)! - Int(calcSp[i+1])!)
            case "*":
                result = String(Int(result)! * Int(calcSp[i+1])!)
            case "/":
                result = String(Int(result)! / Int(calcSp[i+1])!)
            default:
                break
        }
    }
    return result
}

func addComma(str: String) -> String{
    var spstr =  Array(str.characters)
    if spstr.count > 3 {
        for i in 1...spstr.count {
            if i % 3 == 0{
                spstr.insert(",",atIndex: spstr.count - i)
            }
        }
    }
    return String(spstr)
}

print("\(strInput[1])=")

if isInclude(strInput[1], "/0") || !(isInclude(strInput[1],"(") == isInclude(strInput[1],")")){
    print(strInput[0])
}else if isInclude(strInput[1],"(") && isInclude(strInput[1],")"){
    print(strInput[0])
    var strTmp = calcSplit(strInput[1])
    var calcLeft = 0, calcRight = 0, calcStr = ""
    for i in 0...strTmp.count-1{
        if isInclude(strTmp[i],"(") {
            calcLeft += i + 1
        }else if isInclude(strTmp[i],")"){
            calcRight = i - 1
        }
    }
    for _ in 0...calcRight-calcLeft{
        calcStr += strTmp[calcLeft]
        strTmp.removeAtIndex(calcLeft)
    }
    strTmp.removeAtIndex(calcLeft-1)
    strTmp[calcLeft-1] = calcSecond(calcFirst(calcStr))
    finalOutput = calcSecond(calcFirst(calcMix(strTmp)))
    print(finalOutput)
}else{
    print(calcQueue(strInput[1]))
    finalOutput = calcSecond(calcFirst(strInput[1]))
    print(addComma(finalOutput))
}