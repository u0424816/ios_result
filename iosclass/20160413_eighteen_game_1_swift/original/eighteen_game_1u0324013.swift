import Foundation
import CoreFoundation
let time = UInt32(NSDate().timeIntervalSinceReferenceDate)
srand(time)

func rollDice() -> (die1:Int , die2:Int , die3:Int , die4:Int , dieSet1 : Set<Int> , dieSet2 : Set<Int>) {
    let die1 = Int(rand()%6+1)
    let die2 = Int(rand()%6+1)
    let die3 = Int(rand()%6+1)
    let die4 = Int(rand()%6+1)
    let dieSet1 : Set<Int> = [die1 , die2]
    let dieSet2 : Set<Int> = [die3 , die4]
    
    
    return (die1 ,die2 , die3 , die4 , dieSet1 , dieSet2)
}

func displayRoll(rollIn: (Int , Int , Int , Int , Set<Int> , Set<Int> ) , name : String){
    print("\(name) rolled : \(rollIn.0)\t\(rollIn.1)\t\(rollIn.2)\t\(rollIn.3)\t")
    
}

func computeScore (roll: (die1:Int , die2:Int , die3:Int , die4:Int , dieSet1:Set<Int> , dieSet2:Set<Int> )) -> Int{
    
    var sum = 0
    if(roll.dieSet1.union(roll.dieSet2).count == 1){
    sum = 13
    }else{

    if(roll.dieSet1.count == 2 && roll.dieSet2.count == 2){
   
      var intersect = roll.dieSet1.intersect(roll.dieSet2)
   
      if(intersect.count == 1){
          var exclusiveOr = roll.dieSet1.exclusiveOr(roll.dieSet2)
          sum = exclusiveOr.removeFirst() + exclusiveOr.removeFirst()
      }else{
   
      let tmp1 = intersect.removeFirst() * 2
      let tmp2 = intersect.removeFirst() * 2
      
      if(tmp1>tmp2){
          sum = tmp1
      }else{
          sum = tmp2
      }
      }
    
    }else if(roll.dieSet1.count == 1 && roll.dieSet2.count != 1 ) {
        sum = roll.die3 + roll.die4
    }else if(roll.dieSet2.count == 1 && roll.dieSet1.count != 1 ){
        sum = roll.die1 + roll.die2
    }else{

        if(roll.die1 > roll.die3){
            sum = roll.die1 * 2
        }else{
            sum = roll.die3 * 2
        }
    
    }
}
    return sum
}


var rollBanker = rollDice()
var sumBanker = 0
displayRoll(rollBanker , name : "Banker")
while((rollBanker.dieSet1.union(rollBanker.dieSet2)).count == 4){
    rollBanker = rollDice()
    print("\nRedice\n")
    displayRoll(rollBanker , name : "Banker")
    }
    
sumBanker = computeScore(rollBanker)
if(sumBanker <= 12){
    print("Banker's Score = \(sumBanker)")    
}else{
    print("¤@¦â")
}


print("\n-------------------------------------------------------------\n")


var rollPlayer = rollDice()
var sumPlayer = 0
displayRoll(rollPlayer , name : "Player")
while((rollPlayer.dieSet1.union(rollPlayer.dieSet2)).count == 4){
    rollPlayer = rollDice()
    print("\nRedice\n")
    displayRoll(rollPlayer , name : "Player")
    }
    
sumPlayer = computeScore(rollPlayer)
if(sumPlayer <= 12){
    print("Player's Score = \(sumPlayer)")    
}else{
    print("¤@¦â")
}
print("\n-------------------------------------------------------------\n")


if(sumPlayer > sumBanker){
    print("You Win!!!")
}else if(sumPlayer < sumBanker){
    print("You Lose!!!")
}else{
    print("Draw")
}