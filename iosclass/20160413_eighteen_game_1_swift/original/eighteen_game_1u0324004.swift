import Foundation
import CoreFoundation
public enum stat{
    case 點數
    case 十八
    case 扁膣
    case 豹子
    case 無點
}
public class player{
    public var name : String = ""
    public var point : Int = 0
    public var dice_stat = stat.無點
    public var dice : [Int] = [0,0,0,0]
    public init(name : String){
        self.name = name
    }
    public func roll(){
        var dicNA = [0,0,0]
        var dicNB = [0,0,0]  
        var tmpNB : Int = 0
        point = 0
        dice = [0,0,0,0]
        for i in 0...3{
            tmpNB = Int(rand() % 6) + 1
            dice[i] = tmpNB
            print("\(tmpNB)\t",terminator: "")
            for j in 0...2{
                if(dicNA[j] == tmpNB){
                    dicNB[j] = dicNB[j] + 1
                    break
                }else if(dicNA[j] == 0){
                    dicNA[j] = tmpNB
                    dicNB[j] = 1
                    break
                }
            }
        }
        if(dicNB[0] + dicNB[1] + dicNB[2] != 4){
            dice_stat = stat.無點
            print("\(dice_stat) 繼續擲骰子")
            roll()
        }else if(dicNB[0] == 4){
            dice_stat = stat.豹子
            point = dicNA[0] * 4 
            print("\(dice_stat)")
        }else{
            if(dicNB[2] != 0){
                for i in 0...2{
                    if(dicNB[i] != 2){
                        point = point + dicNA[i]
                    }
                }
            }else{
                if(dicNB[0] == 2 || dicNB[1] == 2){
                    if(dicNA[0] > dicNA[1]){
                        point = dicNA[0] * 2
                    }else{
                        point = dicNA[1] * 2
                    }
                }else{
                    point = dicNA[0] + dicNA[1]
                }
            }
            if(point == 3){
                dice_stat = stat.扁膣
            }else if(point == 12){
                dice_stat = stat.十八
            }else{
                dice_stat = stat.點數
            }            
            print("\(dice_stat) => \(point)")  
        }
    }
}
print("點數相同 平手 , 如兩方都為豹子 平手")
var players : [player] = [player(name : "莊家")]
let playerNumber = 1
for index in 1...playerNumber{
    players.append(player(name : "player" + String(index)))
}
for index in 0...players.count - 1{
    print("\n----------\(players[index].name)----------")
    players[index].roll()
    if(index != 0){
        if(players[0].dice_stat == stat.豹子 || players[index].dice_stat == stat.豹子 ){
            if(players[0].dice_stat == players[index].dice_stat){
                print("平手!")
            }else{
                if(players[0].dice_stat == stat.豹子){print("\(players[0].name) 獲勝!")}
                if(players[index].dice_stat == stat.豹子){print("\(players[index].name) 獲勝!")}
            }
        }else{
            if(players[0].point > players[index].point){ print("\(players[0].name) 獲勝!")}
            if(players[0].point < players[index].point){ print("\(players[index].name) 獲勝!")}
            if(players[0].point == players[index].point){ print("平手!")}
        }
    }
}