import Foundation
import CoreFoundation

let time = UInt32(NSDate().timeIntervalSinceReferenceDate)
srand(time)

enum Status {
    case Continue, Won, Lost
}

var a : Bool = true
func rollDice() -> (die1: Int,die2: Int,die3: Int,die4: Int,sum: Int) {
    let die1 = Int(rand()%6+1)
    let die2 = Int(rand()%6+1)
    let die3 = Int(rand()%6+1)
    let die4 = Int(rand()%6+1)
    if(die1 == die2 && die2 == die3 && die3 == die4){
        return(die1,die2,die3,die4,die1 + die2)
    }else if(die1 == die2 && die3 == die4 && die1 > die3){
        return(die1,die2,die3,die4,die1 + die2)
    }else if(die1 == die2 && die3 == die4 && die1 < die3){
        return(die1,die2,die3,die4,die3 + die4)
    }else if(die1 == die3 && die2 == die4 && die1 > die2){
        return(die1,die2,die3,die4,die1 + die3)
    }else if(die1 == die3 && die2 == die4 && die1 < die2){
        return(die1,die2,die3,die4,die2 + die4)
    }else if(die1 == die2){
        return(die1,die2,die3,die4,die3 + die4)
    }else if(die1 == die3){
        return(die1,die2,die3,die4,die2 + die4)
    }else if(die1 == die4){
        return(die1,die2,die3,die4,die2 + die3)
    }else if(die2 == die3){
        return(die1,die2,die3,die4,die1 + die4)
    }else if(die3 == die4){
        return(die1,die2,die3,die4,die1 + die2)
    }else if(die2 == die4){
        return(die1,die2,die3,die4,die1 + die3)
    }else{
        return(die1,die2,die3,die4,0)
    }
}

var roll = rollDice()
func displayRoll(roll: (Int , Int , Int , Int, Int)){
    if(roll.4 == 3){
        print(" roll : \(roll.0)  \(roll.1)  \(roll.2)  \(roll.3)  Point : BG")
        a = false
    }else if(roll.0 == roll.1 && roll.1 == roll.2 && roll.2 == roll.3){
        print(" roll : \(roll.0)  \(roll.1)  \(roll.2)  \(roll.3)  Point : 豹子")
        a = false
    }else if(roll.0 == roll.1 && roll.1 == roll.2 && roll.2 == roll.3 && roll.0 == 6){
        print(" roll : \(roll.0)  \(roll.1)  \(roll.2)  \(roll.3)  Point : 十八啦")
        a = false
    }else if(roll.3 > 3 && roll.3 < 12){
        print(" roll : \(roll.0)  \(roll.1)  \(roll.2)  \(roll.3)  Point : \(roll.4)")
        a = false
    }else if(roll.4 == 0){
        print(" roll : \(roll.0)  \(roll.1)  \(roll.2)  \(roll.3)  Point : reRoll")
    }
}
    
var gameStatus = Status.Continue      
var PlayerPoint = 0                                        
var BankerPoint = 0

while gameStatus == Status.Continue {           
    while a {
        roll = rollDice()
        print("玩家", terminator : "")
        displayRoll(roll)
        PlayerPoint = roll.4
    }
    a = true
    while a {
        roll = rollDice()
        print("莊家", terminator : "")
        displayRoll(roll)
        BankerPoint = roll.4
    }
    a = true
    if(PlayerPoint > BankerPoint) {
        gameStatus = Status.Won
    }else {
        gameStatus = Status.Lost
    }
}

if gameStatus == Status.Won {                    
    print("玩家勝利")
}else {
    print("莊家勝利")
}