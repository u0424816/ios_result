
Share your code
Use this custom URL to share your source code, or click below to choose your preferred social media channel.

Saving...
  Copy
 What's New?Here's some info on what we've changed in the Sandbox recently.
April 1, 2016
0.5.1
- Users can submit feedback and ask questions via developerWorks.
- Snapshot popup now has an automatic Copy button.
- Optimizations to increase load speed.
- Bug fixes and improvements.
March 25, 2016
0.5
- Users can now choose which version of Swift to run their code against.
- More Swift builds will be added in the future.
- Bug fixes and improvements.
March 14, 2016
0.4.7
- Upgraded to Swift 3.0 development screenshot from March 1, 2016.
February 19, 2016
0.4.5
- Users can now sign up for e-mail updates regarding Swift@IBM.
- Improved user metrics.
- Bug fixes and improvements.
January 25, 2016
0.4.1
- Drafts now include dates.
- Alert appears if you have an unsaved draft available.
- Bug fixes and improvements.
January 19, 2016
0.4
- User interface redesigned for improved experience on both desktop and mobile.
- Clear Code button allows users to quickly clear the editor.
- Sandbox now autosaves code, which can be restored from the Source Code menu.
December 28, 2015
0.3.1
- Fixed caching bug with non-deterministic code.
- Minor UI fixes.
December 18, 2015
0.3
- Code "snapshots" can now be shared by clicking the Share Code button, creating a unique URL to your code.
- Users can now choose between a Dark Theme and Light Theme.
December 15, 2015
0.2
- Major backend and database updates to prepare for 0.3.
- Various bug fixes and improvements.
December 10, 2015
0.1.2
- Various bug fixes and improvements.
December 8, 2015
0.1.1
- Added new samples and theming to celebrate the Hour of Code.
- Minor UI tweaks and backend improvements.
December 4, 2015
0.1
- First release of the IBM Swift Sandbox.
Sign up to get advance news specific to Swift@IBM updates!Join us as we bring Swift to the cloud!

Email Address

I accept IBM's Privacy Statement and Terms and Conditions
Sign up now
Select Source CodePlease select source sample code or your saved code below.Source Samples  Saved Draft
fibonacci.swift
filereader.swift
filestat.swift
hello_world.swift
hour_of_code.swift
hour_of_code_solution.swift
nsurlcomponents.swift
otherlangs.swift
programwriter.swift
quicksort.swift
scriptwriter.swift
server.swift
stack.swift
tree.swift
Start Coding
Clear Code?
Are you sure you want to start over? This action cannot be undone.
No  Yes

Settings
SELECT VERSION
Ver. 3.0 (Mar 24, 2016)

SELECT THEME
  Dark Theme
  Light Theme
TOGGLE AUTOSAVE
Sandbox will autosave your code every so often. You can restore an autosaved draft in the Source Code menu.
 Enable Autosave

You have a saved draft available. Would you like to load it?
You can also load drafts from the Source Code menu at any time.
 Do not show this alert again
No  Yes
Shape the future of the Sandbox!
If you're enjoying the IBM Swift Sandbox, please take a couple minutes to take our quick survey!
No thanks Take survey

IBM Swift Sandbox BETA
About
What's New?
Feedback
 

49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86
87
88
89
90
91
92
var myPoint = 0                                 //玩家初始點數        
var BOOKIEnoun = 0                              //莊家初始點數
var play = 4
var big=0
var bigPlay=0
while gameStatus == Status.Continue {        //賽局判斷
    for var i in 1...play {
    
      while a {
           roll = rollDice()
           print("玩家\(i)", terminator : "")
          display(roll)
          myPoint = roll.3
          if (myPoint > big){
              big=myPoint
              bigPlay=i
          }
         
        } 
        
         a = true
    }
    while a {
        roll = rollDice()
        print("莊家", terminator : "")
        display(roll)
        BOOKIEnoun = roll.3
    }
    a = true
    if(big > BOOKIEnoun) {
        gameStatus = Status.Win
    }else {
        gameStatus = Status.Lose
    }
}
if gameStatus == Status.Win {                     //輸出判斷
    print("玩家 \(bigPlay)勝利")
}else {
    print("莊家勝利")
}
Swift version 3.0-dev (LLVM b010debd0e, Clang 3e4d01d89b, Swift 7182c58cb2)
Target: x86_64-unknown-linux-gnu
玩家1 3  6  2  重骰
玩家1 6  5  3  重骰
玩家1 5  5  3  點數 : 3
玩家2 5  4  5  點數 : 4
玩家3 3  6  2  重骰
玩家3 2  3  5  重骰
玩家3 6  1  5  重骰
玩家3 1  3  4  重骰
玩家3 6  2  2  點數 : 6
玩家4 2  5  3  重骰
玩家4 5  5  6  點數 : 6
莊家 6  2  5  重骰
莊家 6  1  1  點數 : 6
莊家勝利
 Source Code 
 Clear Code
 Settings
 Share Code