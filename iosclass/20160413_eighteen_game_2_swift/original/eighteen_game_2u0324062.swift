import Foundation
import CoreFoundation

func main(){

    var cpu = player18(name:"cpu")
    
    var playerlist:[player18] = []
    playerlist.append(player18(name:"player1"))
    playerlist.append(player18(name:"player2"))
    playerlist.append(player18(name:"player3"))
    
    cpu.show()
    
    print("")
    
    for i in playerlist{
        i.show()
        if(cpu.point>=i.point){
        print("\(cpu.name) win\n")
        }
        else{
        print("\(i.name) win\n")
        }
    }
    
    
}

func rolldice()->[Int]{
    var people:[Int]=[]
    var compare:Int=0
    while(compare<2){
    people=[]
    for _ in 1...4{
        people.append(Int(rand()%6+1))
    }
        for i in people{
            compare=0
            for j in people{
                if(i==j){
                  compare+=1
                }
            }
        }
    }
    return people
}

func calc(point:[Int])->Int{
    var cut:Int=0
    var count:Int
    for i in point{
        count=0
        for j in point{
            if(i==j){
                count+=1
            }
        }
        if(count>1){
            cut=i
            break
        }
    }
    if(point[0]==point[1]&&point[1]==point[2]&&point[2]==point[3]){
        return 13
        }
    return (point[0]+point[1]+point[2]+point[3]-cut*2)
}

class player18 {
    var name:String
    var dice:[Int] = []
    var point:Int = 0
    func show(){
        print("\(self.name):\(self.dice)->\(self.point)")
    }
    init(name: String) {
    self.name = name
    self.dice = rolldice()
    self.dice = self.dice.sort()
    self.point = calc(dice)
    }
}

main()
