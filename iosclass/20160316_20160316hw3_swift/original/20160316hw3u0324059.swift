var YYYY = 2016
var MM = 03
var DD = 16

var week : [String] = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
var month : [Int] = [31, (YYYY%400 == 0 || YYYY%4 == 0 && YYYY%100 != 0) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
var date : [Int] = [6, 2, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4]

if(MM<1 || MM>12 || DD<1 || DD>month[MM-1])
{
    print("error")
}
else
{
    var w = date[MM-1] + YYYY + (YYYY/4) - (YYYY/100) + (YYYY/400)
    
    if (((YYYY%4) == 0) && (MM<3))
    {
        w--;
        
        if((YYYY%100) == 0)
        {
            w++
        }
        
        if((YYYY%400) == 0)
        {
            w--
        }
    }
    
    print("\(week[((w+DD)%7)])")
}