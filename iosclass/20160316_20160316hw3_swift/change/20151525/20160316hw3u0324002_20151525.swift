import Foundation

var YYYY = 2015
var MM = 3
var DD = 25

func isCorrectDate(y:Int, m:Int, d: Int) -> Bool {
    var d_:[Int] = [31,28,31,30,31,30,31,31,30,31,30,31]
    if ((y%4==0)&&(y%100 != 0)||(y%400==0)){ d_[1] = 29}
    if m<1 || m > 12 {
        return false
    }
    if d<1 || d > d_[m-1] {
        return false
    }
    return true
}

if isCorrectDate(YYYY, m: MM, d: DD){
    //«Å§i
    let dateFormatter = NSDateFormatter()
    let calendar = NSCalendar.currentCalendar()
    var dateComponents = NSDateComponents()
    
    //³]©w¤é´Á®æ¦¡
    dateFormatter.locale = NSLocale(localeIdentifier: "zh_TW")
    dateFormatter.dateFormat = "EEEE"
    //«ü©w¤é´Á
    dateComponents.year = YYYY
    dateComponents.month = MM
    dateComponents.day = DD
    let d = NSDate()
    var date = calendar.dateFromComponents(dateComponents)
    print(dateFormatter.stringFromDate(date!), terminator:"")

}else{
    print("error", terminator:"")
}