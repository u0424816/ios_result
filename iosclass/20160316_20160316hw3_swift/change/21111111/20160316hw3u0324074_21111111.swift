var YYYY = 2111
var MM = 11
var DD = 11
var isLeap = false
var isError = false
var day = 5
var month = [3,1,3,2,3,2,3,3,2,3,2,3]
var now = 2016
if(YYYY % 400 == 0 || (YYYY % 4 == 0 && YYYY % 100 != 0)){
    isLeap = true
}else{
    isLeap = false
}
if MM != 2 {
    if(MM == 1 || MM == 3 || MM == 5 || MM == 7 || MM == 8 || MM == 10 || MM == 12 ){
        if(DD > 31 || DD < 1){
          isError = true  
        } 
    }else if(MM == 4 || MM == 6 || MM == 9 || MM == 11){
        if(DD > 30 || DD < 1){
          isError = true  
        }
    }else{
      isError = true  
    }
}else{
    if(isLeap){
        if(DD > 29 || DD < 1){
            isError = true
        }
    }else{
        if(DD > 28 || DD < 1){
            isError = true
        }
    }
}

if(isError){
    print("error")
}else{
    for(var i = 2 ; i <= MM ; i++ ){
        day+=month[i-2]
    }
    for(var j = 2 ; j <= DD ; j++){
        day++
    }
    if(YYYY > now){
        repeat {
            if((MM == 2 && DD == 29) || MM > 2 ){
                now++
            }
            if(now % 400 == 0 || (now % 4 == 0 && now % 100 != 0)){
                day += 2
            }else{
                day++
            }   
            if(MM == 1 || (MM == 2 && DD < 29)){
                now++
            }
        } while(YYYY != now)
    }else if (YYYY < now){
        repeat {
            if(MM == 1 || (MM == 2 && DD < 29)){
                now--
            }
            if(now % 400 == 0 || (now % 4 == 0 && now % 100 != 0)){
                day -= 2
            }else{
                day--
            }
            if(day < 7){
                day += 7
            }
            if((MM == 2 && DD == 29) || MM > 2 ){
                now--
            }
        } while(YYYY != now)
    }
    
    day = day % 7
    
    switch day {
        case 0:
            print("星期日")
        case 1:
            print("星期一")
        case 2:
            print("星期二")
        case 3:
            print("星期三")
        case 4:
            print("星期四")
        case 5:
            print("星期五")
        case 6:
            print("星期六")
        default:
            print("")
    }
}