//
//  ViewController.swift
//  Calculator
//
//  Created by Dien-Ju, Lin on 5/18/16.
//  Copyright © 2016 Dien-Ju, Lin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view, typically from a nib.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }

    @IBOutlet weak var txtDisplay: UITextField!
    var m: Model = Model()
    var isTyping: Bool = false
    
    var display: Double {
        get{
            return Double(txtDisplay.text!)!
        }
        set{
            txtDisplay.text = String(newValue)
        }
    }
    
    @IBAction func digitButton(sender: UIButton){
        
        if (!isTyping || txtDisplay.text == "0"){
            txtDisplay.text = ""
            isTyping = true
        }
        txtDisplay.text = txtDisplay.text! + String(sender.currentTitle!)
        
    }
    
    @IBAction func functionButton(sender: UIButton) {
        //print(txtDisplay.text)
        switch (sender.currentTitle!){
            case "C":
                txtDisplay.text = "0"
                m = Model()
            case "A":
                txtDisplay.text = "u0324002林典儒"
            
            default:
                if (Double(txtDisplay.text!) != nil){
                    m.setNumber(display)
                    m.setOp(sender.currentTitle!)
                    display = m.displayNumber
                }
        }
        isTyping = false
    }

}

