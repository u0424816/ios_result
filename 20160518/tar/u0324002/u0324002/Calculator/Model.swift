//
//  Model.swift
//  Calculator
//
//  Created by Dien-Ju, Lin on 5/24/16.
//  Copyright © 2016 Dien-Ju, Lin. All rights reserved.
//

import Foundation

class Model{
    
    private var temp: Double = 0.0
    
    func setNumber(n: Double){
        temp = n
    }
    
    func setOp(op: String){
        switch (op){
            case "π":
                temp = M_PI
            case "√":
                temp = sqrt(temp)
            case "Exp":
                temp = M_E
            default:
                temp = 0.0
        }
    }
    
    var displayNumber: Double{
        get{
            return temp
        }
    }
    
}