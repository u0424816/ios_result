//
//  File.swift
//  test123
//
//  Created by nkfust02_2 on 2016/5/18.
//  Copyright © 2016年 nkfust02_2. All rights reserved.
//

import Foundation

class TestModel{
    private var temp = 0.0
    
    
    func setOperand(operand:Double){
        temp = operand
    }
    func performOperation(symbol: String){
        switch symbol{
        case "拍" : temp = M_PI
        case "根" : temp = sqrt(temp)
        case "E"  : temp = M_E
        default:break
        }
    
    }
    
    
    
    var result : Double{
        get{
             return temp
        }
    }
}