import Foundation
import CoreFoundation

let time = UInt32(NSDate().timeIntervalSinceReferenceDate)
srand(time)

var play : Array<Int> = []

func roll(rolldies : [Int]) -> [Int]
{
    var dies = rolldies
    
    for a in 0...3
    {
        dies.insert((Int((rand()%6+1))), atIndex : a)
    }
    
    return dies
}


func check(result : [Int]) -> [Int] 
{
    var f = result
    f = f.sort()
    var s = 0
    
    for a in 0...f.count-2
    {
        if(f[a] == f[a+1])
        {
            if(a == 0 && f[a+2] == f[a+3])
            {
                f.removeAtIndex(a+1)
                f.removeAtIndex(a)
            }
            else
            {
                f.removeAtIndex(a+1)
                f.removeAtIndex(a)
            }
            s=1
        }
        
        if(s == 1)
        {
            break
        }
        
    }
    if(s == 0)
    {
        f = []
        f = roll(f)
        f = check(f)
    }
    
    return (f)
}

var so = check(roll(play))
print ("Maker  : \(so[0]) + \(so[1]) = \(so[0]+so[1])\n")

var sp = check(roll(play))
print ("Player : \(sp[0]) + \(sp[1]) = \(sp[0]+sp[1])\n")

var sumo = 0
var sump = 0
for a in 0...1
{
    sumo += so[a]
    sump += sp[a]
}

if(sumo >= sump)
{
    print("Player Lose")
}
else
{
    print("Player Win")
}