//
//  ViewController.swift
//  testmethod
//
//  Created by nkfust09 on 2016/5/11.
//  Copyright © 2016年 nkfust09. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var temp="0"
    var num1="0"
    @IBOutlet weak var display: UILabel!
    
    
    @IBAction func showMessage(sender: UIButton) {
        
        
        
        if(temp=="0")
        {
            temp=""
        }
        temp+=sender.currentTitle!
        display.text=temp
        
        
    }
    
    var displayValue: String{
        get {
            return display.text!
        }
        set {
            display.text=String(newValue)
        }
    }
    
    private var model = TestModel()
    
    @IBAction func math(sender: UIButton) {
        
        model.setOperand(Double(displayValue)!)
        temp=""
        if let mathSymbol = sender.currentTitle {
            model.performOperation(mathSymbol)
        }
        
        displayValue = model.result
        
    }
}

