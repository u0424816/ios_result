import Foundation
import CoreFoundation

let time = UInt32(NSDate().timeIntervalSinceReferenceDate)
srand(time)

var play : Array<Int> = []

func roll(rolldies : [Int]) -> [Int]
{
    var dies = rolldies
    
    for a in 0...3
    {
        dies.insert((Int((rand()%6+1))), atIndex : a)
    }
    
    return dies
}


func check(result : [Int]) -> [Int] 
{
    var f = result
    f = f.sort()
    var s = 0
    
    for a in 0...f.count-2
    {
        if(f[a] == f[a+1])
        {
            if(a == 0 && f[a+2] == f[a+3])
            {
                f.removeAtIndex(a+1)
                f.removeAtIndex(a)
            }
            else
            {
                f.removeAtIndex(a+1)
                f.removeAtIndex(a)
            }
            s=1
        }
        
        if(s == 1)
        {
            break
        }
        
    }
    if(s == 0)
    {
        f = []
        f = roll(f)
        f = check(f)
    }
    
    return (f)
}

var so = check(roll(play))
print ("Maker   : \(so[0]) + \(so[1]) = \(so[0]+so[1])\n\n")
var sumo = so[0]+so[1]

var sp : [[Int]] = [[],[],[],[]]
var sump = 0
var ww : Array<Int> = []
for a in 0...3
{
    sp[a] = check(roll(play))
    sump = sp[a][0]+sp[a][1]
    print("Player\(a+1) : \(sp[a][0]) + \(sp[a][1]) = \(sump)\n")
    
    if(sumo >= sump)
    {
        ww.append(1)
    }
    else
    {
        ww.append(0)
    }
    
}

print("\n")
var win = ""
var lose = ""
for a in 0...ww.count-1
{
    if(ww[a] == 0)
    {
        win += "Player\(a+1)  "
    }
    if(ww[a] == 1)
    {
        lose += "Player\(a+1)  "
    }
}

if(win != "")
{
    print("The Winner : \(win)\n")
}
if(lose != "")
{
    print("The Loser  : \(lose)\n")
}