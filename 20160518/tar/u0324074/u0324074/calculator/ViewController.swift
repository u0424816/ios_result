//
//  ViewController.swift
//  calculator
//
//  Created by 郭哲宇 on 5/23/16.
//  Copyright © 2016 mis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var model=Model()
    @IBOutlet weak var display: UILabel!
    @IBAction func setDigit(sender:UIButton){
    model.setDigit(sender.currentTitle!)
    display.text=model.n
    }
    
    
    @IBAction func otherKey(sender:UIButton){
        model.setOper(sender.currentTitle!)
        display.text=model.n
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

