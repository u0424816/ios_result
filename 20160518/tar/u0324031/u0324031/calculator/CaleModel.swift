//
//  CaleModel.swift
//  calculator
//
//  Created by nkfust08 on 2016/5/18.
//  Copyright © 2016年 nkfust. All rights reserved.
//

import Foundation

class CaleModel {
    
    private var tmp = 0.0
    
    func setDigit(digit: Double){
        tmp = digit
    }
    
    func perforOperation(symbol: String){
        switch symbol {
        case "C": tmp = 0
        case "π": tmp = M_PI
        case "√": tmp = sqrt(tmp)
        case "℮": tmp = M_E
        /* case "+": tmp = 0
        case "-": tmp = 0
        case "X": tmp = 0
        case "/": tmp = 0
        */
        default: break
        }
    }
    var result: Double {
        get {
            return tmp
        }
    }
    
}