//
//  symbolModel.swift
//  20160518
//
//  Created by nkfust03 on 2016/5/25.
//  Copyright © 2016年 u0324004. All rights reserved.
//

import Foundation
class symbolModel{
    private var tmp = 0.0
    func setOperand(operand : Double){
        tmp = operand
    }
    func performOperation(symbol : String){
        switch symbol {
        case "C" : tmp = 0
        case "PI" : tmp = M_PI
        case "sqr" : tmp = sqrt(tmp)
        case "e" : tmp = M_E
        default : break
        }
    }
    func displaySTID() -> String{
        return "0324004 : 李婉瑄"
    }
    var result : String{
        get{
            if(tmp % 1 == 0){
                return String(Int(tmp))
            }else{
                return String(tmp)
            }
        }
    }
}