//
//  ViewController.swift
//  20160518
//
//  Created by nkfust03 on 2016/5/25.
//  Copyright © 2016年 u0324004. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private var model = symbolModel()
    private var isFirstNumber = true
    
    @IBOutlet weak var display: UILabel!
    var displayValue : Double {
        get{
            return Double(display.text!)!
        }
    }
    

    @IBAction func clickNumber(sender: UIButton) {
        let number = sender.currentTitle!
        if(display.text == "0"){
            isFirstNumber = true
        }
        if(isFirstNumber){
                display.text = number
                isFirstNumber = false
        }else{
            display.text = display.text! + number
        }
    }
    
    @IBAction func clicksymbol(sender: UIButton) {
        let symbol = sender.currentTitle!
        if(display.text == model.displaySTID()){
            model.setOperand(0.0)
        }else{
            model.setOperand(displayValue)
        }
        model.performOperation(symbol)
        isFirstNumber = true
        display.text = model.result
    }
    
    @IBAction func clickA(sender: UIButton) {
        display.text = model.displaySTID()
        isFirstNumber = true
    }
}

