//
//  ViewController.swift
//  test0511
//
//  Created by nkfust15 on 2016/5/11.
//  Copyright © 2016年 nkfust15. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    //@IBAction func showMessage(){
    //    let Alertmessage = UIAlertController(title: "first", message: "app", preferredStyle: UIAlertControllerStyle.Alert)
        
        
    //}
        
    var showing : Bool = false
    func num_in(num:String){
        if(num=="0"){
            if(showing){
                lbl_Out.text = lbl_Out.text!+num
            }else{
                lbl_Out.text = num
            }
        }else{
            if(showing){
                lbl_Out.text = lbl_Out.text!+num
            }else{
                lbl_Out.text = num
                showing = true
            }
            
        }
    }
    
    @IBOutlet weak var lbl_Out: UILabel!
    @IBAction func btn_press(sender: UIButton) {
        let in_char : String = sender.currentTitle!
        switch in_char {
        case "1","2","3","4","5","6","7","8","9","0":
            num_in(in_char)
            print (in_char)
            break
        case "C":
            lbl_Out.text="0"
            showing = false
            break
        case "A":
            lbl_Out.text = "0324057:李柏陞"
            showing = false
        default:
            break
        }
        
    }
    var displayValue: Double{
        get{
            return Double(lbl_Out.text!)!
        }
        set{
            lbl_Out.text = String(newValue)
        }
    }
    
    private var calc =  CalcModel()
    
    
    @IBAction func calc_press(sender: UIButton) {
        if(showing){
            calc.setOperand(displayValue)
            showing = false
        }
        if let mathSymbol = sender.currentTitle{
            calc.performOperation(mathSymbol)
        }
        displayValue = calc.result
        lbl_Out.text = String(displayValue)
    }
    
    }

