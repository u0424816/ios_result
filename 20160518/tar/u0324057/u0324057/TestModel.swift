//
//  TestModel.swift
//  test0511
//
//  Created by nkfust15 on 2016/5/18.
//  Copyright © 2016年 nkfust15. All rights reserved.
//

import Foundation

class CalcModel {
    var operand:Double = 0.0
    var symbol:String = ""
    func setOperand(operand: Double){
        self.operand = operand
    }
    func performOperation(symbol: String){
        switch symbol {
        case "π":
            self.operand=M_PI
            break
        case "√":
            self.operand=sqrt(self.operand)
            break
        case "e":
            self.operand=M_E
        default :
            break
        }
    }
    var result: Double{
        get{
            return operand
        }
    }
}