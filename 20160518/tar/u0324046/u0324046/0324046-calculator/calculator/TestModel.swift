//
//  TestModel.swift
//  calculator
//
//  Created by nkfust11 on 2016/5/18.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import Foundation
class TestModel{
    private var name :String = "0324046 - 徐琨淋"
    private var value : String="0"
    private var temp : String="0"
    private var stringtype = false
    
    func setOperand(operand: String){
        temp=operand
    }
    func performOperation(symbol: String){
        stringtype = false
        switch symbol {
        case "+":
            value=String(Double(value)!+Double(temp)!)
            break
        case "-":
            value=String(Double(value)!-Double(temp)!)
            break
        case "*":
            value=String(Double(value)!*Double(temp)!)
            break
        case "/":
            value=String(Double(value)!/Double(temp)!)
            break
        case "兀":
            value=String(M_PI)
        case "√":
            value=String(sqrt(Double(temp)!))
        case "C":
            value="0"
        case "A":
            stringtype=true
        default:
            break
        }
    }
    
    var result:String{
        get{
            return (stringtype ? name: String(value))
        }
    }
}
