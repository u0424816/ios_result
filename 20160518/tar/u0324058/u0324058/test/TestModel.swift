//
//  TestModel.swift
//  test
//
//  Created by nkfust06 on 2016/5/18.
//  Copyright © 2016年 nkfust06. All rights reserved.
//

import Foundation
class  TestModel {
    private var temp = 0.0
    func setOperand(operand: Double){
    temp = operand
    }
    func performOperation(symbol: String){
        switch symbol {
        case "c": temp = 0.0
        case "π": temp = M_PI
        case "e": temp = M_E
        case "√": temp = sqrt(temp)
        default : break
        }}
    var result : Double{
        get  {
            return temp
        }
    }}
