///Users/leviathan/Desktop/ios/File.swift
//  File.swift
//  Test1
//
//  Created by Leviathan on 2016/5/18.
//  Copyright © 2016年 Leviathan. All rights reserved.
//

import Foundation

class TestModel {
    
    private var temp = 0.0
    
    
    func setOperand(operand: Double){
        temp = operand
    }
    func performOperation(symbol: String){
        
        switch symbol {
        case "C": temp = 0
        case "pi": temp = M_PI
        case "sqrt": temp = sqrt(temp)
        case "e": temp = M_E
        default: break
        }
        
    }
    var result: Double {
        get {
            return temp
        }
    }
}