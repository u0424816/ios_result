//
//  ViewController.swift
//  Test1
//
//  Created by Leviathan on 2016/5/11.
//  Copyright © 20/16年 Leviathan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var  msg2 = ""
    @IBOutlet weak var display: UILabel!
    
    var 正在輸入 = false
    @IBAction func ShowMessage(sender: UIButton) {
        let msg = sender.currentTitle!
        
        if 正在輸入 {
            let DisPlay = display.text!
            
            if  display.text == "0" {
                if msg == "0"
                {msg2 = ""}
                else
                {msg2 = ""
                    msg2 += msg
                    display.text = msg2}
            }else{
                display.text = DisPlay + msg}
       } else {
            display.text = msg
        }
        
        正在輸入 = true
    }
    
    var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            display.text = String(newValue)
        }
    }
    
    
    
    private var model = TestModel()
    
    
    @IBAction func touchKeyC(sender: UIButton) {
        if 正在輸入 {
            model.setOperand(displayValue)
            正在輸入 = false
        }
        if let mathSymbol = sender.currentTitle {
            model.performOperation(mathSymbol)
        }
        displayValue = model.result
    }
}



