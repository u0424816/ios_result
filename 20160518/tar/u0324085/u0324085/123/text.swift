//
//  text.swift
//  123
//
//  Created by nkfust18 on 2016/5/18.
//  Copyright © 2016年 nkfust18. All rights reserved.
//

import Foundation

class text{
    private var temp = 0.0
    func setOperand(operand: Double){temp = operand}
    func performOperation(symbol: String) {
        switch symbol {
            case "根號": temp = sqrt(temp)
            case "拍": temp = M_PI
            case "c": temp = 0
            case "a": temp = 0324085
            case "e": temp = M_E
        default: break
        }
    }
    var result: Double{
        get{
            return temp
        }
    }
}