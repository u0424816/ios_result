//
//  Faceit.swift
//  test0608
//
//  Created by nkfust05 on 2016/6/8.
//  Copyright © 2016年 nkfust05. All rights reserved.
//

import UIKit

class Faceit:  UIView{

    override func drawRect(rect: CGRect) {
        //drawing code 
        let skullRadius = min(bounds.size.width,bounds.size.height) / 3
        let skullCenter = CGPoint(x:bounds.midX,y: bounds.midY)
        
        let skull = UIBezierPath(arcCenter: skullCenter ,radius: skullRadius, startAngle: 0.0, endAngle:CGFloat(M_PI*2),clockwise:false)

        skull.lineWidth = 5.0
        UIColor.redColor().set()
        skull.stroke()
        
        
        let skullRadius1 = min(bounds.size.width,bounds.size.height)/20
        let skullCenter1 = CGPoint(x:bounds.midX-60,y: bounds.midY-60)
        
        let skull1 = UIBezierPath(arcCenter: skullCenter1 ,radius: skullRadius1, startAngle: 0.0, endAngle:CGFloat(M_PI*2),clockwise:false)
        
        skull1.lineWidth = 1.0
        UIColor.brownColor().set()
        skull1.stroke()
        
        let skullRadius2 = min(bounds.size.width,bounds.size.height)/20
        let skullCenter2 = CGPoint(x:bounds.midX+60,y: bounds.midY-60)
        
        let skull2 = UIBezierPath(arcCenter: skullCenter2 ,radius: skullRadius2, startAngle: 0.0, endAngle:CGFloat(M_PI*2),clockwise:true)
        
        skull2.lineWidth = 1.0
        UIColor.brownColor().set()
        skull2.stroke()
        
        
        
        
        
        let skullRadius9 = min(bounds.size.width,bounds.size.height)/10
        let skullCenter9 = CGPoint(x:bounds.midX,y: bounds.midY+50)
        
        let skull9 = UIBezierPath(arcCenter: skullCenter9 ,radius: skullRadius9, startAngle: 0.0, endAngle:CGFloat(M_PI*1),clockwise:true)
        
        skull9.lineWidth = 1.0
        UIColor.blackColor().set()
        skull9.stroke()
        
        
        //let line = UIBezierPath()
        //line.moveToPoint(CGPointMake(200,350))
        //line.addLineToPoint(CGPointMake(120, 350))
        //line.lineWidth = 10.0
        //UIColor.redColor().set()
        //line.stroke()
        
        
        
        
        let skullRadius3 = min(bounds.size.width,bounds.size.height)/10
        let skullCenter3 = CGPoint(x:bounds.midX-70,y: bounds.midY-220)
        
        let skull3 = UIBezierPath(arcCenter: skullCenter3 ,radius: skullRadius3, startAngle: 0.0, endAngle:CGFloat(M_PI*2),clockwise:false)
        
        skull3.lineWidth = 3.0
        UIColor.blueColor().set()
        skull3.stroke()
        
        let skullRadius5 = min(bounds.size.width,bounds.size.height)/10
        let skullCenter5 = CGPoint(x:bounds.midX+70,y: bounds.midY-220)
        
        
        let skull5 = UIBezierPath(arcCenter: skullCenter5 ,radius: skullRadius5, startAngle: 0.0, endAngle:CGFloat(M_PI*2),clockwise:false)
        
        skull5.lineWidth = 3.0
        UIColor.redColor().set()
        skull5.stroke()
        
        let skullRadius6 = min(bounds.size.width,bounds.size.height)/10
        let skullCenter6 = CGPoint(x:bounds.midX,y: bounds.midY-220)
        
        let skull6 = UIBezierPath(arcCenter: skullCenter6 ,radius: skullRadius6, startAngle: 0.0, endAngle:CGFloat(M_PI*2),clockwise:false)
        
        skull6.lineWidth = 3.0
        UIColor.blackColor().set()
        skull6.stroke()
        
        let skullRadius7 = min(bounds.size.width,bounds.size.height)/10
        let skullCenter7 = CGPoint(x:bounds.midX-35,y: bounds.midY-190)
        
        let skull7 = UIBezierPath(arcCenter: skullCenter7 ,radius: skullRadius7, startAngle: 0.0, endAngle:CGFloat(M_PI*2),clockwise:false)
        
        skull7.lineWidth = 3.0
        UIColor.yellowColor().set()
        skull7.stroke()
        
        let skullRadius8 = min(bounds.size.width,bounds.size.height)/10
        let skullCenter8 = CGPoint(x:bounds.midX+35,y: bounds.midY-190)
        
        let skull8 = UIBezierPath(arcCenter: skullCenter8 ,radius: skullRadius8, startAngle: 0.0, endAngle:CGFloat(M_PI*2),clockwise:false)
        
        skull8.lineWidth = 3.0
        UIColor.greenColor().set()
        skull8.stroke()
        

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
}
