//
//  FaceIt.swift
//  test0608
//
//  Created by nkfust14 on 2016/6/8.
//  Copyright © 2016年 nkfust14. All rights reserved.
//

import UIKit

class FaceIt: UIView {

    override func drawRect(rect: CGRect) {
        // Drawing code
        let faceR=min(bounds.height , bounds.width)/2
        drawCircle(faceR,  cp: CGPoint(x: bounds.midX , y: bounds.midY), sa: 0.0,ea: CGFloat(M_PI*2),color: UIColor.yellowColor(),s:false,isfill: true)
        drawCircle(faceR*0.035, cp: CGPoint(x:bounds.width/3,y:bounds.height*0.4),sa: 0.0,ea: CGFloat(M_PI*2), color: UIColor.blackColor(),s:false,isfill: true)
        drawCircle(faceR*0.035, cp: CGPoint(x:bounds.width*0.67,y:bounds.height*0.4),sa: 0.0,ea: CGFloat(M_PI*2), color: UIColor.blackColor(),s:false,isfill: true)
        drawCircle(faceR*0.035, cp: CGPoint(x:bounds.width*0.67,y:bounds.height*0.55),sa: 0.0,ea: CGFloat(M_PI), color: UIColor.blueColor(),s:true,isfill: true)
        drawLine(CGPoint(x:bounds.width*0.67+faceR*0.035,y:bounds.height*0.55), p2: CGPoint(x:bounds.width*0.67,y:bounds.height*0.5),color: UIColor.blueColor())
        drawLine(CGPoint(x:bounds.width*0.67-faceR*0.035,y:bounds.height*0.55), p2: CGPoint(x:bounds.width*0.67,y:bounds.height*0.5),color: UIColor.blueColor())
        drawCircle(faceR,  cp: CGPoint(x: bounds.midX , y: bounds.midY*0.9), sa: CGFloat(M_PI*0.15),ea: CGFloat(M_PI*0.85),color: UIColor.blackColor(),s:true,isfill: false)
    }
    
    func drawCircle(r:CGFloat,cp:CGPoint,sa:CGFloat,ea:CGFloat, color:UIColor,s:Bool,isfill:Bool){
        let skull = UIBezierPath(arcCenter: cp, radius: r, startAngle: sa, endAngle: ea, clockwise: s)
        skull.lineWidth = 3.0
        color.set()
        skull.stroke()
        if(isfill){
            skull.fill()
        }
    }
    func drawLine(p1:CGPoint,p2:CGPoint,color:UIColor){
        var aPath = UIBezierPath()
        
        aPath.moveToPoint(p1)
        aPath.addLineToPoint(p2)
        
        aPath.lineWidth=3.0
        aPath.closePath()
        color.set()
        aPath.stroke()
        aPath.fill()
    }
}
