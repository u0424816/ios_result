import UIKit

class FaceIt: UIView {
    override func drawRect(rect: CGRect) {
        //Drawing code
        
        
        //奧運藍環
        let blueRadius = min(bounds.size.width,bounds.size.height) / 7
        let blueCenter = CGPoint(x: bounds.midX - 145 , y: bounds.midY - 100)
        
        
        let blue = UIBezierPath(arcCenter: blueCenter, radius: blueRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        blue.lineWidth = 5.0
        UIColor.blueColor().set()
        blue.stroke()
        
        
        //奧運黃環
        let yellowRadius = min(bounds.size.width,bounds.size.height) / 7
        let yellowCenter = CGPoint(x: bounds.midX - 77.5 , y: bounds.midY - 40)
        
        
        let yellow = UIBezierPath(arcCenter: yellowCenter, radius: yellowRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        yellow.lineWidth = 5.0
        UIColor.orangeColor().set()
        yellow.stroke()
        
        
        //奧運黑環
        let blackRadius = min(bounds.size.width,bounds.size.height) / 7
        let blackCenter = CGPoint(x: bounds.midX - 10, y: bounds.midY - 100)
        
        
        let black = UIBezierPath(arcCenter: blackCenter, radius: blackRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        black.lineWidth = 5.0
        UIColor.blackColor().set()
        black.stroke()
        
        
        //奧運綠環
        let greenRadius = min(bounds.size.width,bounds.size.height) / 7
        let greenCenter = CGPoint(x: bounds.midX + 57.5 , y: bounds.midY - 40)
        
        
        let green = UIBezierPath(arcCenter: greenCenter, radius: greenRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        green.lineWidth = 5.0
        UIColor.greenColor().set()
        green.stroke()
        
        
        //奧運紅環
        let redRadius = min(bounds.size.width,bounds.size.height) / 7
        let redCenter = CGPoint(x: bounds.midX + 125 , y: bounds.midY - 100)
        
        
        let red = UIBezierPath(arcCenter: redCenter, radius: redRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        red.lineWidth = 5.0
        UIColor.redColor().set()
        red.stroke()
        
        
        //臉部
        let skullRadius = min(bounds.size.width,bounds.size.height) / 8 //控制圓的半徑
        let skullCenter = CGPoint(x: bounds.midX , y: bounds.midY + 70) //圓的位置
        
        
        let skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        skull.lineWidth = 5.0
        UIColor.brownColor().set()
        skull.stroke()
        skull.fill()
        
        
        //左眼
        let ReyeRadius = min(bounds.size.width,bounds.size.height) / 40
        let ReyeCenter = CGPoint(x: bounds.midX - 25 , y: bounds.midY + 50)
        
        
        let Reye = UIBezierPath(arcCenter: ReyeCenter, radius: ReyeRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        Reye.lineWidth = 5.0
        UIColor.whiteColor().set()
        Reye.stroke()
        Reye.fill()
        
        //右眼
        let LeyeRadius = min(bounds.size.width,bounds.size.height) / 40
        let LeyeCenter = CGPoint(x: bounds.midX + 25 , y: bounds.midY + 50)
        
        
        let Leye = UIBezierPath(arcCenter: LeyeCenter, radius: LeyeRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        Leye.lineWidth = 5.0
        UIColor.whiteColor().set()
        Leye.stroke()
        Leye.fill()
        
        //眼珠
        let Weye1Radius = min(bounds.size.width,bounds.size.height) / 60
        let Weye1Center = CGPoint(x: bounds.midX + 25 , y: bounds.midY + 55)
        
        
        let Weye = UIBezierPath(arcCenter: Weye1Center, radius: Weye1Radius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        Weye.lineWidth = 5.0
        UIColor.blackColor().set()
        Weye.stroke()
        Weye.fill()
        
        
        //眼珠
        let Weye2Radius = min(bounds.size.width,bounds.size.height) / 60
        let Weye2Center = CGPoint(x: bounds.midX - 25 , y: bounds.midY + 55)
        
        
        let Weye2 = UIBezierPath(arcCenter: Weye2Center, radius: Weye2Radius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        Weye2.lineWidth = 5.0
        UIColor.blackColor().set()
        Weye2.stroke()
        Weye2.fill()
        
        
        //微笑
        let smileRadius = min(bounds.size.width,bounds.size.height) / 14
        let smileCenter = CGPoint(x: bounds.midX , y: bounds.midY + 80 )
        
        let smile = UIBezierPath(arcCenter: smileCenter, radius: smileRadius, startAngle: 0.0 ,endAngle: CGFloat(M_PI * 1), clockwise: true) // clockwise <順時鐘逆時鐘
        smile.lineWidth = 5.0
        UIColor.redColor().set()
        smile.stroke()
        smile.fill()
        
    }
    
    
}