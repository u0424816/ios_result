//
//  FaceIt.swift
//  test0608
//
//  Created by nkfust04 on 2016/6/8.
//  Copyright © 2016年 nkfust04. All rights reserved.
//

import UIKit

class FaceIt: UIView {

    override func drawRect(rect: CGRect) {
        // Drawing code
        
        let skullRadius = min(bounds.size.width, bounds.size.height) / 2
        let skullCenter = CGPoint(x: bounds.midX , y: bounds.midY)
        var skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        skull.lineWidth = 3.0
        UIColor.whiteColor().set()
        skull.stroke()
        UIColor.blueColor().set()
        skull.fill()
        
        
        let eye1Center = CGPoint(x: bounds.midX - 70 , y: bounds.midY - 50)
        skull = UIBezierPath(arcCenter: eye1Center, radius: 50, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        UIColor.whiteColor().set()
        skull.stroke()
        skull.fill()
        
        let eye2Center = CGPoint(x: bounds.midX + 70 , y: bounds.midY - 50)
        skull = UIBezierPath(arcCenter: eye2Center, radius: 50, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        UIColor.whiteColor().set()
        skull.stroke()
        skull.fill()
        
        
        /*
        let line = UIBezierPath()
        line.moveToPoint(CGPointMake(40 , 40))
        line.addLineToPoint(CGPointMake(60, 60))
        line.lineWidth = 5.0
        UIColor.blackColor().set()
        line.stroke()
    */
 }

    
    
}
