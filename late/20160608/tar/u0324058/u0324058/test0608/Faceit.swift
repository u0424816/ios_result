//
//  Faceit.swift
//  test0608
//
//  Created by nkfust06 on 2016/6/8.
//  Copyright © 2016年 nkfust06. All rights reserved.
//

import UIKit

class Faceit: UIView {

    
    override func drawRect(rect: CGRect){
    // Drawing code 

        let tearRadius = min(bounds.size.width, bounds.size.height)/8
        let tearCenter = CGPoint(x: bounds.midX/0.9,y: bounds.midY/1.08)
        let tear1Center = CGPoint(x: bounds.midX/1.8,y: bounds.midY/1.08)
        let tear2Center = CGPoint(x: bounds.midX/0.6,y: bounds.midY/1.08)
        let tear3Center = CGPoint(x: bounds.midX/0.7,y: bounds.midY/0.9)
        let tear4Center = CGPoint(x: bounds.midX/1.2,y: bounds.midY/0.9)
             let tear5Radius = min(bounds.size.width, bounds.size.height)/6.5
        let tear5Center = CGPoint(x: bounds.midX/0.91,y: bounds.midY/1.9)
        
        
        let line = UIBezierPath()
        line.moveToPoint(CGPointMake(175, 50))
        line.addLineToPoint(CGPointMake(125, 150))
        UIColor.blueColor().set()
        line.lineWidth = 5.0
        line.stroke()
        
        let line1 = UIBezierPath()
        line1.moveToPoint(CGPointMake(175, 50))
        line1.addLineToPoint(CGPointMake(225, 150))
        UIColor.blueColor().set()
        line1.lineWidth = 5.0
        line1.stroke()
        
        let tear5 = UIBezierPath( arcCenter: tear5Center , radius: tear5Radius, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI) , clockwise: true)
        tear5.lineWidth = 4.0
        UIColor.blueColor().set()
        tear5.stroke()
        
        let tear = UIBezierPath( arcCenter: tearCenter , radius: tearRadius, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2) , clockwise: false)
        tear.lineWidth = 4.0
        UIColor.blackColor().set()
        tear.stroke()
        
        let tear1 = UIBezierPath( arcCenter: tear1Center , radius: tearRadius, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2) , clockwise: false)
              tear1.lineWidth = 4.0
        UIColor.blueColor().set()
        tear1.stroke()
        let tear2 = UIBezierPath( arcCenter: tear2Center , radius: tearRadius, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2) , clockwise: false)
        tear2.lineWidth = 4.0
        UIColor.redColor().set()
        tear2.stroke()
        let tear3 = UIBezierPath( arcCenter: tear3Center , radius: tearRadius, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2) , clockwise: false)
        tear3.lineWidth = 4.0
        UIColor.greenColor().set()
        tear3.stroke()
        let tear4 = UIBezierPath( arcCenter: tear4Center , radius: tearRadius, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2) , clockwise: false)
        tear4.lineWidth = 4.0
        UIColor.yellowColor().set()
        tear4.stroke()

    }
    
}
