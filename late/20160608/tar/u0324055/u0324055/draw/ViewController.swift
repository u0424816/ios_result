//
//  tony.swift
//  20160608
//
//  Created by nkfust01 on 2016/6/8.
//  Copyright © 2016年 nkfust01. All rights reserved.
//

import UIKit

class tony: UIView {
    
        override func drawRect(rect: CGRect) {
        let skullRadius = min(bounds.size.width,bounds.size.height) / 3 //R
        let skullCenter = CGPoint(x: bounds.midX,y: bounds.midY) //中心點
        let skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius,
            startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: true)
        skull.lineWidth = 5.0 //線的粗細
        UIColor.blueColor().set()
        skull.stroke()
       
            
        let skullRadius1 = min(bounds.size.width,bounds.size.height) / 12 //R
        let skullCenter1 = CGPoint(x: bounds.midX/1.35,y: bounds.midY/1.1) //中心點
        let skull1 = UIBezierPath(arcCenter: skullCenter1, radius: skullRadius1,
            startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: true)
        skull1.lineWidth = 5.0 //線的粗細
        UIColor.blueColor().set()
        skull1.stroke()
        UIColor.yellowColor().set()
        skull1.fill()
            
        let skullRadius3 = min(bounds.size.width,bounds.size.height) / 12 //R
        let skullCenter3 = CGPoint(x: bounds.midX*1.3,y: bounds.midY/1.1) //中心點
        let skull3 = UIBezierPath(arcCenter: skullCenter3, radius: skullRadius3,
                                      startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: true)
        skull3.lineWidth = 5.0 //線的粗細
        UIColor.blueColor().set()
        skull3.stroke()
            UIColor.yellowColor().set()
            skull3.fill()
            
        let skullRadius4 = min(bounds.size.width,bounds.size.height) / 12 //R
        let skullCenter4 = CGPoint(x: bounds.midX,y: bounds.midY*1.2) //中心點
        let skull4 = UIBezierPath(arcCenter: skullCenter4, radius: skullRadius4,
                                      startAngle: 0.0, endAngle: CGFloat(M_PI*1), clockwise: true)
        skull4.lineWidth = 5.0 //線的粗細
        UIColor.blueColor().set()
        skull4.stroke()
        
            
          //five circle
            
            let skullRadius5 = min(bounds.size.width,bounds.size.height) / 12 //R
            let skullCenter5 = CGPoint(x: bounds.midX/2,y: bounds.midY*1.7) //中心點
            let skull5 = UIBezierPath(arcCenter: skullCenter5, radius: skullRadius5,
                        startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: true)
            skull5.lineWidth = 3.0 //線的粗細
            UIColor.blueColor().set()
            skull5.stroke()
            
            let skullRadius6 = min(bounds.size.width,bounds.size.height) / 12 //R
            let skullCenter6 = CGPoint(x: bounds.midX/1.6,y: bounds.midY*1.8) //中心點
            let skull6 = UIBezierPath(arcCenter: skullCenter6, radius: skullRadius6,
                        startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: true)
            skull6.lineWidth = 3.0 //線的粗細
            UIColor.yellowColor().set()
            skull6.stroke()
       
            
            let skullRadius7 = min(bounds.size.width,bounds.size.height) / 12 //R
            let skullCenter7 = CGPoint(x: bounds.midX/1.3,y: bounds.midY*1.7) //中心點
            let skull7 = UIBezierPath(arcCenter: skullCenter7, radius: skullRadius7,
                                      startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: true)
            skull7.lineWidth = 3.0 //線的粗細
            UIColor.purpleColor().set()
            skull7.stroke()
            
            let skullRadius8 = min(bounds.size.width,bounds.size.height) / 12 //R
            let skullCenter8 = CGPoint(x: bounds.midX,y: bounds.midY*1.7) //中心點
            let skull8 = UIBezierPath(arcCenter: skullCenter8, radius: skullRadius8,
                                      startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: true)
            skull8.lineWidth = 3.0 //線的粗細
            UIColor.greenColor().set()
            skull8.stroke()
        
            let skullRadius9 = min(bounds.size.width,bounds.size.height) / 12 //R
            let skullCenter9 = CGPoint(x: bounds.midX/1.1,y: bounds.midY*1.8) //中心點
            let skull9 = UIBezierPath(arcCenter: skullCenter9, radius: skullRadius9,
                                      startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: true)
            skull9.lineWidth = 3.0 //線的粗細
            UIColor.redColor().set()
            skull9.stroke()
    }
    
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
}