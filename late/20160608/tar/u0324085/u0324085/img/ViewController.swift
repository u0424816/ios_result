//
//  ViewController.swift
//  img
//
//  Created by nkfust18 on 2016/6/15.
//  Copyright © 2016年 nkfust18. All rights reserved.
//

import UIKit

class ViewController: UIView {

    override func drawRect(rect: CGRect){
        let skullRadius = min(bounds.size.width, bounds.size.height) / 2
        let skullCenter = CGPoint(x: bounds.midX , y: bounds.midY)
        var skull = UIBezierPath(arcCenter: skullCenter,radius : skullRadius,startAngle: 0.0,endAngle: CGFloat(M_PI * 2),clockwise: false)
        skull.lineWidth = 3.0
        UIColor.whiteColor().set()
        skull.stroke()
        UIColor.blueColor().set()
        skull.fill()
        
        let eye1Center = CGPoint(x: bounds.midX - 70 , y: bounds.midY - 50)
        skull = UIBezierPath(arcCenter: eye1Center, radius: 50, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        skull.stroke()
        skull.fill()
        
        let eye2Center = CGPoint(x: bounds.midX + 70 , y: bounds.midY - 50)
        skull = UIBezierPath(arcCenter: eye2Center, radius: 50, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        skull.stroke()
        skull.fill()
  
    }
    
    
}


