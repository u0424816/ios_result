//
//  Facelt.swift
//  testgammm
//
//  Created by 林奕任 on 2016/6/8.
//  Copyright © 2016年 林奕任. All rights reserved.
//

import UIKit

class Facelt: UIView {

   override func drawRect(rect: CGRect) {
   
    let skullRadius = min(bounds.size.width,bounds.size.height) / 2
    
    
    let skullCenter = CGPoint(x:bounds.midX , y:bounds.midY)
    
    let skull = UIBezierPath(arcCenter: skullCenter , radius : skullRadius , startAngle:0.0, endAngle:CGFloat(M_PI * 2),clockwise: false)
    skull.lineWidth = 5.0
    UIColor.yellowColor().set()
    skull.fill()
    skull.stroke()
    
    let eyeL = UIBezierPath(arcCenter: CGPoint(x: 130 , y: 300), radius:10,startAngle:0.0, endAngle:CGFloat(M_PI * 2),clockwise: false)
    
    UIColor.blackColor().set()
    eyeL.fill()
    eyeL.stroke()
    
    let eyeR = UIBezierPath(arcCenter: CGPoint(x: 276, y: 300), radius:10,startAngle:0.0, endAngle:CGFloat(M_PI * 2),clockwise: false)
    
    UIColor.blackColor().set()
    eyeR.fill()
    eyeR.stroke()
    
    //let mon = UIBezierPath()
   /* let path = UIBezierPath(rect: CGRectMake(110, 100, 150, 100)
    
    UIColor.blackColor().set()
    mon.fill()
    mon.stroke()*/
    let context:CGContextRef =  UIGraphicsGetCurrentContext()!
    CGContextSetLineWidth(context, 2)
    CGContextMoveToPoint(context, 183, 400);
    CGContextAddLineToPoint(context, 223, 400);
    CGContextStrokePath(context)
    
    CGContextSetLineWidth(context, 2)
    CGContextMoveToPoint(context, 330, 290);
    CGContextAddLineToPoint(context, 330, 370);
    CGContextStrokePath(context)
    
    CGContextSetLineWidth(context, 2)
    CGContextMoveToPoint(context, 340, 290);
    CGContextAddLineToPoint(context, 340, 370);
    CGContextStrokePath(context)
    
    CGContextSetLineWidth(context, 2) 
    CGContextMoveToPoint(context, 350, 290);
    CGContextAddLineToPoint(context, 350, 370);
    CGContextStrokePath(context)

    
    }
    
    

}
