//
//  Faceit.swift
//  Face
//
//  Created by nkfust08_2 on 2016/6/8.
//  Copyright © 2016年 nkfust08_2. All rights reserved.
//

import UIKit
class FaceIt: UIView {
    override func drawRect(rect: CGRect){
        let skullRadius = min (bounds.size.width, bounds.size.height)/2
        let skullRadius2 = min (bounds.size.width, bounds.size.height)/10
        let skullRadius3 = min (bounds.size.width, bounds.size.height)/10
        let skullRadius4 = min (bounds.size.width, bounds.size.height)/40
        let skullRadius6 = min (bounds.size.width, bounds.size.height)/40
        let skullRadius5 = min (bounds.size.width, bounds.size.height)/5
        let skullRadius7 = min (bounds.size.width, bounds.size.height)/40
        let skullCenter = CGPoint(x: bounds.midX, y:bounds.midY)
        let skullCenter2 = CGPoint(x: bounds.midX/2+50, y:bounds.midY/2+100)
        let skullCenter3 = CGPoint(x: bounds.midX/2+150, y:bounds.midY/2+100)
        let skullCenter4 = CGPoint(x: bounds.midX/4+150, y:bounds.midY/2+150)
        let skullCenter5 = CGPoint(x: bounds.midX/4+150, y:bounds.midY/2+250)
        let skullCenter6 = CGPoint(x: bounds.midX/2+50, y:bounds.midY/2+100)
        let skullCenter7 = CGPoint(x: bounds.midX/2+150, y:bounds.midY/2+100)
        //let skullCenter8 = CGPoint(x: bounds.midX/2+150, y:bounds.midY/2+100)

        let skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2),clockwise: false)
        let skull2 = UIBezierPath(arcCenter: skullCenter2, radius: skullRadius2, startAngle: 0.0,
                                 endAngle: CGFloat(M_PI * 2),clockwise: false)
        let skull3 = UIBezierPath(arcCenter: skullCenter3, radius: skullRadius3, startAngle: 0.0,
                                  endAngle: CGFloat(M_PI * 2),clockwise: false)
        let skull4 = UIBezierPath(arcCenter: skullCenter4, radius: skullRadius4, startAngle: 0.0,
                                  endAngle: CGFloat(M_PI * 2),clockwise: false)
        let skull5 = UIBezierPath(arcCenter: skullCenter5, radius: skullRadius5, startAngle: 0.0,
                                  endAngle: CGFloat(M_PI),clockwise: true)
        let skull6 = UIBezierPath(arcCenter: skullCenter6, radius: skullRadius6, startAngle: 0.0,
                                  endAngle: CGFloat(M_PI*2),clockwise: true)
        let skull7 = UIBezierPath(arcCenter: skullCenter7, radius: skullRadius7, startAngle: 0.0,
                                  endAngle: CGFloat(M_PI*2),clockwise: true)
        let line3 = UIBezierPath()
        line3.moveToPoint(CGPoint(x: bounds.midX/2-50, y:bounds.midY/2+300))
        line3.addLineToPoint(CGPoint(x: bounds.midX/2+50, y:bounds.midY/2+220))
        line3.lineWidth=2.0
        UIColor.blackColor().set()
        let line2 = UIBezierPath()
        line2.moveToPoint(CGPoint(x: bounds.midX/2-60, y:bounds.midY/2+200))
        line2.addLineToPoint(CGPoint(x: bounds.midX/2+50, y:bounds.midY/2+200))
        line2.lineWidth=2.0
        UIColor.blackColor().set()
        
        let line1 = UIBezierPath()
        line1.moveToPoint(CGPoint(x: bounds.midX/2-50, y:bounds.midY/2+100))
        line1.addLineToPoint(CGPoint(x: bounds.midX/2+50, y:bounds.midY/2+180))
        line1.lineWidth=2.0
        UIColor.blackColor().set()
        
        let line6 = UIBezierPath()
        line6.moveToPoint(CGPoint(x: bounds.midX/2+150, y:bounds.midY/2+220))
        line6.addLineToPoint(CGPoint(x: bounds.midX/2+250, y:bounds.midY/2+300))
        line6.lineWidth=2.0
        UIColor.blackColor().set()
        let line5 = UIBezierPath()
        line5.moveToPoint(CGPoint(x: bounds.midX/2+150, y:bounds.midY/2+200))
        line5.addLineToPoint(CGPoint(x: bounds.midX/2+270, y:bounds.midY/2+200))
        line5.lineWidth=2.0
        UIColor.blackColor().set()
        
        let line4 = UIBezierPath()
        line4.moveToPoint(CGPoint(x: bounds.midX/2+150, y:bounds.midY/2+180))
        line4.addLineToPoint(CGPoint(x: bounds.midX/2+250, y:bounds.midY/2+100))
        line4.lineWidth=2.0
        UIColor.blackColor().set()
        
        
        skull.lineWidth=5.0
        UIColor.blueColor().set()

        skull.stroke()
        UIColor.yellowColor().set()
        skull.fill()
        
        
        
        skull2.lineWidth=2.0
        UIColor.redColor().set()
        skull2.stroke()
        
        skull3.lineWidth=2.0
        UIColor.redColor().set()
        skull3.stroke()
        
        skull4.lineWidth=2.0
        UIColor.blueColor().set()
        skull4.stroke()
        
        skull5.lineWidth=2.0
        UIColor.blueColor().set()
        skull5.closePath()
        skull5.stroke()
        
        skull6.lineWidth=2.0
        UIColor.blackColor().set()
        skull6.fill()
        
        
        skull7.lineWidth=2.0
        UIColor.blackColor().set()
        skull7.fill()
        line3.stroke()
        line2.stroke()
        line1.stroke()
        line6.stroke()
        line4.stroke()
        line5.stroke()
        
        
        
        
        
    }
}