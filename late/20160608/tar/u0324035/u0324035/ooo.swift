//
//  ooo.swift
//  0608
//
//  Created by nkfust10 on 2016/6/8.
//  Copyright © 2016年 nkfust10. All rights reserved.
//

import UIKit

class ooo: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    var timer:NSTimer?
    
    
    func tickDown()
    {
        var randomRed:CGFloat = CGFloat(arc4random()) / CGFloat(UInt32.max)
        
        var randomGreen:CGFloat = CGFloat(arc4random()) / CGFloat(UInt32.max)
        
        var randomBlue:CGFloat = CGFloat(arc4random()) / CGFloat(UInt32.max)
        
        

        UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0).set()
        print("tick...")
    }
    
    override func drawRect(rect: CGRect) {
        
        var randomRed:CGFloat = CGFloat(arc4random()) / CGFloat(UInt32.max)
        
        var randomGreen:CGFloat = CGFloat(arc4random()) / CGFloat(UInt32.max)
        
        var randomBlue:CGFloat = CGFloat(arc4random()) / CGFloat(UInt32.max)
        // Drawing code
        let skullRadius = min(bounds.size.width, bounds.size.height) / 2
        let skullCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let skullCenter1 = CGPoint(x: bounds.midX/1.7, y: bounds.midY/4.75)
        let skullCenter2 = CGPoint(x: bounds.midX/0.7, y: bounds.midY/4.75)
        let skullCenter3 = CGPoint(x: bounds.midX, y: bounds.midY/4.75)
        let skullCenter4 = CGPoint(x: bounds.midX/1.25, y: bounds.midY/2.75)
        let skullCenter5 = CGPoint(x: bounds.midX/0.85, y: bounds.midY/2.75)
        
        let skullCenter6 = CGPoint(x: bounds.midX/1.7, y: bounds.midY/1.25)
        let skullCenter7 = CGPoint(x: bounds.midX/0.7, y: bounds.midY/1.25)
        
        
        let skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        skull.lineWidth = 5.0
        UIColor.blueColor().set()
        skull.stroke()
        UIColor.yellowColor().set()
        skull.fill()
        let skull1 = UIBezierPath(arcCenter: skullCenter1, radius: skullRadius / 4, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        skull1.lineWidth = 5
        UIColor.yellowColor().set()
        skull1.stroke()
        let skull2 = UIBezierPath(arcCenter: skullCenter2, radius: skullRadius / 4, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        skull2.lineWidth = 5
        UIColor.redColor().set()
        skull2.stroke()
        let skull3 = UIBezierPath(arcCenter: skullCenter3, radius: skullRadius / 4, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        skull3.lineWidth = 5
        UIColor.greenColor().set()
        skull3.stroke()
        
        let skull4 = UIBezierPath(arcCenter: skullCenter4, radius: skullRadius / 4, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        skull4.lineWidth = 5
        UIColor.blackColor().set()
        skull4.stroke()
        
        let skull5 = UIBezierPath(arcCenter: skullCenter5, radius: skullRadius / 4, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        skull5.lineWidth = 5
        UIColor.blueColor().set()
        skull5.stroke()
       
        
        let skull6 = UIBezierPath(arcCenter: skullCenter6, radius: skullRadius / 4, startAngle: 0.0, endAngle: CGFloat(M_PI*1), clockwise: true)
        skull6.lineWidth = 5
        UIColor.blackColor().set()
        skull6.stroke()
        UIColor.redColor().set()
        skull6.fill()
        let skull7 = UIBezierPath(arcCenter: skullCenter7, radius: skullRadius / 4, startAngle: 0.0, endAngle: CGFloat(M_PI*1), clockwise: true)
        skull7.lineWidth = 5
        UIColor.blueColor().set()
        skull7.stroke()
        //tickDown()
       UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0).set()
        skull7.fill()
        


        let line = UIBezierPath()
        line.moveToPoint(CGPointMake(140, 400))
        line.addLineToPoint(CGPointMake(240, 400))
        
        line.moveToPoint(CGPointMake(143, 400))
        line.addLineToPoint(CGPointMake(160, 420))
        
        line.moveToPoint(CGPointMake(155, 420))
        line.addLineToPoint(CGPointMake(173, 400))
        
        line.moveToPoint(CGPointMake(176, 400))
        line.addLineToPoint(CGPointMake(193, 420))
        
        line.moveToPoint(CGPointMake(188, 420))
        line.addLineToPoint(CGPointMake(206, 400))
        
        line.moveToPoint(CGPointMake(209, 400))
        line.addLineToPoint(CGPointMake(226, 420))
        
        line.moveToPoint(CGPointMake(221, 420))
        line.addLineToPoint(CGPointMake(237, 400))
        line.lineWidth = 8.0
        UIColor.redColor().set()
        line.stroke()
        
    }


}
