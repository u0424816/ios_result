
import UIKit

class FaceIt: UIView {

    override func drawRect(rect: CGRect){
   
        
        let skullRadius = min(bounds.size.width, bounds.size.height) / 2
        let skullCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let skull = UIBezierPath(arcCenter: skullCenter , radius: skullRadius, startAngle: 0.0 , endAngle: CGFloat(M_PI * 2) , clockwise : false)
       
        skull.lineWidth = 5.0
        
        UIColor.blackColor().set()
        
        skull.stroke()
        
        UIColor.redColor().set()
        
        skull.fill()
        
        let skullRadiusa = min(bounds.size.width, bounds.size.height) / 15

        let skulla = UIBezierPath(arcCenter: CGPoint(x: bounds.midX/3+210, y: bounds.midY-90) , radius: skullRadiusa, startAngle: 0.0 , endAngle: CGFloat(M_PI * 2) , clockwise : false)
        let skullb = UIBezierPath(arcCenter: CGPoint(x: bounds.midX/3+55, y: bounds.midY-90) , radius: skullRadiusa, startAngle: 0.0 , endAngle: CGFloat(M_PI * 2) , clockwise : false)
        
        
        skulla.lineWidth = 1
        UIColor.blackColor().set()
        skullb.lineWidth = 1
        UIColor.blackColor().set()
        
        skulla.stroke()
        skullb.stroke()
        UIColor.blackColor().set()
        UIColor.blackColor().set()
        skulla.fill()
        skullb.fill()
        
          let skullRadiusc = min(bounds.size.width, bounds.size.height) / 4
        
        let skullc = UIBezierPath(arcCenter: CGPoint(x:bounds.midX ,y: bounds.midY+20) , radius: skullRadiusc, startAngle: CGFloat( 180.0*M_PI/180.0) , endAngle: CGFloat( 360.0*M_PI/180.0) , clockwise :false )
        skullc.stroke()
        UIColor.redColor().set()
        
        let skullRadiuswater = min(bounds.size.width, bounds.size.height) / 25

        
    let line = UIBezierPath()
        line.moveToPoint(CGPoint(x: bounds.midX/3+100,y: bounds.midY-40))
        line.addCurveToPoint(CGPoint(x: bounds.midX/3+110,y: bounds.midY-40), controlPoint1: CGPoint(x: bounds.midX/3+50 , y: bounds.midY+80), controlPoint2: CGPointMake(bounds.midX/3+150, bounds.midY+80))
        line.lineWidth = 1.0
        UIColor.blueColor().set()
        line.stroke()
        line.fill()
        
    }
    
}