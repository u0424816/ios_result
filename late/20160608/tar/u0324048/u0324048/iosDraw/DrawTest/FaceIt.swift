//
//  FaceIt.swift
//  DrawTest
//
//  Created by Leviathan on 2016/6/8.
//  Copyright © 2016年 Leviathan. All rights reserved.
//

import UIKit

class FaceIt: UIView {

    override func drawRect(rect: CGRect) {
        
        let skullRadius = min(bounds.size.width , bounds.size.height) / 3
        let skullCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let skull = UIBezierPath(arcCenter: skullCenter, radius: skullRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        skull.lineWidth = 5.0
        UIColor.blueColor().set()
        skull.stroke()
        UIColor.yellowColor().set()
        skull.fill()
        
        let lefteyes = CGPoint(x: bounds.midX-40, y: bounds.midY-20)
        let skull2 = UIBezierPath(arcCenter: lefteyes, radius: skullRadius/10, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        skull2.lineWidth = 5.0
        UIColor.blueColor().set()
        skull2.stroke()
        
        
        let righteyes = CGPoint(x: bounds.midX+40, y: bounds.midY-20)
        let skull3 = UIBezierPath(arcCenter: righteyes, radius: skullRadius/10, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        skull3.lineWidth = 5.0
        UIColor.blueColor().set()
        skull3.stroke()
        
        
        let CircleRadius = min(bounds.size.width , bounds.size.height) / 8
        
        let oneCenter = CGPoint(x: bounds.midX, y: bounds.midY-230)
        let oneCircle = UIBezierPath(arcCenter: oneCenter, radius: CircleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        oneCircle.lineWidth = 4.0
        UIColor.blackColor().set()
        oneCircle.stroke()

        
        let twoCenter = CGPoint(x: bounds.midX-89, y: bounds.midY-230)
        let twoCircle = UIBezierPath(arcCenter: twoCenter, radius: CircleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        twoCircle.lineWidth = 4.0
        UIColor.blueColor().set()
        twoCircle.stroke()
        
        let threeCenter = CGPoint(x: bounds.midX+89, y: bounds.midY-230)
        let threeCircle = UIBezierPath(arcCenter: threeCenter, radius: CircleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        threeCircle.lineWidth = 4.0
        UIColor.redColor().set()
        threeCircle.stroke()
        
        let fourCenter = CGPoint(x: bounds.midX-47, y: bounds.midY-190)
        let fourCircle = UIBezierPath(arcCenter: fourCenter, radius: CircleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        fourCircle.lineWidth = 4.0
        UIColor.yellowColor().set()
        fourCircle.stroke()

        
        let fiveCenter = CGPoint(x: bounds.midX+47, y: bounds.midY-190)
        let fiveCircle = UIBezierPath(arcCenter: fiveCenter, radius: CircleRadius, startAngle: 0.0, endAngle: CGFloat(M_PI * 2), clockwise: false)
        fiveCircle.lineWidth = 4.0
        UIColor.greenColor().set()
        fiveCircle.stroke()
        
        
        
        
        
        
        
        
        let line = UIBezierPath()
        line.moveToPoint(CGPointMake(40, 0))
        line.addLineToPoint(CGPointMake(20, 100))
        line.lineWidth = 10.0
        UIColor.redColor().set()
        line.stroke()
        skull.stroke()
    }
}
