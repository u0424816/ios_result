//
//  FaceIt.swift
//  20160608
//
//  Created by nkfust11 on 2016/6/8.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import UIKit

class FaceIt: UIView {

    override func drawRect(rect: CGRect){
     //drawing code
        
        let skullRadius = min(bounds.size.width, bounds.size.height) / 2
        let skullCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let skull = UIBezierPath(arcCenter: skullCenter , radius: skullRadius, startAngle: 0.0 , endAngle: CGFloat(M_PI * 2) , clockwise : false)
       
        skull.lineWidth = 5.0
        
        UIColor.redColor().set()
        
        skull.stroke()
        
        UIColor.yellowColor().set()
        
        skull.fill()
        
        let skullRadiusa = min(bounds.size.width, bounds.size.height) / 15 //大小

        let skulla = UIBezierPath(arcCenter: CGPoint(x: bounds.midX/3+210, y: bounds.midY-90) , radius: skullRadiusa, startAngle: 0.0 , endAngle: CGFloat(M_PI * 2) , clockwise : false)
        let skullb = UIBezierPath(arcCenter: CGPoint(x: bounds.midX/3+55, y: bounds.midY-90) , radius: skullRadiusa, startAngle: 0.0 , endAngle: CGFloat(M_PI * 2) , clockwise : false)
        
        
        skulla.lineWidth = 1
        UIColor.blackColor().set()
        skullb.lineWidth = 1
        UIColor.blackColor().set()
        
        skulla.stroke()
        skullb.stroke()
        UIColor.blackColor().set()
        UIColor.blackColor().set()
        skulla.fill()
        skullb.fill()
        
          let skullRadiusc = min(bounds.size.width, bounds.size.height) / 4 //大小
        
        let skullc = UIBezierPath(arcCenter: CGPoint(x:bounds.midX ,y: bounds.midY+20) , radius: skullRadiusc, startAngle: CGFloat( 180.0*M_PI/180.0) , endAngle: CGFloat( 360.0*M_PI/180.0) , clockwise :false )
        skullc.stroke()
        UIColor.blueColor().set()
        
        let skullRadiuswater = min(bounds.size.width, bounds.size.height) / 25 //大小
//        
//        let skullwater = UIBezierPath(arcCenter: CGPoint(x:bounds.midX+40 ,y: bounds.midY-10) , radius: skullRadiuswater, startAngle: CGFloat( 180.0*M_PI/180.0) , endAngle: CGFloat( 360.0*M_PI/180.0) , clockwise :false )
//        skullwater.stroke()
//        UIColor.blueColor().set()
//        skullwater.fill()

        

    let line = UIBezierPath()
        line.moveToPoint(CGPoint(x: bounds.midX/3+210,y: bounds.midY-40))
        line.addCurveToPoint(CGPoint(x: bounds.midX/3+210,y: bounds.midY-40), controlPoint1: CGPoint(x: bounds.midX/3+160 , y: bounds.midY+80), controlPoint2: CGPointMake(bounds.midX/3+260, bounds.midY+80))
        line.lineWidth = 1.0
        UIColor.blueColor().set()
        line.stroke()
        line.fill()
        
    }
    
}