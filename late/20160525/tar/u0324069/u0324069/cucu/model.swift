//
//  model.swift
//  cucu
//
//  Created by nkfust10 on 2016/5/18.
//  Copyright © 2016年 nkfust10. All rights reserved.
//

import Foundation
class TestModel{
    private var tem = 0.0
    private var temp = 0.0
    var ss = ""
    func setOperand(operand: Double){
        tem = operand
    }
    func performOperation(symbol: String){
        switch symbol{
        case "P" : tem = M_PI
        case "C" : tem = 0
        case "E" : tem = M_E
        case "S" : tem = sqrt(tem)
        case "=" :
            switch ss{
            case "+" : tem = temp + tem
            case "-" : tem = temp - tem
            case "*" : tem = temp * tem
            case "/" : tem = temp / tem
            default : break
            }
            
        default : ss = symbol
            temp = tem
            break
        }
    
    }
    var result: Double{
        get{
            return tem
        }
    }
}
