//
//  Model.swift
//  20160525
//
//  Created by nkfust16_2 on 2016/6/22.
//  Copyright © 2016年 nkfust16_2. All rights reserved.
//

import Foundation

func multiply (op1 :Double, op2 :Double)-> Double {
    return op1 * op2
}
func subtract (op1 :Double, op2 :Double)-> Double {
    return op1 - op2
}
func plus (op1 :Double, op2 :Double)-> Double {
    return op1 + op2
}
func divide (op1 :Double, op2 :Double)->Double {
    return op1 / op2
}

class testmodel {
    
    private var temp = 0.0
    func baibai (operand: Double) {
        temp = operand
    }
    var operations: Dictionary<String, Operation> = [
        "兀": Operation.Constant(M_PI), //M_PI,
        "根": Operation.UnaryOperation(sqrt), //sqrt,
        "e": Operation.Constant(M_E),//M_E,
        "cos": Operation.UnaryOperation(cos),//cos
        "x":Operation.BinaryOperation(multiply),
        "-":Operation.BinaryOperation(subtract),
        "+":Operation.BinaryOperation(plus),
        "/":Operation.BinaryOperation(divide),
        "=":Operation.Equals
    ]
    
    enum Operation {
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
    }
    
    func performOperation(symbol: String){
        if let operation = operations[symbol]{
            print("operation is \(operation)")
            switch operation{
            case Operation.Constant(let value): temp = value
            case Operation.UnaryOperation(let function): temp = function(temp)
            case Operation.BinaryOperation(let function):
                pending = PendingBinaryOperationInfo(binaryFunction: function , firstOperand: temp )
            case Operation.Equals:
                print("pending test is \(pending)")
                if pending != nil {
                    print("pending is \(pending)")
                    temp = pending!.binaryFunction(pending!.firstOperand, temp)
                    pending = nil
                }
            }
        }
    }
    private var pending : PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {     //不能有繼承
        var binaryFunction: (Double , Double) -> Double
        var firstOperand : Double
    }
    func gobaibai(symbol: String){
        switch symbol {
        case "兀": temp = M_PI
        case "根": sqrt(temp)
        default: break
        }
    }
    var result: Double {
        get {
            print("\(temp)")
            return temp
        }
    }
}