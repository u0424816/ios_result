import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var flag = false
    var CaculateId = ""
    var priviousNum = 0.0
    private var model = TestModel()
    
    
    @IBOutlet weak var display: UILabel!

    var 正在輸入 = false
    
    
    @IBAction func show(sender: UIButton) {
        let msg = sender.currentTitle!
        
        if flag {
            
            display.text! += msg
            
        }else if display.text == "0" && msg == "0"{
            display.text = "0"
        }else{
            display.text = msg
            flag = true
        }
        if(msg == "C"){
            display.text = "0"
            flag = false
        }else if (msg == "A"){
            display.text = "0324065 鄭凱文"
            flag = false
        }
    }
    
    var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            display.text = String(newValue)
        }
    }
    
    
    @IBAction func touchkeyC(sender: UIButton) {
        if flag == true {
            model.setOperand(displayValue)
            flag = false
        }
        if let mathsymbol = sender.currentTitle{
               model.perforOperation(mathsymbol)
        }
        displayValue = model.result
    }
    



}

