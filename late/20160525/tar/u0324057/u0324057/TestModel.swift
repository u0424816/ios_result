//
//  TestModel.swift
//  test0511
//
//  Created by nkfust15 on 2016/5/18.
//  Copyright © 2016年 nkfust15. All rights reserved.
//

import Foundation

class CalcModel {
    var operand:Double = 0.0
    var symbol:String = ""
    func setOperand(operand: Double){
        self.operand = operand
    }
    func performOperation(symbol: String){
        switch symbol {
        case "π":
            self.operand=M_PI
            break
        case "√":
            self.operand=sqrt(self.operand)
            break
        case "e":
            self.operand=M_E
        default :
            break
        }
    }
    
    func strtocha(input:String)->[String]{
        var restr:[String] = [""]
        var chartemp: [Character] = Array(input.characters)
        var index:Int=0
        
        for i in 0...chartemp.count-1{
            switch chartemp[i]{
            case "+","-","*","/","(",")":
                if(restr[index]==""){
                    restr[index]=String(chartemp[i])
                    restr.append("")
                    index+=1
                }else{
                    restr.append(String(chartemp[i]))
                    restr.append("")
                    index+=2
                }
                break
            default :
                restr[index] += String(chartemp[i])
                break
            }
        }
        if(restr[restr.count-1]==""){
            restr.removeLast()
        }
        return restr
    }
    
    func topostfix(input:[String])->[String]{
        var stack:[String]=[""]
        var output:[String]=[""]
        for i in input{
            switch i{
            case "+","-":
                var templast:String = stack[stack.count-1]
                while(((templast=="*")||(templast=="/")||(templast=="+")||(templast=="-"))){
                    output.append(stack.removeLast())
                    templast=stack[stack.count-1]
                }
                stack.append(i)
                break
            case "*","/":
                var templast:String = stack[stack.count-1]
                while(((templast=="*")||(templast=="/"))){
                    output.append(stack.removeLast())
                    templast=stack[stack.count-1]
                }
                stack.append(i)
                break
            case "(":
                stack.append(i)
                break
            case ")":
                var templast:String = stack[stack.count-1]
                while(templast != "("){
                    output.append(stack.removeLast())
                    templast=stack[stack.count-1]
                }
                stack.removeLast()
                break
            default:
                output.append(i)
                break
            }
        }
        for(var index=stack.count-1;index>=0;index-=1){
            output.append(stack[index])
        }
        output.removeLast()
        output.removeAtIndex(0)
        return output
    }
    
    func calc2(input:[String])->Double{
        var temp:Double = 0
        var stack:[Double]=[]
        for i in input{
            switch i{
            case "+":
                temp=stack[stack.count-2]+stack[stack.count-1]
                stack.removeLast()
                stack.removeLast()
                stack.append(temp)
                break
            case "-":
                temp=stack[stack.count-2]-stack[stack.count-1]
                stack.removeLast()
                stack.removeLast()
                stack.append(temp)
                break
            case "*":
                temp=stack[stack.count-2]*stack[stack.count-1]
                stack.removeLast()
                stack.removeLast()
                stack.append(temp)
                break
            case "/":
                temp=stack[stack.count-2]/stack[stack.count-1]
                stack.removeLast()
                stack.removeLast()
                stack.append(temp)
                break
            default:
                stack.append(Double(i)!)
                break
            }
        }
        return stack[0]
    }
    
    func calc(input:String)->String{
        var charstr:[String] = []
        var answer:Double = 0
        charstr=strtocha(input)
        charstr=topostfix(charstr)
        answer=calc2(charstr)
        return String(answer)
     
    }
    
    var result: Double{
        get{
            return operand
        }
    }
}