//
//  model.swift
//  cucu
//
//  Created by nkfust10 on 2016/5/18.
//  Copyright © 2016年 nkfust10. All rights reserved.
//

import Foundation
func add(op1:Double , op2:Double) -> Double{
    return op1 + op2
}
func co(op1:Double, op2:Double) ->Double{
    return op1 - op2
}
func xxx(op1:Double, op2:Double) ->Double{
    return op1 * op2
}

func rrrr(op1:Double, op2:Double) ->Double{
    return op1 / op2
}

class TestModel{
    
    private var tem = 0.0
    private var temp = 0.0
    var ss = ""
    
    func setOperand(operand: Double){
        tem = operand
    }
    var operations: Dictionary<String, Operation> = [
        "P": Operation.constant(M_PI),
        "E": Operation.constant(M_E),
        "S": Operation.UnaryOperation(sqrt),
        "cos": Operation.UnaryOperation(cos),
        "+":Operation.BinaryOperation(add),
        "-":Operation.BinaryOperation(co),
        "*":Operation.BinaryOperation(xxx),
        "/":Operation.BinaryOperation(rrrr),
        "=":Operation.Equals,
        "C":Operation.constant(0)
        
        
    ]
    enum Operation{
        case constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
    }
    func performOperation(symbol: String){
        /*switch symbol{
         case "P" : tem = M_PI
         case "C" : tem = 0
         case "E" : tem = M_E
         case "S" : tem = sqrt(tem)
         case "=" :
         switch ss{
         case "+" : tem = temp + tem
         case "-" : tem = temp - tem
         case "*" : tem = temp * tem
         case "/" : tem = temp / tem
         default : break
         }
         
         default : ss = symbol
         temp = tem
         break
         }*/
        
        if let operation = operations[symbol]{
            switch operation{
            case .constant(let value): tem = value
            case .UnaryOperation(let kk): tem = kk(tem)
            case .BinaryOperation(let function):
                pending = PendingBunaryOperationInfo(binaryFunction: function , firstOperand: tem)
                
               // tem = pending!.binaryFunction(pending!.firstOperand, tem)
                
                
            case .Equals:
                if pending != nil{
                    tem = pending!.binaryFunction(pending!.firstOperand, tem)
                    pending = nil
                }
                
            }
        }
        
    }
    
    private var pending:PendingBunaryOperationInfo?
    
    struct PendingBunaryOperationInfo {
        var binaryFunction: (Double, Double) -> Double
        var firstOperand : Double
    }
    var result: Double{
        get{
            return tem
        }
    }
}
