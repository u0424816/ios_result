//
//  sym.swift
//  0324075-1
//
//  Created by nkfust29_2 on 2016/6/22.
//  Copyright © 2016年 nkfust29_2. All rights reserved.
//

import Foundation


func plus(op1: Double, op2: Double)-> Double{
    return op1 + op2
}

func minus(op1: Double, op2: Double)-> Double{
    return op1 - op2
}

func multiply(op1: Double, op2: Double)-> Double{
    return op1 * op2
}

func devide(op1: Double, op2: Double)-> Double{
    return op1 / op2
}

func transferPlusMinus(op:Double)-> Double{
    return op * (-1)
}


class TestModel{
    private var temp = 0.0
    
    func setOperand(operand: Double){
        temp = operand
    }
    
    enum Operation {
        case Constant(Double)
        case UnaryOperation((Double -> Double))
        case BinaryOperation((Double, Double) -> (Double))
        case Equals
    }
    
    
    var operations: Dictionary<String, Operation> = [
        "+": Operation.BinaryOperation(plus),
        "−": Operation.BinaryOperation(minus),
        "×": Operation.BinaryOperation(multiply),
        "÷": Operation.BinaryOperation(devide),
        "=": Operation.Equals,
        "±": Operation.UnaryOperation(transferPlusMinus)
    ]
    func performOperation(symbol: String){
        
        if let operation = operations[symbol]{
            switch operation {
            case Operation.Constant(let value): temp = value
            case Operation.UnaryOperation(let function): temp = function(temp)
            case Operation.BinaryOperation(let function):
                if pending == nil{
                    pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperator: temp)
                }else{
                    temp = pending!.binaryFunction(pending!.firstOperator , temp)
                    pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperator: temp)
                }
            case Operation.Equals:
                if pending != nil{
                    temp = pending!.binaryFunction(pending!.firstOperator , temp)
                    pending = nil
                }
            }
        }
    }
    
    private var pending: PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFunction: (Double , Double) -> Double
        var firstOperator: Double
    }
    
    var result: Double {
        get {
            return temp
        }
    }
}