import Foundation
import CoreFoundation

var player = 1 

enum Status {
    case Continue, Win, Lose, Tie
}
var a : Bool = true
var b : Bool = false
var sumDice = 0
func rollDice() -> (die1 : Int, die2 : Int,die3 : Int,sum : Int) {
    let die1 = Int(rand()%6+1)
    let die2 = Int(rand()%6+1)
    let die3 = Int(rand()%6+1)
    sumDice = die1 + die2 + die3
    if(die1 == die2 && die2 == die3) {          //豹子判斷
        return(die1, die2, die3, die1 + 7)
    }else if(die1 == die2) {                    //骰子點數
        return(die1, die2, die3, die3)
    }else if(die1 == die3) {
        return(die1, die2, die3, die2)
    }else if(die2 == die3) {
        return(die1, die2, die3, die1)
    }else if(sumDice == 6) {                    //逼機判斷
        return(die1, die2, die3, 0)
    }else if(sumDice == 15) {                   //456判斷
        return(die1, die2, die3, 7)
    }else{
        return(die1, die2, die3, -10)           //重骰判斷
    }
}
var roll = rollDice()
func display (roll : (Int, Int, Int, Int)) {    //判斷輸出
    if(roll.3 > 7 && roll.3 < 20){
        print("  roll  \(roll.0)  \(roll.1)  \(roll.2)  點數 : 豹子 \(roll.0)")
        a = false
    }else if(roll.3 == 0) {
        print("  roll  \(roll.0)  \(roll.1)  \(roll.2)  點數 : 逼機")
        a = false
    }else if(roll.3 == 7) {
        print("  roll  \(roll.0)  \(roll.1)  \(roll.2)  點數 : 四五六")
        a = false
    }else if(roll.3 > 0 && roll.3 < 7) {
        print("  roll  \(roll.0)  \(roll.1)  \(roll.2)  點數 : \(roll.3)")
        a = false  
    }else{
        print("  roll  \(roll.0)  \(roll.1)  \(roll.2)  重骰")
    }
}
var gameStatus = Status.Continue                //賽局初始狀態

var myPoint = 0                                 //玩家初始點數        
var BOOKIEnoun = 0                              //莊家初始點數

var big = -3                                    //玩家骰出的最大數字
var winPlay = 0                                 //最贏的玩家

//判斷複數玩家勝利
var TiePlayer = 0
var numPlayer : Array<String> = []

while gameStatus == Status.Continue {           //賽局判斷
    for var i in 1...player {                   //玩家骰骰子
        while a {
            roll = rollDice()
            print("玩家\(i)", terminator : "")
            display(roll)
            myPoint = roll.3
            
            if (myPoint >= big){
                if(myPoint > big){
                    big = myPoint
                    winPlay = i
                    b = true 
                    TiePlayer = 0
                    numPlayer = []
                }
                if(big == roll.3){
                    winPlay = i
                    TiePlayer += 1
                    numPlayer += ["\(i)"]
                }
            }
        } 
        a = true
        print() 
    }
    while a {
        roll = rollDice()
        print("莊家 ", terminator : "")
        display(roll)
        BOOKIEnoun = roll.3
    }
    print()
    a = true
    if(big > BOOKIEnoun) {
        gameStatus = Status.Win
    }else {
        gameStatus = Status.Lose
    }
}

if gameStatus == Status.Win { //輸出判斷
    if(TiePlayer == 1){
        print("玩家 \(winPlay) 勝利")
    }else{
        for i in 1...numPlayer.count {
            print("玩家 \(numPlayer[i-1])  ",terminator : "")
        }
        print(" 勝利!!")
    }
}else {
    print("莊家勝利")
}