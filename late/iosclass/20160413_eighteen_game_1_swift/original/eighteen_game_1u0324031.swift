import Foundation
import CoreFoundation
let time = UInt32(NSDate().timeIntervalSinceReferenceDate)
srand(time)
enum Status {
    case Countinue, Won, Equal, Lost
}

func rollDice() -> (die1: Int, die2: Int, die3: Int){
    let die1 = Int(rand()%6+1)
    let die2 = Int(rand()%6+1)
    let die3 = Int(rand()%6+1)
    return (die1, die2, die3)
}

func displayRoll(str: String,_ roll: (Int, Int, Int)){
    print("\(str): (\(roll.0),\(roll.1),\(roll.2),\(roll.2))")
}

var gameStatus = Status.Countinue
var masterRoll = rollDice()
var playerRoll = rollDice()

displayRoll("master", masterRoll)
displayRoll("player", playerRoll)

func rollAdd(Roll: (Int, Int, Int)) -> Int{
    var point: Int = Roll.0 + Roll.1
    if Roll.0 == Roll.1 {
        if Roll.0 < Roll.2{
            point = Roll.2 + Roll.2
        }
    }
    return point
}

var masterPoint = rollAdd(masterRoll)
var playerPoint = rollAdd(playerRoll)

if playerRoll.0 == playerRoll.1 && playerRoll.0 == playerRoll.2 {
    if masterRoll.0 == masterRoll.1 && masterRoll.0 == masterRoll.2{
        if playerRoll.0 < masterRoll.0{
            gameStatus = Status.Lost
        }else if playerRoll.0 == masterRoll.0 {
            gameStatus = Status.Equal
        }else{
            gameStatus = Status.Won
        }
    }else{
        gameStatus = Status.Won    
    }
}else if masterRoll.0 == masterRoll.1 && masterRoll.0 == masterRoll.2{
    if playerRoll.0 == playerRoll.1 && playerRoll.0 == playerRoll.2 {
        if playerRoll.0 < masterRoll.0{
                gameStatus = Status.Lost
            }else{
            gameStatus = Status.Won
            }
        }
    gameStatus = Status.Lost
}else{
    if masterPoint > playerPoint {
        gameStatus = Status.Lost
    }else if masterPoint < playerPoint{
        gameStatus = Status.Won
    }else{
        gameStatus = Status.Equal
    }
    print("master: \(masterPoint) \nplayer: \(playerPoint)")
}

if gameStatus == Status.Won {
    print("player win")
}else if gameStatus == Status.Lost{
    print("master win")
}else{
    print("Equal")
}