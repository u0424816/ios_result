import Foundation
import CoreFoundation

func isPointOk(point : [Int]) -> Bool {
    var i : Int
    var j : Int
    for(i=0;i<point.count;i++){
        for(j=0;j<point.count;j++){
            if(i==j) {continue;}
            if(point[i] == point[j]) {return true}
        }
    }
    return false
}

func rollDice() -> ([Int]){
    var die = [Int]()
    let diceNumber = 4
    repeat{
        die.removeAll()
        var i : Int
        for (i=0;i<diceNumber;i++){
            die.append(Int(rand()%6+1))
        }
        die=die.sort(<)
    }while(!isPointOk(die))
    return die
}

func pointCalc(point : [Int]) -> Int {
    var p = point
    var sum = 0
    var i = 0
    if(p[0] == p[1] && p[1] == p[2] && p[2] == p[3]){
        return point[0]*20
    }
    while(p.count > 2){
        if(p[i] == p[i+1]){   
            p.removeAtIndex(i)
            p.removeAtIndex(i)
        }
        i++
    }
    for(i=0;i<p.count;i++){
        sum = sum + p[i]
    }
    return sum
}

func displayResult(No : Int , point : [Int] , playSum : Int , makerSum : Int , name : String){
    print("player \(No) \(name) point : ",terminator:"")
    var i : Int
    for(i=0;i<point.count;i+=1){
        print(point[i],terminator:" ")
    }
    print("sum : \(playSum)" , terminator : " ")
    if(playSum > makerSum){
        print("Win")
    }else if(playSum < makerSum){
        print("Lose")
    }else{
        print("Draw")
    }
}

func rollDiceGame(playerNumber : Int){
    var playersPoint = [Int]()
    var makerPoint = [Int]()
    var i : Int
    makerPoint = rollDice()
    var makerPointSum = pointCalc(makerPoint)
    print("maker \(makerPoint) \(makerPointSum)")
    for(i=1;i<=playerNumber;i++){
        var playerPointArray = rollDice()
        var playerPointSum = pointCalc(playerPointArray)
        displayResult( i , point:playerPointArray , playSum:playerPointSum , makerSum:makerPointSum , name:"name")
        if(playerPointSum > makerPointSum){
        }
    }
}

rollDiceGame(1)