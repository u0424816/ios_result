import Foundation
import CoreFoundation

let time = UInt32(NSDate().timeIntervalSinceReferenceDate)
srand(time)
var playerRollNum = 0
var playRoll = (0,0,0,0)
var masterRoll = (0,0,0,0)
var masterRollNum = 0
func rollDice()->(die1: Int,die2: Int,die3: Int,die4: Int){
    let die1 = Int(rand()%6+1)
    let die2 = Int(rand()%6+1)
    let die3 = Int(rand()%6+1)
    let die4 = Int(rand()%6+1)
    return(die1,die2,die3,die4)
}

func displayRoll(roll:(Int,Int,Int,Int),_ result:Int){
    print("Player rolled: (\(roll.0),\(roll.1),\(roll.2),\(roll.3))=\(result)")
}
func displayRollMaster(roll:(Int,Int,Int,Int),_ result:Int){
    print("Master rolled: (\(roll.0),\(roll.1),\(roll.2),\(roll.3))=\(result)")
}
func rollCalculate(roll:(Int,Int,Int,Int))->Int{
    var result = 0
    if roll.0==roll.1 && roll.2==roll.3{
        if roll.0>roll.2{
            result = roll.0*2
        }else{
            result = roll.2*2
        }
    }else if roll.0==roll.2 && roll.1==roll.3{
        if roll.0>roll.1{
            result = roll.0*2
        }else{
            result = roll.1*2
        }
    }else if roll.0==roll.3 && roll.1==roll.2{
        if roll.0>roll.1{
            result = roll.0*2
        }else{
            result = roll.1*2
        }
    }else{
            if roll.0==roll.1{
            result = roll.2+roll.3
        }else if roll.0==roll.2{
            result = roll.1+roll.3
        }else if roll.0==roll.3{
            result = roll.1+roll.2
        }else if roll.1==roll.2{
            result = roll.0+roll.3
        }else if roll.1==roll.3{
            result = roll.0+roll.2
        }else if roll.2==roll.3{
            result = roll.0+roll.1
        }
    }
    return result
}

func playerRollStart(){
    var roll = rollDice()
    while(roll.0 != roll.1 && roll.0 != roll.2 && roll.0 != roll.3 ){
        roll = rollDice()
    }
    if roll.0==roll.1 && roll.1 == roll.2 && roll.2 == roll.3{
        print("Player rolled: (\(roll.0),\(roll.1),\(roll.2),\(roll.3))")
    }else{
        displayRoll(roll,rollCalculate(roll))
        playerRollNum = rollCalculate(roll)
    }
    playRoll = roll

    
}
func masterRollStart(){
    var roll = rollDice()
    while(roll.0 != roll.1 && roll.0 != roll.2 && roll.0 != roll.3 ){
        roll = rollDice()
    }
    if roll.0 == roll.1 && roll.1 == roll.2 && roll.2 == roll.3{
        print("Master rolled: (\(roll.0),\(roll.1),\(roll.2),\(roll.3))")
    }else{
        displayRollMaster(roll,rollCalculate(roll))
        masterRollNum = rollCalculate(roll)
    }
    masterRoll = roll

    
}
var run = true
func same(){
    if playRoll == masterRoll{
        print("Master and Player is equal!")
        run = true
    }else if playRoll.0 == playRoll.1 && playRoll.1 == playRoll.2 && playRoll.2 == playRoll.3 || masterRoll.0 != masterRoll.1 || masterRoll.0 != masterRoll.2 || masterRoll.0 != masterRoll.3{
        print("Player Win!")
        run = false
    }else if masterRoll.0 == masterRoll.1 && masterRoll.1 == masterRoll.2 && masterRoll.2 == masterRoll.3 || masterRoll.0 != masterRoll.1 || masterRoll.0 != masterRoll.2 || masterRoll.0 != masterRoll.3{
        print("Master Win!")
        run = false
    }else{
        if masterRoll.0>playRoll.0{
            print("Master Win!")
            run = false
        }else if masterRoll.0<playRoll.0{
            print("Player Win!")
            run = false
        }else{
            print("Master and Player is equal!")
            run = true
        }
    }
}


while(run){
    masterRollStart()
    playerRollStart()
    if playRoll.0 == playRoll.1 && playRoll.1 == playRoll.2 && playRoll.2 == playRoll.3 || masterRoll.0 == masterRoll.1 && masterRoll.1 == masterRoll.2 && masterRoll.2 == masterRoll.3{
        same()
    }else{
        if masterRollNum > playerRollNum{
            print("Master Win!")
            run = false
        }else if masterRollNum < playerRollNum{
            print("Player Win!")
            run = false
        }else{
            print("Master and Player is equal!")
            run = true
        }
    }
    
    
}


