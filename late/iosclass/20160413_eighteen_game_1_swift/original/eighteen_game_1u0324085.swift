import Foundation
import CoreFoundation

enum Status {
    case Continue, Win, Lose
}
var a : Bool = true
func rd() -> (die1 : Int, die2 : Int,die3 : Int,sum : Int) {
    let die1 = Int(rand()%6+1)
    let die2 = Int(rand()%6+1)
    let die3 = Int(rand()%6+1)
    var sumDice = die1 + die2 + die3
    if(die1 == die2 && die2 == die3) {          
        return(die1, die2, die3, die1 + 7)
    }else if(die1 == die2) {                    
        return(die1, die2, die3, die3)
    }else if(die1 == die3) {
        return(die1, die2, die3, die2)
    }else if(die2 == die3) {
        return(die1, die2, die3, die1)
    }else if(sumDice == 6) {                 
        return(die1, die2, die3, 0)
    }else if(sumDice == 15) {                   
        return(die1, die2, die3, 7)
    }else{
        return(die1, die2, die3, 111)          
    }
}
var roll = rd()
func display (roll : (Int, Int, Int, Int)) {    
    if(roll.3 > 7 && roll.3 < 20){
        print(" \(roll.0)  \(roll.1)  \(roll.2)  點數 : 豹子 \(roll.0)")
        a = false
    }else if(roll.3 == 0) {
        print(" \(roll.0)  \(roll.1)  \(roll.2)  點數 : 逼機")
        a = false
    }else if(roll.3 == 7) {
        print(" \(roll.0)  \(roll.1)  \(roll.2)  點數 : 四五六")
        a = false
    }else if(roll.3 > 0 && roll.3 < 7) {
        print(" \(roll.0)  \(roll.1)  \(roll.2)  點數 : \(roll.3)")
        a = false  
    }else{
        print(" \(roll.0)  \(roll.1)  \(roll.2)  重骰")
    }
}
var gameStatus = Status.Continue               
var myPoint = 0                                   
var BOOKIEnoun = 0                            

while gameStatus == Status.Continue {        
    while a {
        roll = rd()
        print("玩家", terminator : "")
        display(roll)
        myPoint = roll.3
    }
    a = true
    while a {
        roll = rd()
        print("莊家", terminator : "")
        display(roll)
        BOOKIEnoun = roll.3
    }
    a = true
    if(myPoint > BOOKIEnoun) {
        gameStatus = Status.Win
    }else {
        gameStatus = Status.Lose
    }
}

if gameStatus == Status.Win {                     //輸出判斷
    print("玩家勝利")
}else {
    print("莊家勝利")
}