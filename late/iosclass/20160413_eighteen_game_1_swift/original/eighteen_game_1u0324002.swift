import Foundation
import CoreFoundation

func roll() -> [Int]{
	var result:[Int] = [0,0,0,0]
	repeat {
		if result[0] != 0 {
			print("\t擲出：\(result)，重骰")
		}
		for i in 0...3 {
			result[i] = Int(rand()) % 6 + 1
		}
	} while (isFourDiff(result))
	return result.sort()
}

func isFourDiff(dice:[Int]) -> Bool {
	for i in 0...dice.count-2 {
		for j in i+1...dice.count-1 {
			if dice[i] == dice[j] {
				return false
			}
		}
	}
	return true
}

func isFourSame(dice:[Int]) -> Bool {
	for i in 0...dice.count-2 {
		for j in i+1...dice.count-1 {
			if dice[i] != dice[j] {
				return false
			}
		}
	}
	return true
}

func countPoint(_dice:[Int]) -> Int {
	var dice:[Int] = _dice.sort()
	var point:Int = 0
	var d = 1
	repeat {
		dice = dice.sort()
		while (dice.indexOf(d) != nil) {
			dice.removeAtIndex(dice.indexOf(d)!)
		}		
		if dice.count == 1 {
			dice.append(d)
			break
		} else if dice.count == 2 {
			break
		} else if dice.count == 4 {
			d++
			continue
		} else {
			dice.append(d)
		}
		d++
	} while (d <= 6)	
	for item in dice {
		point += item
	}
	return point
}

func game(){

	print("莊家：")
	var host:[Int] = roll()
	print("\t擲出：\(host)")
	var hostPoint:Int = countPoint(host)
	var isHostBowZi:Bool = isFourSame(host)
	if isHostBowZi {
		print("\t擲出豹子")
	} else {
		print("\t點數為：\(hostPoint)")
	}
	print("玩家：")
	var player:[Int] = roll()
	print("\t擲出：\(player)")
	var playerPoint:Int = countPoint(player)
	var isPlayerBowZi:Bool = isFourSame(player)
	if isPlayerBowZi {
		print("\t擲出豹子")
	} else {
		print("\t點數為：\(playerPoint)")
	}
	
	if playerPoint == hostPoint || (isHostBowZi && isPlayerBowZi){
		print("==========")
		print("平手，重玩")
		print("==========")
		game()
	} else if isHostBowZi {
		print("==========")
		print("　莊家勝　")
		print("==========")
	} else if isPlayerBowZi {
		print("==========")
		print("　玩家勝　")
		print("==========")
	} else if hostPoint > playerPoint{
		print("==========")
		print("　莊家勝　")
		print("==========")
	} else if playerPoint > hostPoint {
		print("==========")
		print("　玩家勝　")
		print("==========")
	}
}

srand(UInt32(time(nil)))
game()




