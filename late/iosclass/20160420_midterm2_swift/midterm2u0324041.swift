func factorial(number: Int64) -> Int64 {
    if number <= 1 {
        return 1
    }else {
        return number * factorial(number - 1)
    }
    
}

for counter in 1...20 {
    print("\(counter)! = \(factorial(Int64(counter)))")
}
print("21! = \(((factorial(Int64(20)))/10000)*21)0000")