func factorial(number: Int64) ->Int64 {
    if number <= 1 {
        return 1
    } else {
        return number * factorial(number-1)
    } 
}

for count in 0...20{
    print("\(count)! = \(factorial(Int64(count)))")
}

var x = (factorial(Int64(20)) / 1000)
print("\(21)! = \(x*21)000")