public class fun1{
    public var a : Int64 = 1
    public var b : Int = 0
    public init (){
        a = 1
        b = 0
    }
    public func f(n : Int){
        a = 1
        b = 0
       for i in 1...n{
            a = a * Int64( i ) 
            while( a % 10 == 0){
                b = b + 1
                a = a / 10
            }
            display(i)
       }
    }
    public func display(i : Int){
        print("\(i)! = ",terminator: "")
        print("\(a)",terminator: "")
        if(b != 0){
            for i in 1...b{
                print("0",terminator: "")
            }   
        }
        print("\n",terminator: "")
    }
}
var f : fun1 = fun1()
f.f(21)