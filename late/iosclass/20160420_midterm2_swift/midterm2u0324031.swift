func factorial(n: Double) -> Double {
    if n <= 1{
        return 1
    }else{
        return n * factorial(n-1)
    }
}

func DoubleStringToIntString(inout str: String) -> String{
    var i: Int = 0
    var IntString: String = ""
    if Array(str.characters)[str.characters.count-2] == "." {
        IntString = String(Array(str.characters)[0...str.characters.count-3])
    }else{
        while(Array(str.characters)[i] != "e"){
            if Array(str.characters)[i] != "."{
                IntString += String(Array(str.characters)[i])
            }
            i++
        }
        for c in i...Int(String(Array(str.characters)[str.characters.count-2...str.characters.count-1]))!+1{
            IntString += "0"
        }
    }
    return IntString
}

var f: String
for i in 0...21{
    f = String(factorial(Double(i)))
    print("\(i)! = \(DoubleStringToIntString(&f))")
}
