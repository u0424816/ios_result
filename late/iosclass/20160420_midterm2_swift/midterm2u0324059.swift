func factorial(number : Int64) -> Int64
{
    if number <= 1
    {
        return 1
    }
    else
    {
        if(number%5 == 0)
        {
            return (number * factorial(number-1))/10
        }
        else
        {
            return number * factorial(number-1)
        }
    }
}

var s : String = ""
var a = 0
for c in 0...23
{
    s += "\(factorial(Int64(c)))"
    
    for(a = 0; a < (c/5); a+=1)
    {
        s += "0"
    }
    
    print("\(c)! = \(s)")
    s = ""
}
