func f(n:Int) -> [Int] {
    if n == 1 {
        return [1, 0]
    } else {
        var tmp:[Int] = f(n - 1)
        tmp[0] *= n
        while (tmp[0] % 10 == 0) {
            tmp[0] /= 10
            tmp[1] += 1
        }
        return tmp
    }
}

for i in 20...22 {
    var t:[Int] = f(i)
    var result = "\(t[0])"
    while (t[1] != 0) {
        result = result + "0"    
        t[1] -= 1
    }
    print("f(\(i)) = \(result)")
}
