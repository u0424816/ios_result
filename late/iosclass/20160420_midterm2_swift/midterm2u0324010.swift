class factorial{

    func fac(number: Double) -> (Double) {
    if (number <= 1) {
        return 1
    }
        
    return number * fac(number - 1)
}
}
var fact = factorial()

for count in 0...21{
    print("\(count) = \(fact.fac(Double(count)))")
}