func f(input:Int){
    var sum:Double=1
    for i in 1...input{
        sum = sum * Double(i)
    }
    print("\(input)! = \(sum)")
}
func main(){
    for i in 1...30{
        f(i)
    }
}
main()
