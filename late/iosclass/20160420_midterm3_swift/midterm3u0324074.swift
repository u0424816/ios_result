import Foundation
var n = 0
var blockDigit:NSString = "%09d"
var block = 1000000000
while(n<=1000000){
    var ans1 = [Int](count:1,repeatedValue:1)
    var ans2 = [Int](count:1,repeatedValue:0)
    var sum = [Int](count:1,repeatedValue:1)
    for var i=1;i<=n;i++ {
        var temp = 0
        for var j=0 ;j<ans1.count;j++ {
            sum[j]=ans1[j]+ans2[j]+temp
            temp=sum[j]/block
            sum[j]%=block
            ans1[j]=ans2[j]
            ans2[j]=sum[j]
            if(temp != 0 && j+1 >= sum.count){
                ans1.append(0)
                ans2.append(0)
                sum.append(0)
            }
        }
    }    
    print("F\(n)",terminator:" : ")
    print(ans2[sum.count-1],terminator:"")
    for var i=sum.count-2;i>=0;i-- {
        print(NSString(format:blockDigit,ans2[i]),terminator:"")
    }
    print("")
    n++
}