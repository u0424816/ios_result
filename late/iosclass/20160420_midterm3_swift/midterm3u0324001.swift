func fibonacci (number: Double) -> (Double) {
    if number <= 1 {
        return number 
    } else {
        return fibonacci(number-1) + fibonacci(number-2)
    }
}

var fibNumber = fibonacci(174299)
print("The 174299th Fibonacci number is: \(fibNumber)")