func fibonacci(number: Int) -> (Int) {
    if number <= 1 {
        return number
    } else {
        return fibonacci(number - 1) + fibonacci(number - 2)
    }
}

for show in 1...25{
    print("The \(show)th Fibonacci number is: \(fibonacci(show))")
}
