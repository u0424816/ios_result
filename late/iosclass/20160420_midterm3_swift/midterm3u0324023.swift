func f(num : Int) -> Int{
    if num == 0{
        return 0
    }else if num == 1{
        return 1
    }else {
        return f(num-1)+f(num-2)
    }
}
/*for i in 0...43{
   print("F(\(i)) = \(f(i))") 
}*/
//print(" = \(f(4))") 
func f2(num : Int) -> Int{
    var total = 0
    var a = 0
    var b = 0
    var c = 0
    for i in 0...num{
        if i==1{
            c = 1
        }else if i == 2{
            b = c
            c = 0
        }else{
            a = a+b
            b = c
            c = a

        }

    }
    total = a+b+c
    return total
}
for i in 0...92{
   print("F(\(i)) = \(f2(i))") 
}


