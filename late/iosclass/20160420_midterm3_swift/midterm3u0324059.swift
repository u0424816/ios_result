func F2(n : Int64) -> Int64
{
    if(n <= 2)
    {
        return 1
    }
    
    return F2(n-1) + F2(n-2)
}



print(F2(44))