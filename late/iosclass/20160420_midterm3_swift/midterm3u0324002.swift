import Foundation
var d = 1000000000
public class BigInteger {
    public var value:[Int] = []
    
    public init(n:Int){
        var tmp = Array(String(n).characters)
        for item in tmp {
            value.append(Int(String(item))!)
        }
        value = value.reverse()
        //print(value)
    }
    
    public func add(_another:BigInteger) {
        var another = _another
        while (self.value.count < another.value.count) {
            self.value.append(0)
        }
        for i in another.value.indices {
            self.value[i] += another.value[i]
            if self.value.count - 1 == i {
                break
            }
            self.value[i + 1] += self.value[i] / d
            self.value[i] = self.value[i] % d
        }
        if self.value[self.value.count - 1] >= d {
            self.value.append(self.value[self.value.count - 1] / d)
            self.value[self.value.count - 2] = self.value[self.value.count - 2] % d
        }
    }
    
    public func show() -> String {
        var result = ""
		var first = true
        for item in value.reverse() {
			if first {
				result += String(item)
				first = false
			} else {
	            result += String(NSString(format:"%09d", item))
			}
        }
        return result
    }
}
var a = BigInteger(n:0)
var b = BigInteger(n:1)
var ans = BigInteger(n:1)
func f(n:Int) -> BigInteger{
    switch n {
        case 0:
            //print("\(a.show())", terminator:":")
            return a
        case 1, 2:
            //print("\(b.show())", terminator:":")
            return b
        default:
        	var tmp = ans
            a.value = b.value
            b.value = ans.value
            tmp.add(a)
            ans.value = tmp.value
    }
    
    return ans
}

for i in 0..<100000 {
	f(i)
}
print("f(\(100000)) = \(f(100000).show())")

