func factorial (number: Double) ->Double {

        if number <= 1{
            
            return 1
        }else{
            
           return factorial(number-1) + factorial(number-2)
        }
}


for counter in 0...40{
    print("F\(counter) = \(factorial(Double(counter)))")
}