func face(n: Int) -> Int {
    var a = 0
    var b = 1
    for _ in 0..<n {
	let temp = a
	a = b
	b = temp + b
    }
    return a
}

for i in 0..<92{
    let result = face(i)
    print("¶O¤ó¼Æ¦C \(i) = \(result)")
}