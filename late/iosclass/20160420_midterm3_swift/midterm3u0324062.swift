//func f(input:Double)->Double{
//    if(input == 0){
//        return 0
//    }else if(input == 1){
//        return 1
//    }else{
//        return f(input-1)+f(input-2)
//    }
//}

func f2(input:Int)->Double{
    if((input==0)||input==1){
        return Double(input)
    }
    var temp:Double,a:Double,b:Double
    a=0
    b=1
    for i in 2...input{
        temp = b
        b = a+b
        a = temp
    }
    return b
}

//print(f(50))
print(f2(1476))
