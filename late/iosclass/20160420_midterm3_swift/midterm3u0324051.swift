func f (x: UInt) -> UInt {
    
    if( x == 0 ){
        return 0
    }else if ( x == 1 ){
        return 1
    }else{
        return f(x - 1) + f (x - 2)
    }
    
}

for i in 0 ... 41 {
    
    print (f(UInt(i)))
    
}