
func isPrefix(str:String, pattern:String) -> Bool {
	let pattern_array = [Character](pattern.characters)
	if (pattern_array.count > str.characters.count) {
		return false
	}
	let str_array = [Character]([Character](str.characters)[0..<pattern_array.count])
	return str_array == pattern_array
}

var a:String = "nkfust_mis"
var b:String = "nkfust"

print("Str1 = \(a)")
print("Pattern = \(b)")
print("isPrefix = \(isPrefix(a, pattern:b))")
		
print("===================================")
a = "mis"
print("Str1 = \(a)")
print("Pattern = \(b)")
print("isPrefix = \(isPrefix(a, pattern:b))")

print("===================================")

a = "nkfust_mis"
b = "mis"

