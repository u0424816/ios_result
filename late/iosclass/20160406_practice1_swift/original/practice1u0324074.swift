var str1 = "abcdabcddabcdddabc"
var str2 = "abc"
var str1Array = [String]()
var str2Array = [String]()
//頭是否一樣
var compare = true
for index in str1.characters {
    str1Array.append(String(index))
}
for index in str2.characters {
    str2Array.append(String(index))
}
for (var i = 0 ; i < str2Array.count ; i++){
    if(str1Array[i] != str2Array[i]){
        compare = false
    }
}
print(compare)
//一樣的有幾個
var count = 0
for (var i = 0 ; i < str1Array.count-2 ; i++){
    compare = true
    for (var j = 0 ; j < str2Array.count ; j++){
        if(str1Array[i+j] != str2Array[j]){
            compare = false
        }
    }
    if(compare == true){
        count++
    }
}
print(count)
//尾是否一樣
compare = true
for (var i = 0 ; i < str2Array.count ; i++){
    if(str1Array[i + str1Array.count - str2Array.count] != str2Array[i]){
        compare = false
    }
}
print(compare)
