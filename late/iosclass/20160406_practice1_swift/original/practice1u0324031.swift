var a: String = "cafe\u{301}00000"
var b: String = "caf\u{E9}"

func prefixCheck(str: String,_ cs: String) -> Bool {
    for index in 0...cs.characters.count-1 {
        if Array(str.characters)[index] != Array(cs.characters)[index]{
            return false
        }
    }
    return true
}

print(prefixCheck(a, b))