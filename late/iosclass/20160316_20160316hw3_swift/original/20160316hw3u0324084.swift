import Foundation

var YYYY = 2016
var MM = 03
var DD = 16


//檢查日期輸入問題
func isCorrectDate(y:Int, m:Int, d: Int) -> Bool {
    var d_:[Int] = [31,28,31,30,31,30,31,31,30,31,30,31]
    if ((y%4==0)&&(y%100 != 0)||(y%400==0)){ d_[1] = 29}
    if m<1 || m > 12 {
        return false
    }
    if d<1 || d > d_[m-1] {
        return false
    }
    return true
}

if isCorrectDate(YYYY, m: MM, d: DD){
    
    
    //  第一種方法 系統呼叫
    
    //宣告
    let dateFormatter = NSDateFormatter()
    let calendar = NSCalendar.currentCalendar()
    var dateComponents = NSDateComponents()
    
    //設定日期格式
    dateFormatter.locale = NSLocale(localeIdentifier: "zh_TW")
    dateFormatter.dateFormat = "EEEE"
    //指定日期
    dateComponents.year = YYYY
    dateComponents.month = MM
    dateComponents.day = DD
    let d = NSDate()
    var date = calendar.dateFromComponents(dateComponents)
    print(dateFormatter.stringFromDate(date!), terminator:"")
    
}else{
    print("error", terminator:"")
}




