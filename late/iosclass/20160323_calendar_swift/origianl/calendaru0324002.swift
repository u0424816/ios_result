var args:[String] = ["xyz", "20160318", "0"]
import Foundation

func isCorrectDate(y:Int, m:Int, d: Int) -> Bool {
	var d_:[Int] = [31,28,31,30,31,30,31,31,30,31,30,31]
	if ((y%4==0)&&(y%100 != 0)||(y%400==0)){ d_[1] = 29}
	if m<1 || m > 12 {
		return false
	}
	if d<1 || d > d_[m-1] {
		return false
	}
	return true
}

func getWeekDay(year:Int, month:Int, day:Int) -> Int{
	var ans = -1
	if isCorrectDate(year, m: month, d: day){
		//declare
		let dateFormatter = NSDateFormatter()
		let calendar = NSCalendar.currentCalendar()
		var dateComponents = NSDateComponents()
		//setting format
		dateFormatter.dateFormat = "e"
		//setting date
		dateComponents.year = year
		dateComponents.month = month
		dateComponents.day = day
		let date = calendar.dateFromComponents(dateComponents)
		ans = Int(dateFormatter.stringFromDate(date!))! - 1
		
	}else{
		ans = -1
	}
	return ans
}

var y = Int(Int(args[1])! / 10000)
var m = Int(Int(args[1])! % 10000 / 100)
var d = Int(Int(args[1])! % 100)
var weekDayStart = Int(args[2])!
if getWeekDay(y, month:m, day:d) != -1 {
	//Output_1
	print("\(y)\n\(m)\n\(d)")
	
	//Output_2
	var wdStr:[String] = ["日","一","二","三","四","五","六"]
	var wd = getWeekDay(y, month:m, day:d)
	print("\(wd == -1 ? "error" : "星期\(wdStr[wd])")")
	
	//Output_3
	
	for i in 0...6{
		print("\(wdStr[(i + weekDayStart) % 7])", terminator:"\t")
	}
	if weekDayStart != wd{ print()}
	d = 1
	wd = getWeekDay(y, month:m, day:1)
	d = d - (weekDayStart > wd ? (7 - (abs(weekDayStart - wd))) : abs(weekDayStart - wd))
	var w = 0
	while d < 32{
		wd = getWeekDay(y, month:m, day:d)
		var t = wd == -1 ? "" : String(d)
		if (wd == weekDayStart && w % 7 == 0) {
			print("", terminator:"\n")
		}
		print("\(t)\(wd == -1 && d > 28 ? "" : "\t")", terminator:"")
		
		d = d + 1
		w = w + 1
	}
	print("")
} else{
	print("error", terminator:"")
}
