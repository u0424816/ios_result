﻿var args:[String]=["xyz","20150318","1"]
extension String {
    subscript (r: Range<Int>) -> String {
        get {
            let startIndex = self.startIndex.advancedBy(r.startIndex)
            let endIndex = self.startIndex.advancedBy(r.endIndex)

            return self[Range(start: startIndex, end: endIndex)]
        }
    }
}
var YYYY:Int? = Int(args[1][0...3])
var MM:Int? = Int(args[1][4...5])
var DD:Int? = Int(args[1][6...7])
var w:Int,c:Int,y:Int,m:Int,d:Int
var check=true
var days:Int=0
var startday:Int? = Int(args[2])
switch MM! {
    case 1,3,5,7,8,10,12:
        if(DD!<1||DD!>31){
            check=false
        }
        days=31
        break
    case 4,6,9,11:
        if(DD!<1||DD!>30){
            check=false
        }
        days=30
        break
    case 2:
        days=28
        if(DD!<1||DD!>28){
            check=false
        }
        if(DD!==29){
             if (((YYYY! % 4 == 0) && (YYYY! % 100 != 0)) || (YYYY! % 400 == 0)){
                check=true
                days=29
            }else{
                check=false
            }
        }
    default:
        check=false
}

if(MM!<3){
    MM!+=12
    YYYY!-=1
}

c=(YYYY!)/100

y=YYYY!%100

m=MM!

d=DD!

w=0

w=(w+y+(y/4)+(c/4)-(2*c)+(26*(m+1))/10+d-1)%7

var daynum:[String] = ["日","一","二","三","四","五","六"]

func printdate(){
    let fdayweek = (w-(DD!-(DD!/7)*7)+1)
    var nullday = fdayweek-startday!-1
    if(nullday<0){
        nullday+=7
    }
    //print ("fdayweek \(fdayweek)")
    //print ("nullday \(nullday)")
    for index in 0...6{
        print ("\(daynum[(index+startday!)%7])\t",terminator: "")
    }
    print()
    
    if(fdayweek != startday!){
        for _ in 0...(nullday){
            print("\t",terminator: "")
        }
    }
    
    for index in 0...(days-1){
        print ("\(index+1)\t",terminator: "")
        if((nullday+index+2)%7==0){
            print()
        }
        
        
    }
    
}

if(check){
    print (YYYY!)
    print (MM!)
    print (DD!)
    print("星期\(daynum[w])")
    printdate()
}else{
    print("error")
}

