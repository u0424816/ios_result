import Foundation
import CoreFoundation
var makers=Int(rand()%12+1)//亂數
var play=0
public class Player
{
    public var name = ""
    public init(name : String )
    {
        self.name=name//存入玩家名稱
    }
      public func rollDice() ->(Int)//擲骰子
    {   
        var point : [Int] = [0,0,0,0,0,0,0]//存點數有幾個
        var die : [Int] = [0,0,0,0]//玩家擲的點數
        var temp = Set<Int>()//存點數，判斷用
        var sum : Int = 0//最後點數總合
        var max=0
        var str=""
        for i in 0...3
        {
            die[i]=Int(rand()%6+1)//亂數1-6
            temp.insert(die[i])
            point[die[i]] += 1
        }
        str+="\(name)->\(die) = "
        switch(temp.count)
        {
            case 3://有兩個相同
                for x in 0..<point.count
                {
                    if(point[x]==2)
                    {
                     temp.remove(x)   
                    }
                }
                for index in temp
                {   
                    sum+=index
                }
                str+="\(sum)"
            case 2://2個2個相同 or 三個相同
                if(point.contains(2))//判斷是兩個兩個相同情況
                {
                    for index in temp
                    {
                        if(index>max)
                        {
                            max=index
                        }
                    }
                    sum=max*2
                }
                else//三個相同情況
                {
                    for index in temp
                    {
                        sum+=index
                    }
                }
                str+="\(sum)"
            case 1://清一色
                for index in temp
                {
                    sum=index+12
                }
            default://都不同!!重骰
                sum=0
                str="\(name)->\(die) 無點重骰!!"
        }
        print(str)
        return (sum==0 ? rollDice() : sum)
    }
}
var Player1 : [Player] = [Player(name: "Player1"),Player(name: "Player2"),Player(name: "Player3")]  //增減玩家!!
for i in 0..<Player1.count//讀取玩家個數
{
    play=Player1[i].rollDice()
    if(makers>play)//判斷輸贏
    {
        print("makers: \(makers) > \(Player1[i].name): \(play) -> \(Player1[i].name) Lose!!\n")
    }
    else if(makers<play)
    {
        print("makers: \(makers) < \(Player1[i].name): \(play) -> \(Player1[i].name) Win!!\n")
    }
    else if(makers==play)
    {
        print("makers: \(makers) = \(Player1[i].name): \(play) -> equal!!\n")
    }
}






