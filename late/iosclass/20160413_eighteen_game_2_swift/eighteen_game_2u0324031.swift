public class Player{ 
    public var name: String
    public private(set) var die: (Int, Int, Int, Int)
    public private(set) var point: Int = 0
    public init(name: String){
        self.name=name
        die.0 = Int(rand()%6+1)
        die.1 = Int(rand()%6+1)
        die.2 = Int(rand()%6+1)
        die.3 = die.2
    }
    
    public func rollDice(){
        die.0 = Int(rand()%6+1)
        die.1 = Int(rand()%6+1)
        die.2 = Int(rand()%6+1)
        die.3 = die.2
    }
    
    public func displayRoll(){
        print("\(name): (\(die.0),\(die.1),\(die.2),\(die.3))")
    }
    
    public func rollAdd(){
        point = die.0 + die.1
        if die.0 == die.1 {
            if die.0 < die.2{
                point = die.2 + die.3
            }
        }
    }
    public func BG(master: Player) -> String{
        if die.0 == die.1 && die.0 == die.2 {
            if master.die.0 == master.die.1 && master.die.0 == master.die.2{
                if die.0 < master.die.0{
                    return "\(name) Lost"
                }else if die.0 == master.die.0 {
                    return "Equal"
                }else{
                    return "\(name) Won"
                }
            }else{
                return "\(name) Won"   
            }
        }else if master.die.0 == master.die.1 && master.die.0 == master.die.2{
            if die.0 == die.1 && die.0 == die.2 {
                if die.0 < master.die.0{
                        return "\(name) Lost"
                    }else{
                        return "\(name) Won"
                    }
                }
        }else{
            if master.point > point {
                return "\(name) Lost"
            }else if master.point < point{
                 return "\(name) Won"
            }else{
                return "Equal"
            }
            print("\(master.name) \(master.point) \n \(name): \(point)")
        }
        return "continue"
    }
}

import Foundation
import CoreFoundation
let time = UInt32(NSDate().timeIntervalSinceReferenceDate)
srand(time)

func playGame(player: Player){
   // player.rollDice()
    player.displayRoll()
    player.rollAdd()
    print(player.BG(master))
}

var master = Player(name: "master")
var player1 = Player(name: "East")
var player2 = Player(name: "North")

playGame(player1)
playGame(player2)

