import Foundation
import CoreFoundation
public class Game{
    public var name :String=""
    public init(name:String){
        self.name = name
    }
    public func rollStart()->(Int,Int,Int,Int){
        var roll = rollDice()
        while(roll.0 != roll.1 && roll.0 != roll.2 && roll.0 != roll.3 ){
            roll = rollDice()
        }
        return roll
    }
    public func rollDice()->(die1: Int,die2: Int,die3: Int,die4: Int){
        let die1 = Int(rand()%6+1)
        let die2 = Int(rand()%6+1)
        let die3 = Int(rand()%6+1)
        let die4 = Int(rand()%6+1)
        return(die1,die2,die3,die4)
    }
    public func rollCalculate(roll:(Int,Int,Int,Int))->Int{
        var result = 0
        if roll.0==roll.1 && roll.2==roll.3{
            if roll.0>roll.2{
                result = roll.0*2
            }else{
                result = roll.2*2
            }
        }else if roll.0==roll.2 && roll.1==roll.3{
            if roll.0>roll.1{
                result = roll.0*2
            }else{
                result = roll.1*2
            }
        }else if roll.0==roll.3 && roll.1==roll.2{
            if roll.0>roll.1{
                result = roll.0*2
            }else{
                result = roll.1*2
            }
        }else{
                if roll.0==roll.1{
                result = roll.2+roll.3
            }else if roll.0==roll.2{
                result = roll.1+roll.3
            }else if roll.0==roll.3{
                result = roll.1+roll.2
            }else if roll.1==roll.2{
                result = roll.0+roll.3
            }else if roll.1==roll.3{
                result = roll.0+roll.2
            }else if roll.2==roll.3{
                result = roll.0+roll.1
            }
        }
        return result
    }
    
    
    
}

var masterRollNum = 0
var masterRollNum2 = 0
var playerRollNum1 = 0
var playerRollNum2 = 0
let master = Game(name:"Master")
let player1 = Game(name:"Player1")
let player2 = Game(name:"Player2")
var run = true
var run1 = false
var run2 = false
var start1 = true
var start2 = true
func same(masterRoll:(Int,Int,Int,Int),_ playRoll:(Int,Int,Int,Int),_ name:String){
    if playRoll == masterRoll{
        print("Master and \(name) is equal!")
            if name == "Player1"{
                start1 = true
                run1 = true
            }else if name == "Player2"{
                start2 = true
                run2 = true
            }
    }else if (playRoll.0 == playRoll.1 && playRoll.1 == playRoll.2 && playRoll.2 == playRoll.3) || masterRoll.0 != masterRoll.1 || masterRoll.0 != masterRoll.2 || masterRoll.0 != masterRoll.3{
        print("\(name) Win!")
        if name == "Player1"{
                start1 = false
                run1 = false
        }else if name == "Player2"{
                start2 = false
                run2 = false
        }
    }else if masterRoll.0 == masterRoll.1 && masterRoll.1 == masterRoll.2 && masterRoll.2 == masterRoll.3 || playRoll.0 != playRoll.1 || playRoll.0 != playRoll.2 || playRoll.0 != playRoll.3{
        print("Master Win!")
        if name == "Player1"{
                start1 = false
                run1 = false
        }else if name == "Player2"{
                start2 = false
                run2 = false
        }
        
    }else{
        if masterRoll.0>playRoll.0{
            print("Master Win!")
            if name == "Player1"{
                    start1 = false
                    run1 = false
            }else if name == "Player2"{
                    start2 = false
                    run2 = false
            }
        }else if masterRoll.0<playRoll.0{
            print("\(name) Win!")
            if name == "Player1"{
                    start1 = false
                    run1 = false
            }else if name == "Player2"{
                    start2 = false
                    run2 = false
            }
        }
    }
}

func result(masterRoll:(Int,Int,Int,Int),_ playRoll:(Int,Int,Int,Int),_ masterRollNum:Int,_ playerRollNum:Int,_ name:String){
    if playRoll == masterRoll{
        print("Master and \(name) is equal!")
            if name == "Player1"{
                start1 = true
                run1 = true
            }else if name == "Player2"{
                start2 = true
                run2 = true
            }
    }else if (playRoll.0 == playRoll.1 && playRoll.1 == playRoll.2 && playRoll.2 == playRoll.3) || (masterRoll.0 == masterRoll.1 && masterRoll.1 == masterRoll.2 && masterRoll.2 == masterRoll.3){
        same(masterRoll,playRoll,name)
        
    }else{
        if masterRollNum > playerRollNum{
            print("\(name) Lose!")
            if name == "Player1"{
                start1 = false
                run1 = false
               
            }else if name == "Player2"{
                start2 = false
                run2 = false
            }
        }else if masterRollNum < playerRollNum{
            print("\(name) Win!")
            if name == "Player1"{
                start1 = false
                run1 = false
            }else if name == "Player2"{
                start2 = false
                run2 = false
            }
        }else{
            print("Master and \(name) is equal!")
            if name == "Player1"{
                start1 = true
                run1 = true
            }else if name == "Player2"{
                start2 = true
                run2 = true
            }
        }
    }
}
func displayRoll(name:String,_ roll:(Int,Int,Int,Int),_ result:Int){
    if(roll.0 == roll.1 && roll.1 == roll.2 && roll.2 == roll.3){
        print("\(name): \(roll)")
    }else{
        print("\(name): \(roll)=\(result)")
    }
}

var mdiffient = true
var p1diffient = true
var p2diffient = true
var playRoll1 = (0,0,0,0)
var playRoll2 = (0,0,0,0)
var masterRoll = (0,0,0,0)
var masterRoll2 = (0,0,0,0)
var dieSame = true

while(start1||start2){

    masterRoll=master.rollStart()
    masterRoll2 = masterRoll
    //print("+++++++++\(masterRoll2)")
    playRoll1=player1.rollStart()
    playRoll2=player2.rollStart()
    masterRollNum = master.rollCalculate(masterRoll)
    masterRollNum2 = master.rollCalculate(masterRoll2)
    playerRollNum1 = player1.rollCalculate(playRoll1)
    playerRollNum2 = player2.rollCalculate(playRoll2)
    displayRoll(master.name,masterRoll,master.rollCalculate(masterRoll))
    displayRoll(player1.name,playRoll1,player1.rollCalculate(playRoll1))
    displayRoll(player2.name,playRoll2,player2.rollCalculate(playRoll2))
    /*print("\(master.name): \(masterRoll)=\(master.rollCalculate(masterRoll))")
    print("\(player1.name): \(playRoll1)=\(player1.rollCalculate(playRoll1))")
    print("\(player2.name): \(playRoll2)=\(player2.rollCalculate(playRoll2))")*/
    print("-----------------------------------------------------------")
    while(start1){
        result(masterRoll,playRoll1,masterRollNum,playerRollNum1,"Player1")
        while(run1){
            masterRoll=master.rollStart()
            masterRollNum = master.rollCalculate(masterRoll)
            displayRoll(master.name,masterRoll,master.rollCalculate(masterRoll))
            playRoll1=player1.rollStart()
            playerRollNum1 = player1.rollCalculate(playRoll1)
            displayRoll(player1.name,playRoll1,player1.rollCalculate(playRoll1))
            result(masterRoll,playRoll1,masterRollNum,playerRollNum1,"Player1")
        }
    }
    print("-----------------------------------------------------------")
    while(start2){
        result(masterRoll2,playRoll2,masterRollNum2,playerRollNum2,"Player2")
        //print("\(masterRollNum2)*******\(playerRollNum2)")
        while(run2){
            masterRoll2=master.rollStart()
            masterRollNum = master.rollCalculate(masterRoll2)
            displayRoll(master.name,masterRoll2,master.rollCalculate(masterRoll2))
            playRoll2=player2.rollStart()
            playerRollNum2 = player2.rollCalculate(playRoll2)
            displayRoll(player2.name,playRoll2,player2.rollCalculate(playRoll2))
            result(masterRoll2,playRoll2,masterRollNum,playerRollNum2,"Player2")
        }
    
    }    
    
}







