import Foundation
import CoreFoundation
public class Player{
    public var name: String = ""
    public var point: Int = 0
    public var dice = [Int]()
    public init(name: String){
        self.name = name
    }
    public func roll(){
        var GaCo = true
        while GaCo{
            dice = [Int]()
            dice.append(Int(rand()%6+1))
            dice.append(Int(rand()%6+1))
            dice.append(Int(rand()%6+1))
            dice.append(Int(rand()%6+1))
            dice.sort()
            for i in 0...dice.count-2{
        
                if (GaCo){
            
                    for ii in i+1...dice.count-1{
                    
                        if (dice[i] == dice[ii]){
                            GaCo = false
                           self.point = dice[0]+dice[1]+dice[2]+dice[3]-(2*dice[i])
                        }
                    }
                }
            }
        }
    }
    public func showpoint(){
        print("\(name)")
        print(dice)
        print("Point is \(point)\n")
    }
    public func getpoint() -> Int{
        return(point)
    }
   
}

let player0 = Player(name: "player0")
let player1 = Player(name: "player1")
player0.roll()
player1.roll()
player0.showpoint()
player1.showpoint()

var boss = Int(rand()%10+3)
print("\n\nMaker's point : \(boss)\n\(player0.name)'s point : \(player0.getpoint())\n")

if (player0.getpoint() > boss) {
    print("\(player0.name) win!!")
}else{
    print("Maker win!!")
}

print("\n\nMaker's point : \(boss)\n\(player1.name)'s point : \(player1.getpoint())\n")

if (player1.getpoint() > boss) {
    print("\(player1.name) win!!")
}else{
    print("Maker win!!")
}
