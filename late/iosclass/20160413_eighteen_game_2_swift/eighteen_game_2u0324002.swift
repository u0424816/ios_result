import Foundation
import CoreFoundation

public enum Status{
	case WIN
	case LOSE
	case TIE
}

public class Dicies{
	public var name:String = ""
	public var dice:[Int] = [0,0,0,0]
	public private(set) var point = 0
	public private(set) var isBowZi = false
	private var isReRoll = false
	public init(_name:String) {
		self.name = _name
		start()
	}

	private func start() {
		isBowZi = false
		dice = [0,0,0,0]
		point = 0
		print("\(name)：", terminator:"\n")
		roll()
		show()
	}

	public func roll() {
		repeat {
			if dice[0] != 0 {
				print("\(isReRoll ? "重骰：" : "")\t擲出：\(dice.sort())")
				isReRoll = true
			}
			point = 0
			for i in 0...3 {
				dice[i] = Int(rand()) % 6 + 1
				point += dice[i]
			}
		} while (isSameOrDiff(0))
		dice = dice.sort()
		countPoint()
	}

	private func isSameOrDiff(which:Int) -> Bool {
		for i in 0...dice.count-2 {
			for j in i+1...dice.count-1 {
				if which == 0 {
					if dice[i] == dice[j] {
						return false
					}
				} else {
					if dice[i] != dice[j] {
						return false
					}
				}
			}
		}
		return true
	}

	private func countPoint() -> Int{
		var d = 1
		var _dice = dice.sort()
		var isEnd:Bool = false
		if isSameOrDiff(1) {
			isBowZi = true
		} else {
			repeat{
				dice = dice.sort()
				while (dice.indexOf(d) != nil) {
					dice.removeAtIndex(dice.indexOf(d)!)
				}
				switch dice.count {
					case 1:
						dice.append(d)
						point -= d*2
						isEnd = true
					case 2:
						point -= d*2
						isEnd = true
					case 4:
						isEnd = false
					default:
						dice.append(d)
						isEnd = false
				}
				d++
			} while (!isEnd)
		}
		dice = _dice
		return point
	}

	public func show() {
		print("\(isReRoll ? "重骰：" : "")\t擲出：\(dice)")
		if isBowZi {
			print("\t擲出豹子")
		} else {
			print("\t點數為：\(point)")
		}
	}

	public func compare(player:Dicies) -> Status {
		if point == player.point || (isBowZi && player.isBowZi){
			return Status.TIE
		} else if isBowZi {
			return Status.WIN
		} else if player.isBowZi {
			return Status.LOSE
		} else if point > player.point{
			return Status.WIN
		} else if player.point > point {
			return Status.LOSE
		}
		return Status.TIE
	}
}
srand(UInt32(time(nil)))
print("===============================")
var host = Dicies(_name:"莊家")
print("\n===============================")

for p in 0..<5 {
	var player = Dicies(_name:"玩家\(p+1)") 
	while (host.compare(player) == Status.TIE) {
		print("\n\t\t>>平手，重骰<<\n")
		player.roll()
		player.show()
	}
	if host.compare(player) == Status.WIN {
		print("\n\t\t>>\(host.name)　勝<<")
	} else {
		print("\n\t\t>>\(player.name) 勝<<")
	}
	print("-------------------------------")
}

