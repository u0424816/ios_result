import UIKit
class ViewController: UIViewController {
    private var model = TestModel()
    private var isFirstNumber = true
    
    @IBOutlet weak var display: UILabel!
    @IBAction func clickNumber(sender: UIButton) {
        let number = sender.currentTitle!
        if(display.text == "0"){
            isFirstNumber = true
        }
        if(isFirstNumber){
            display.text = number
            isFirstNumber = false
        }else{
            display.text = display.text! + number
        }
    }
    @IBAction func clicksymbol(sender: UIButton) {
        let symbol = sender.currentTitle!
        model.setOperand(display.text!)
        model.performOperation(symbol)
        isFirstNumber = true
        display.text = model.result
    }
    @IBAction func clickDecimal(sender: UIButton) {
        let checkDecimal : Double? = Double(display.text! + ".")
        if(checkDecimal != nil){
            display.text = display.text! + "."
        }
    }
}