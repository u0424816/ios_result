import Foundation
func pp(w : String) -> Int{
    var a : Int = 0
    switch w {
    case "+" , "-":
        a = 1
    case "x" , "/" :
        a = 2
    default:
        a = 3
    }
    return a
}
func calculator(strInput : String) -> Double{
    let inp = Array(strInput.characters)
    var stPx = Array<String>()
    var stpx2 = Array<String>()
    var icp = 0
    var isp = 0
    var strtmp = ""
    var lasticp = 0
    var a , b : Double
    for index in inp.indices{
        icp = pp(String(inp[index]))
        if(index == 0){
            strtmp = String(inp[index])
        }else{
            if(lasticp != 3){
                strtmp += String(inp[index])
            }else{
                if(icp == 3){
                    strtmp += String(inp[index])
                }else{
                    stPx.append(strtmp)
                    strtmp = ""
                    if(stpx2.count == 0){
                        stpx2.append(String(inp[index]))
                    }else{
                        isp = pp(stpx2[0])
                        while(isp >= icp && stpx2.count > 0){
                            a = Double(stPx.removeLast())!
                            b = Double(stPx.removeLast())!
                            if(stpx2[0] == "+"){stPx.append(String( b + a)) }
                            if(stpx2[0] == "-"){stPx.append(String( b - a)) }
                            if(stpx2[0] == "x"){stPx.append(String( b * a)) }
                            if(stpx2[0] == "/"){stPx.append(String( b / a)) }
                            stpx2.removeAtIndex(0)
                            if(stpx2.count > 0){isp = pp(stpx2[0])}
                        }
                        stpx2.insert(String(inp[index]),atIndex : 0)
                    }
                }
            }
        }
        lasticp = icp
    }
    stPx.append(strtmp)
    while(stpx2.count > 0){
        a = Double(stPx.removeLast())!
        b = Double(stPx.removeLast())!
        if(stpx2[0] == "+"){stPx.append(String( b + a)) }
        if(stpx2[0] == "-"){stPx.append(String( b - a)) }
        if(stpx2[0] == "x"){stPx.append(String( b * a)) }
        if(stpx2[0] == "/"){stPx.append(String( b / a)) }
        stpx2.removeAtIndex(0)
    }
    return Double(stPx[0])!
}
class TestModel{
    enum Operation{
        case Constant(Double)
        case UnaryOpetation((Double) -> Double)
        case BinaryOperation((Double , Double) -> Double)
        case Equals
        case Clear
        case AC(String)
        case Percentage
        case Factorial
        case MemoryOperation((Double) -> Double)
        case MemoryClear
        case MemoryEquals
    }
    struct PendingBinaryOperationInfo {
        var binaryFunction : (Double, Double) -> Double
        var firstOperand: Double
    }
    private var pending : PendingBinaryOperationInfo?
    private var tmp = ""
    private var tmp2 = 0.0//memory
    private var tmp3 = 0.0//2=
    private var hasFirstEquals = false
    private var tmpvalue : Double?{
        get{
            return Double(tmp)
        }
    }
    var formular = ""
    var result : String{
        get{
            if(tmpvalue == nil){
                return tmp
            }else{
                if(String(tmpvalue!) == "inf" || String(tmpvalue!) == "nan" || String(tmpvalue!) == "-inf" || String(tmpvalue!) == "-nan"){
                    performOperation("C")
                    return "異常 : 無法計算"
                }else{
                    if(tmpvalue <= Double(INT64_MAX)){
                        if(tmpvalue! % 1 == 0){
                            return String(Int64(tmpvalue!))
                        }else{
                            return tmp
                        }
                    }else{
                        return tmp
                    }
                }
            }
        }
    }
    var ishead : Bool = true
    var operations : Dictionary<String, Operation> = [
        "C" : Operation.Clear,
        "e" : Operation.Constant(M_E),
        "PI" : Operation.Constant(M_PI),
        "sqr" : Operation.UnaryOpetation(sqrt),
        "cos" : Operation.UnaryOpetation(cos),
        "x" : Operation.BinaryOperation({$0 * $1 }),
        "=" : Operation.Equals,
        "+/-" : Operation.UnaryOpetation({$0 * -1 }),
        "+" : Operation.BinaryOperation({$0 + $1 }),
        "-" : Operation.BinaryOperation({$0 - $1 }),
        "/" : Operation.BinaryOperation({$0 / $1 }),
        "AC" : Operation.AC("0324004 : 李婉瑄"),
        "sin" : Operation.UnaryOpetation(sin),
        "tan" : Operation.UnaryOpetation(tan),
        "%" : Operation.Percentage,
        "CE" : Operation.Constant(0.0),
        "!" : Operation.Factorial,
        "M+" : Operation.MemoryOperation({+$0}),
        "M-" : Operation.MemoryOperation({-$0}),
        "MR" : Operation.MemoryEquals,
        "MC" : Operation.MemoryClear
    ]
    func setOperand(operand : String){
        tmp = operand
    }
    func performOperation(symbol : String){
        if(tmpvalue == nil){
            clear()
        }
        if let operation = operations[symbol]{
            switch operation{
            case Operation.Constant(let value): tmp = String(value)
            case Operation.UnaryOpetation(let function): tmp = String(function(tmpvalue!))
            case Operation.BinaryOperation(let function) :
                formular = formular + result + symbol
                if(ishead == false){
                    if pending != nil{
                        equal()
                    }
                }
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand : tmpvalue!)
                ishead = false
            case Operation.Equals:
                if pending != nil{
                    formular = formular + result
                    tmp3 = calculator(formular)
                    formular = ""
                    hasFirstEquals = true
                    equal()
                }else{
                    if(hasFirstEquals == true){
                        tmp = String(tmp3)
                        hasFirstEquals = false
                    }
                }
            case Operation.Clear :
                clear()
            case Operation.AC(let value): tmp = value
            case Operation.Percentage :
                if(pending?.firstOperand == nil){
                    tmp = String(tmpvalue! / 100)
                }else{
                    tmp = String((pending?.firstOperand)! * tmpvalue! / 100.0)
                }
            case Operation.Factorial :
                if(tmpvalue! % 1 == 0 && tmpvalue > 0 && tmpvalue <= 20){
                    var sum : Int64 = 1
                    for index in 1...Int64(tmpvalue!){
                        sum = sum * Int64(index)
                    }
                    tmp = String(sum)
                }else{
                    tmp = "異常 : 無法計算"
                }
            case Operation.MemoryOperation(let function) :
                tmp2 = tmp2 + function(tmpvalue!)
            case Operation.MemoryClear :
                tmp2 = 0.0
            case Operation.MemoryEquals :
                tmp = String(tmp2)
            }
        }
    }
    func equal(){
        tmp = String(pending!.binaryFunction(pending!.firstOperand, tmpvalue!))
        pending = nil
        ishead = true
    }
    func clear(){
        tmp = "0"
        formular = ""
        pending = nil
        ishead = true
        hasFirstEquals = false
    }
}
