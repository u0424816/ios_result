//
//  ViewController.swift
//  u0325075
//
//  Created by nkfust29_2 on 2016/6/15.
//  Copyright © 2016年 nkfust29_2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var flag = false
    var CaculateId = ""
    var priviousNum = 0.0
    
    private var model = TestModel()
    
    var displayValue: Double{
        get {
            return Double(display.text!)!
        }
        set{
            display.text = String(newValue)
        }
    }
    @IBAction func Caculate(sender: UIButton) {
        if flag == true {
            model.setOperand(displayValue)
            flag = false
        }
        if let mathSymbol = sender.currentTitle{
            model.performOperation(mathSymbol)
        }
        displayValue = model.result
    }
    @IBAction func showmsg(sender: UIButton) {
        let msg = sender.currentTitle!
        if flag{
            display.text! += msg
        }else if display.text == "0" && msg == "0"{
            display.text = "0"
        }else{
            display.text = msg
            flag = true
        }
        if(msg == "C"){
            display.text = "0"
            flag = false
        }else if (msg == "A"){
            display.text = "0324075 陳文聖"
            flag = false
        }
    }

    @IBOutlet weak var display: UILabel!

}

