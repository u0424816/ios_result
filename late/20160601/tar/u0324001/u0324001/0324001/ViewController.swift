//
//  ViewController.swift
//  0324001
//
//  Created by nkfust29 on 2016/5/11.
//  Copyright © 2016年 nkfust29. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var flag = false
    var CaculateId = ""
    var priviousNum = 0.0
    
    private var model = TestModel()
    
    var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            display.text = String(newValue)
        }
    }
    @IBAction func Caculate(sender: UIButton) {
        if flag == true {
            model.setOperand(displayValue)
            flag = false
        }
        if let mathSymbol = sender.currentTitle{
            model.performOperation(mathSymbol)
        }
        displayValue = model.result
    }

    @IBOutlet weak var display: UILabel!
    @IBAction func showmsg(sender: UIButton) {
        let msg = sender.currentTitle!
        if flag{
            display.text! += msg
        }else if display.text == "0" && msg == "0"{
            display.text = "0"
        }else{
            display.text = msg
            flag = true
        }
        if(msg == "C"){
            display.text = "0"
            flag = false
        }else if (msg == "A"){
            display.text = "0324001 張智威"
            flag = false
        }
//            if(display.text == "Label" || display.text == "0" || display.text == "0324001 張智威"){
//                display.text = msg
//            }else{
//                display.text! += msg
//            }
//        }
//    
//        @IBAction func clean(sender: UIButton) {
//            let msg = "0"
//            display.text = msg
        }
        
        
//        @IBAction func Showabout(sender: UIButton) {
//            let msg : String = "0324001 張智威"
//            display.text = msg
//        }

    
    
}
