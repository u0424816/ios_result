//
//  text.swift
//  123
//
//  Created by nkfust18 on 2016/5/18.
//  Copyright © 2016年 nkfust18. All rights reserved.
//

import Foundation

func multiply(op1: Double, op2: Double) -> Double {
    return op1 * op2
}
func plus(op1: Double, op2: Double) -> Double {
        return op1 + op2
}
func minus(op1: Double, op2: Double) -> Double {
            return op1 - op2
}
func devide(op1: Double, op2: Double) -> Double {
                return op1 / op2
}

class text{
    private var temp = 0.0
    
    func setOperand(operand: Double){temp = operand}
    
        enum Operation {
        case Constant(Double)
        case UnaryOpetaion ((Double) -> Double)
        case BinayOperation ((Double ,Double) -> Double)
        case Equals
    }
    var operations: Dictionary<String, Operation> = [
        "根號": Operation.UnaryOpetaion(sqrt),
        "拍": Operation.Constant(M_PI),
        "e": Operation.Constant(M_E),
        "cos": Operation.UnaryOpetaion(cos),
        "+": Operation.BinayOperation(plus),
        "-": Operation.BinayOperation(minus),
        "x": Operation.BinayOperation(multiply),
        "/": Operation.BinayOperation(devide),
        "=": Operation.Equals
    ]
    

    func performOperation(symbol: String) {
        
        if let operation = operations[symbol] {
            switch operation {
                case Operation.Constant(let value): temp = value
                case Operation.UnaryOpetaion(let function): temp = function(temp)
                case Operation.BinayOperation(let function):
                    if pending == nil{
                        pending = PendingBinaryOperationInfo(binaryFuncion: function, firstOperand: temp)
                    }else{
                        temp = pending!.binaryFuncion(pending!.firstOperand, temp)
                        pending = PendingBinaryOperationInfo(binaryFuncion: function, firstOperand: temp)
                    }
                case Operation.Equals:
                    if pending != nil {
                        temp = pending!.binaryFuncion(pending!.firstOperand, temp)
                        pending = nil
                    }
            }
        }
    }
    
    private var pending: PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFuncion: (Double,Double) -> Double
        var firstOperand: Double
    }
    
    var result: Double {
        get{
            return temp
        }
    }
}
