// AC：姓名學號 n! 按兩次等號：先加減後乘除 異常：無法計算
import Foundation
//
//func multiply(op1: Double, op2: Double) -> Double{
//    print("Multiply")
//    return op1 * op2
//}
class TestModel{
    private var temp = 0.0 //temp是Operand從外面傳進來更改的，所以不用public
    func setOperand(operand: Double){
        temp = operand
    }
    
    var operations: Dictionary<String, Operation> = [ //方法套用型別
        "π": Operation.Constant(M_PI),
        "e": Operation.Constant(M_E),
        "√": Operation.UnaryOperation(sqrt),
        "+/-": Operation.UnaryOperation({0-$0}),
        "cos": Operation.UnaryOperation(cos),
        "sin": Operation.UnaryOperation(sin),
        "x": Operation.BinaryOperation(*),//負號也是Unary
        "=": Operation.Equals,
        "+": Operation.BinaryOperation(+),
        "c": Operation.nuil,
        "-": Operation.BinaryOperation(-),//小數點用Binary做Equals
        "/": Operation.BinaryOperation(/),
        ".": Operation.BinaryOperation({$1/10+$0}),
        "%": Operation.BinaryOperation({$0/100+$1}),
        "AC": Operation.Constant(0324020)
        ]
    
    enum Operation { //宣告型別
    
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
        case nuil
       // case str(String)
        
    }
    
    func performOperation(symbol: String){ //定義型別要幹麻
        if let operation = operations[symbol]  {
            print("operation is \(operation)")
            switch operation {
            case Operation.Constant(let value): temp = value
            case Operation.UnaryOperation(let function): temp = function(temp)
            case Operation.BinaryOperation(let function): pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: temp)
            case Operation.Equals:execut()
            case Operation.nuil: temp = 0.0
            //case Operation.str()
            }
        }  }
    
    private func execut(){
        if pending != nil{
        temp = pending!.binaryFunction(pending!.firstOperand,temp)
            pending = nil
        }}
    private var pending: PendingBinaryOperationInfo? //?-->可以是空值
    
    struct PendingBinaryOperationInfo { //加號的類別
        var binaryFunction: (Double, Double) -> Double
        var firstOperand: Double
    }
    var result: Double{
        get {
            print("\(temp)")
            return temp //內部updata
        }
        
        
    }}