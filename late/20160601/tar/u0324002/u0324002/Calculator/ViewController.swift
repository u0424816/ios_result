//
//  ViewController.swift
//  Calculator
//
//  Created by Dien-Ju, Lin on 5/18/16.
//  Copyright © 2016 Dien-Ju, Lin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view, typically from a nib.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }

    @IBOutlet weak var txtDisplay: UITextField!
    var m: Model = Model()
    var isTyping: Bool = false
	var hasDot: Bool = false
    
	var display: Double{
		get{
            return Double(txtDisplay.text!)!
        }
        set{
			if (newValue % 1 == 0){
				txtDisplay.text = String(Int(newValue))
			}else{
				txtDisplay.text = String(newValue)
			}
        }
    }
    
    @IBAction func digitButton(sender: UIButton){
        var temp = String(sender.currentTitle!)
        if (!isTyping || txtDisplay.text == "0"){
            txtDisplay.text = ""
			if (!hasDot && temp == "."){
				txtDisplay.text = "0"
				hasDot = true
			}
            isTyping = true
		}else if (!hasDot){
			hasDot = true
		}else if (isTyping && hasDot && temp == "."){
			temp = ""
		}
        txtDisplay.text = txtDisplay.text! + temp
        
    }
    
    @IBAction func functionButton(sender: UIButton) {
        //print(txtDisplay.text)
        switch (sender.currentTitle!){
            case "C":
                display = 0
            case "A":
                txtDisplay.text = "u0324002林典儒"
				m = Model()
            default:
                if (Double(txtDisplay.text!) != nil){
                    m.setNumber(display)
                    m.setOp(sender.currentTitle!)
                    display = m.displayNumber
                }
        }
        isTyping = false
		hasDot = false
    }

}

