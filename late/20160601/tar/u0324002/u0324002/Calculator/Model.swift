//
//  Model.swift
//  Calculator
//
//  Created by Dien-Ju, Lin on 5/24/16.
//  Copyright © 2016 Dien-Ju, Lin. All rights reserved.
//

import Foundation

func _factorial(n:Int) -> [Int] {
	if n == 1 {
		return [1, 0]
	} else {
		var tmp:[Int] = _factorial(n - 1)
		tmp[0] *= n
		while (tmp[0] % 10 == 0) {
			tmp[0] /= 10
			tmp[1] += 1
		}
		return tmp
	}
}

func factorial(n: Double) -> Double {
	var int: Int = Int(n)
	var t = _factorial(int)
	var str: String = ""
	for (var i = 0, l = t.count ; i < l ; i++){
		str += String(t[i])
	}
	return Double(str)!
}

class Model{
	
    private var temp: Double = 0.0
	
    func setNumber(n: Double){
        temp = n
    }
	
	enum Op {
		case Constant(Double)
		case UnaryOperation((Double) -> Double)
		case BinaryOperation((Double, Double) -> Double)
		case Equals
	}
	
	struct PendingBinaryOperationInfo {
        var symbol: String
		var function: (Double, Double) -> Double
		var firstOperand: Double
	}
	
	var operation: PendingBinaryOperationInfo?
	
	var ops: Dictionary<String, Op> = [
		"π": Op.Constant(M_PI),
		"Exp": Op.Constant(M_E),
		"√": Op.UnaryOperation(sqrt),
		"Cos": Op.UnaryOperation(cos),
		"Sin": Op.UnaryOperation(sin),
		"Tan": Op.UnaryOperation(tan),
		"±": Op.UnaryOperation({-$0}),
		"!": Op.UnaryOperation(factorial),
		"+": Op.BinaryOperation(+),
		"-": Op.BinaryOperation(-),
		"×": Op.BinaryOperation(*),
		"÷": Op.BinaryOperation(/),
		"=": Op.Equals
		
	]
	
    func setOp(symbol: String){
		if let op = ops[symbol] {
			switch op {
				case .Constant(let value):
					temp = value
				case .UnaryOperation(let foo):
					temp = foo(temp)
				case .BinaryOperation(let foo):
					if operation == nil {
                        operation = PendingBinaryOperationInfo(symbol: symbol, function: foo, firstOperand: temp)
					} else {
						temp = operation!.function(operation!.firstOperand, temp)
                        if (operation!.symbol == symbol){
                            let t = foo(operation!.firstOperand, temp)
                            operation?.firstOperand = t
                            print(t)
                        } else {
                            operation = PendingBinaryOperationInfo(symbol: symbol, function: foo, firstOperand: temp)
                        }
					}
				case .Equals:
					if operation != nil {
						temp = operation!.function(operation!.firstOperand, temp)
						operation = nil
					}
			}
		}
    }
    
    var displayNumber: Double{
        get{
            return temp
        }
    }
 
}