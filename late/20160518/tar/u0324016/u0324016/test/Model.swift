//
//  Model.swift
//  test
//
//  Created by nkfust05 on 2016/5/18.
//  Copyright © 2016年 nkfust05. All rights reserved.
//

import Foundation
func multiply (op1: Double,op2: Double)-> Double{
    return op1 * op2
}
func plus (op1: Double,op2: Double)-> Double{
    return op1 + op2
}

func minus (op1: Double,op2: Double)-> Double{
    return op1 - op2
}

func divided (op1: Double,op2: Double)-> Double{
    return op1 / op2
}


class Model {
    private var temp = 0.0
    func setOperand(operand: Double){
        temp = operand
    }
    
    var operations: Dictionary<String,Operation> = [
        "pi" : Operation.Constant(M_PI),
        "c" : Operation.Constant(0),//M_PI
        "e":  Operation.Constant(M_E),// M_E
        "sqrt": Operation.UnaryOpetation(sqrt), //sqrt
        "cos":Operation.UnaryOpetation(cos),//cos
        "x":Operation.BinaryOperation(multiply),
        "+":Operation.BinaryOperation(plus),
        "-":Operation.BinaryOperation(minus),
        "/":Operation.BinaryOperation(divided),
        "=":Operation.Equals
    ]
    enum Operation{
        case Constant(Double)
        case UnaryOpetation((Double)->Double)
        case BinaryOperation((Double , Double)->Double)
        case Equals
    }
    
    
    func performOperation(symbol: String){
        if let operation = operations[symbol]{
            switch operation{
            case .Constant(let value) : temp = value
            case .UnaryOpetation(let function): temp = function(temp)
            case .BinaryOperation(let function): pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: temp)
            case .Equals:
                if pending != nil {
                    
                    temp = pending!.binaryFunction(pending!.firstOperand,temp)
                    pending = nil
                }
                
                
                
            }
            
            
        }
        
        
        /*switch symbol {
         case "pi": temp = M_PI
         case "sqrt": temp = sqrt(temp)
         case "c": temp = 0
         case "e": temp = M_E
         default: break
         */
        
        
    }
    private var pending: PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFunction: (Double,Double)->Double
        var firstOperand:Double
    }
    var result: Double{
        get{
            return temp
        }
    }
    
}
