//
//  TestModel.swift
//  chen
//
//  Created by 陳昱蓁 on 2016/5/18.
//  Copyright © 2016年 陳昱蓁. All rights reserved.
//

import Foundation

func multiply(op1: Double, op2: Double) -> Double{
    print("Multiply")
    return op1 * op2
}
func plus(op1: Double, op2: Double) -> Double{
    print("plus")
    return op1 + op2
}
func cut(op1: Double, op2: Double) -> Double{
    print("cut")
    return op1 - op2
}
func division(op1: Double, op2: Double) -> Double{
    print("division")
    return op1 / op2
}

class TestModel{
    private var temp = 0.0 //temp是Operand從外面傳進來更改的，所以不用public
    func setOperand(operand: Double){
         temp = operand
    }
    
    var operations: Dictionary<String, Operation> = [ //方法套用型別
     "π": Operation.Constant(M_PI),
     "e": Operation.Constant(M_E),
     "√": Operation.UnaryOperation(sqrt),
     "Cos": Operation.UnaryOperation(cos),//負號也是Unary
     "+": Operation.BinaryOperation(plus),
     "-": Operation.BinaryOperation(cut),
     "/": Operation.BinaryOperation(division),
     "x": Operation.BinaryOperation(multiply),
     "=": Operation.Equals//小數點用Binary做Equals
    ]
    
    enum Operation { //宣告型別
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
        
    }
    
    func performOperation(symbol: String){ //定義型別要幹麻
        if let operation = operations[symbol]  {
            print("operation is \(operation)")
            switch operation {
            case Operation.Constant(let value): temp = value
            case Operation.UnaryOperation(let function): temp = function(temp)
            case Operation.BinaryOperation(let function): pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: temp)
            case Operation.Equals:
                print("pending test is \(pending)")
                if pending != nil{
                temp = pending!.binaryFunction(pending!.firstOperand, temp)
                pending = nil
                }
             }
        }  }
    
    private var pending: PendingBinaryOperationInfo? //?-->可以是空值
    
    struct PendingBinaryOperationInfo { //加號的類別
        var binaryFunction: (Double, Double) -> Double
        var firstOperand: Double
    }
    var result: Double{
        get {
            print("\(temp)")
            return temp //內部updata
        }

    
    }}