//
//  File.swift
//  kuku03
//
//  Created by nkfust07 on 2016/5/18.
//  Copyright © 2016年 nkfust07. All rights reserved.
//

import Foundation

class TestModel {
    private var temp = 0.0
    func setOperand(operand: Double){
        temp = operand
    }
    func performOperation(symbol: String)
    {
        switch symbol{
            case "∏": temp = M_PI
            case "√": temp = sqrt(temp)
            case "C": temp = 0
            //case "A":
             default: break
        }
    }
    var result: Double{
        get {
        return temp
        }
}
}
