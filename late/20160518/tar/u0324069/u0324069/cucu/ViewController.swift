//
//  ViewController.swift
//  cucu
//
//  Created by nkfust10 on 2016/5/18.
//  Copyright © 2016年 nkfust10. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var display: UILabel!
    private var typing = false
    
    @IBAction func msg(sender: UIButton) {
        let msg : String = sender.currentTitle!
        print("flag : \(typing)")
        if typing{
            let textCurrentlyInDisplay = display.text!
            display.text = textCurrentlyInDisplay + msg
            print("touched \(msg) digit")
        }else{
            display.text = msg
        }
        typing = true
    }
    var displayValue: Double{
        get {
            return Double(display.text!)!
        }
        set{
            display.text = String(newValue)
        }
    }
    private var model = TestModel()
    
    
    @IBAction func ReadMyName(sender: UIButton) {
        display.text = "0324035:江秉軒"
    }
    @IBAction func TouchC(sender: UIButton) {
        if typing {
            model.setOperand(displayValue)
            typing = false
        }
        
        if let mathSymbol : String = sender.currentTitle {
            model.performOperation(mathSymbol)
        }
        displayValue = model.result
    }
}


