//
//  TestModel.swift
//  20160518
//
//  Created by nkfust12 on 2016/5/25.
//  Copyright © 2016年 nkfust12. All rights reserved.
//

import Foundation

class TestModel {
    
    private var temp = 0.0
    
    func setOperand (operand : Double){
        temp = operand
    }
    
    func performOperation(symbol : String){
        switch symbol {
            case "e": temp = M_E
            case "π": temp = M_PI
            case "√": temp = sqrt(temp)
        default: break
        }
            
    }
    var result : Double{
        get{
            return temp
        }
    }
    
}