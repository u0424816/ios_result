import Foundation

func multiply(op1: Double, op2: Double) -> Double{
    print("multiply: \(op1 * op2)");return op1 * op2
}

func substract(op1: Double, op2: Double) -> Double {
    print("Subtract: \(op1 - op2)");return op1 - op2
}

class TestModel {
    
    private var temp = 0.0

    func setOperand(operand: Double){
        temp = operand
    }
    
    var operations: Dictionary<String, Operation> = [
        "e": Operation.Constant(M_E),
        "π": Operation.Constant(M_PI),
        "√": Operation.UnaryOpetation(sqrt),
        "cos": Operation.UnaryOpetation(cos),
        "x": Operation.BinaryOperation(*),
        "-": Operation.BinaryOperation(-),
        "=": Operation.Equals
    ]
    
    enum Operation {
        case Constant(Double)
        case UnaryOpetation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
    }
    
    func performOperation(symbol: String) {
        print("symbol is \(symbol)")
        if let operation = operations[symbol]{
            print("operation is \(operation)")
            switch operation {
            case .Constant(let value):
                temp = value
            case .UnaryOpetation(let function):
                temp = function(temp)
            case .BinaryOperation(let function):
                executePendingBinaryOperation()
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: temp)
            case .Equals:
                executePendingBinaryOperation()
                }
            }
        }
    func executePendingBinaryOperation() {
        print("pending test is \(pending)")
        if pending != nil{
            print("pending is \(pending)")
            temp = pending!.binaryFunction(pending!.firstOperand, temp)
            pending = nil
        }
    }
    
private var pending: PendingBinaryOperationInfo?

struct PendingBinaryOperationInfo{
    var binaryFunction: (Double, Double) -> Double
    var firstOperand: Double
}
    var result: Double {
        get {
            print("\(temp)")
        return temp
    }
}
}