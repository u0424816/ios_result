

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var show: UILabel!
    
        var userI = false
    
    @IBAction func pi(sender: UIButton) {
        let digit = sender . currentTitle!
        if userI {
            let text = show.text!
            show.text = text + digit
            print("touched \(digit) yy")
        } else {
            show.text = digit
        }
        userI = true
}
    
    var displayValue: Double{
        get{
            return Double(show.text!)!
        }
        set {
            show.text = String(newValue)
        }
    }   
    private var model = TestModel()
    
    @IBAction func touchKeyC(sender: UIButton) {
        if userI {
            model.setOperand(displayValue)
            userI = false
        }
        if let mathSymbol = sender.currentTitle {
            model.performOperation(mathSymbol)
        }
        displayValue = model.result
 }
}



