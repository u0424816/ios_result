//
//  Model.swift
//  calculator
//
//  Created by 郭哲宇 on 5/23/16.
//  Copyright © 2016 mis. All rights reserved.
//

import Foundation

func plus(op1:Double,op2:Double) -> Double {
    return op1 + op2
}

func minus(op1:Double,op2:Double) -> Double {
    return op1 - op2
}

func multiply(op1:Double,op2:Double) -> Double {
    return op1 * op2
}

func devide(op1:Double,op2:Double) -> Double {
    if(op2==0){return 0}
    return op1 / op2
}

class Model {
    var n = "0"
    var operations: Dictionary<String,Op> = [
        "π":Op.Constant(M_PI),// M_PI
        "sqrt":Op.UnaryOp(sqrt),// sqrt
        "e":Op.Constant(M_E),// M_E
        "C":Op.Constant(0.0),// C
        "+":Op.BinaryOp(plus),// +
        "-":Op.BinaryOp(minus),// -
        "x":Op.BinaryOp(multiply),// x
        "/":Op.BinaryOp(devide),// /
        "=":Op.Equals,
        "A":Op.Name("0324074 guo")//A
    ]
    
    enum Op {
        case Constant(Double)
        case UnaryOp((Double) -> Double)
        case BinaryOp((Double,Double) -> Double)
        case Equals
        case Name(String)
    }
    
    func setDigit(num:String){
        if(Double(n) == nil){n="0"}
        if(Double(n)<=999999999){
        n=String(Int(n+num)!)
        }
    }
    
    func setOper(op:String){
        if(Double(n) == nil)
        {n="0"}
        if let operation = operations[op] {
            switch operation {
            case .Constant(let value):n=String(value)
            case .UnaryOp(let function):n=String(function(Double(n)!))
            case .BinaryOp(let function):
                if pending != nil {
                    n = String(pending!.binaryFunction(pending!.firstOperand,Double(n)!))
                    pending = nil
                }
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: Double(n)!)
                n="0"
            case .Equals:
                if pending != nil {
                    n = String(pending!.binaryFunction(pending!.firstOperand,Double(n)!))
                    pending = nil
                }
            case .Name(let value):n=value
            }
        }
    }
    private var pending:PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFunction: (Double,Double) -> Double
        var firstOperand:Double
    }
}