//
//  TestModel.swift
//  test
//
//  Created by nkfust06 on 2016/5/18.
//  Copyright © 2016年 nkfust06. All rights reserved.
//
import Foundation

func multiply(op1: Double, op2: Double) -> Double{
    print("Multiply")
    return op1 * op2
}
func plus(op1: Double, op2: Double) -> Double{
    print("Plus")
    return op1 + op2
}
func minus(op1: Double, op2: Double) -> Double{
    print("Minus")
    return op1 - op2
}
func division(op1: Double, op2: Double) -> Double{
    print("division")
    return op1 / op2
}
class TestModel{
    private var temp = 0.0 //temp是Operand從外面傳進來更改的，所以不用public
    func setOperand(operand: Double){
        temp = operand
    }
    
    var operations: Dictionary<String, Operation> = [ //方法套用型別
        "π": Operation.Constant(M_PI),
        "e": Operation.Constant(M_E),
        "√": Operation.UnaryOperation(sqrt),
        "Cos": Operation.UnaryOperation(cos),//-也是
        "x": Operation.BinaryOperation(multiply),
        "=": Operation.Equals,
        "+": Operation.BinaryOperation(plus),
        "-": Operation.BinaryOperation(minus),
        "/": Operation.BinaryOperation(division),
]
    
    enum Operation { //宣告型別
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
        
    }
    
    func performOperation(symbol: String){ //定義型別要幹麻
        if let operation = operations[symbol]  {
            print("operation is \(operation)")
            switch operation {
            case Operation.Constant(let value): temp = value
            case Operation.UnaryOperation(let function): temp = function(temp)
            case Operation.BinaryOperation(let function): pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: temp)
            case Operation.Equals:
                print("pending test is \(pending)")
                if pending != nil{
                    temp = pending!.binaryFunction(pending!.firstOperand, temp)
                    pending = nil
                }
            }
        }  }
    
    private var pending: PendingBinaryOperationInfo? //?-->可以是空值
    
    struct PendingBinaryOperationInfo { //加號的類別
        var binaryFunction: (Double, Double) -> Double
        var firstOperand: Double
    }
    var result: Double{
        get {
            print("\(temp)")
            return temp //內部updata
        }
        
        
    }}