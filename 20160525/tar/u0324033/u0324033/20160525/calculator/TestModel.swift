//
//  TestModel.swift
//  testmethodo
//
//  Created by nkfust09 on 2016/5/18.
//  Copyright © 2016年 nkfust09. All rights reserved.
//

import Foundation
func multiply(op1:Double, op2:Double, symbol:String)-> Double {
    switch symbol {
    case "+":  return op1 + op2
    case "-":  return op1 - op2
    case "*":  return op1 * op2
    case "/":  return op1 / op2
    default:
        return 0
    }
}

var numstr=""
var yn = 1
class TestModel{
    private var number = 0.0
    //private var sum = 0.0
    func setOperand(operand: Double) {
        number = operand
        print(operand)
        numstr+=String(operand)
    }
    
    var operation: Dictionary<String, Operation> = [
        "π" : Operation.Constant(M_PI),
        "e" : Operation.Constant(M_E),
        "√" : Operation.UaryOperation(sqrt),
        "cos" : Operation.UaryOperation(cos),
        "+" : Operation.BinaryOperation(multiply),
        "-" : Operation.BinaryOperation(multiply),
        "*" : Operation.BinaryOperation(multiply),
        "/" : Operation.BinaryOperation(multiply),
        "=" : Operation.Equals,
        "C" : Operation.Constant(0)
    
    ]
    
    enum Operation {
        case Constant(Double)
        case UaryOperation( (Double) -> Double)
        case BinaryOperation((Double,Double,String) -> Double)
        case Equals
    }
    
    func performOperation(symbol : String) {
        
        if let operation = operation[symbol] {
            switch operation {
            case .Constant(let value):
                number = value
                if symbol=="C" {
                    numstr=""
                }
            case .UaryOperation(let function): number = function(number)
            case .BinaryOperation(let function):
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: number,symbol: symbol)
                numstr+=symbol
            case .Equals:
                if pending != nil {
                    number = pending!.binaryFunction(pending!.firstOperand, number, pending!.symbol )
                   
                    pending = nil
                }else
                {
                    number=calculator(numstr)
                }
            }
        }
    }
    
    private var pending: PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFunction: (Double,Double,String) -> Double
        var firstOperand: Double
        var symbol : String
    }
    
    
    func calculator(numstr : String) -> Double {
        var S = Array(numstr.characters)
        var num = Array<Double!>()
        var str = Array<String!>()
        var sum : Double = 0  //Int Max = -9223372036854775808 ~ 9223372036854775807
        var x = ""
        var MD : Double
        var TF=true
        var error=false
        var negative=true
        var ascii=""
        for i in  0..<numstr.characters.count
        {
            ascii=String(S[i])
            if(S[i] == "+" || S[i] == "-" || S[i] == "*" || S[i] == "/"  )
            {
                if((S[0]=="-" || S[0]=="+" || S[0]=="*" || S[0]=="/") && negative==true )
                {
                    num.append(0)
                    negative=false
                }
                else
                {
                    num.append(Double(x))
                }
                str.append("\(S[i])")
                x=""
            }
            else if(Double(ascii)>=0 && Double(ascii)<=9 || ascii=="." )
            {
                x=x + "\(S[i])"
            }
            else
            {
                print("error")
                error=true
                break
            }
        }
        //---------------------------------------------------------------------------------------------------------
        num.append(Double(x))
        str.append("")
        sum=num[0]
        if(error==false)
        {
            for var y in 0..<str.count//有先後順序的運算
            {
                if((str[0]=="*" || str[0]=="/") && TF==true)//判斷第一個是否乘除
                {
                    if(str[0]=="*")
                    {
                        sum=sum*num[y+1]
                    }
                    else
                    {
                        sum=sum/num[y+1]
                    }
                    for(var i=1 ;i<str.count;++i)
                    {
                        if(str[y+i]=="*")
                        {
                            sum=sum*num[y+i+1]
                        }
                        else if(str[y+i]=="/")
                        {
                            sum=sum/num[y+i+1]
                        }
                        else
                        {
                            break
                        }
                    }
                    TF=false
                }
                else
                {
                    if(str[y]=="+" || str[y]=="-")//判斷加減
                    {
                        if(str[y+1]=="*" || str[y+1]=="/")//判斷下一個是否為乘or除，如果是就先做乘除
                        {
                            MD=num[y+1]
                            for(var i=1 ;i<str.count-y;++i)
                            {
                                if(str[y+i]=="*")
                                {
                                    MD=MD*num[y+i+1]
                                }
                                else if(str[y+i]=="/")
                                {
                                    MD=MD/num[y+i+1]
                                }
                                else
                                {
                                    break  
                                }
                            }
                            if(str[y]=="+")
                            {
                                sum=sum+MD
                            }
                            else if(str[y]=="-")
                            {
                                sum=sum-MD
                            }
                        }
                        else if(str[y]=="+")
                        {
                            sum=sum+num[y+1]
                        }
                        else if(str[y]=="-")
                        {
                            sum=sum-num[y+1]
                        }
                    }
                }
            } 
            
        }
        return sum
    }
    
    
    var result: Double {
        get {
            return number
        }
        
    }
}