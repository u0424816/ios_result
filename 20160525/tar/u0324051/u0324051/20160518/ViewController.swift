//
//  ViewController.swift
//  20160518
//
//  Created by nkfust12 on 2016/5/25.
//  Copyright © 2016年 nkfust12. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBOutlet weak var display: UILabel!
    
    var a = false
    var b = false
    
    var s : String! = ""
    @IBAction func showMessage(sender: UIButton){
        let msg = sender.currentTitle!
        if(display.text == "0" || display.text == "0324051徐皓韋" || !a){
            s = msg
            a = true
            b = false
        }else if msg == "." && !b
            {
                s! += msg
                b = true
        }
            
        else{
            s! += msg
        }
        display.text = s
    }
    
    @IBAction func showZero(sender: UIButton) {
        let msg = "0"
        s = msg
        display.text = s
    }
    
    @IBAction func showAbout(sender: UIButton){
        let msg : String = "0324051徐皓韋"
        s = msg
        display.text = s
    }
    
    var displayValue : Double {
        get{
            return Double(display.text!)!
        }
        set{
                display.text = String(newValue)
        }
    }
    
    private var model = TestModel()
    
    @IBAction func model(sender: UIButton) {
        a = false
        if display != "0" {
            model.setOperand(displayValue)
        }
        if let mathSymbol = sender.currentTitle {
            model.performOperation(mathSymbol)
        }
        displayValue = model.result
    }


}

