//
//  TestModel.swift
//  20160518
//
//  Created by nkfust12 on 2016/5/25.
//  Copyright © 2016年 nkfust12. All rights reserved.
//

import Foundation

func multiply(op1:Double ,op2:Double) -> Double{
    return op1 * op2
}
func division(op1:Double ,op2:Double) -> Double{
    return op1 / op2
}
func plus(op1:Double ,op2:Double) -> Double{
    return op1 + op2
}
func minus(op1:Double ,op2:Double) -> Double{
    return op1 - op2
}



class TestModel {
    
    private var temp = 0.0
    
    func setOperand(operand: Double) {
        temp = operand
    }
    
    var operatoins: Dictionary<String, Operation> = [
        "π": Operation.Constant(M_PI),
        "℮": Operation.Constant(M_E),
        "√": Operation.UnaryOpetation(sqrt),
        "cos": Operation.UnaryOpetation(cos),
        "×": Operation.BinaryOperation(multiply),
        "÷": Operation.BinaryOperation(division),
        "-": Operation.BinaryOperation(minus),
        "+": Operation.BinaryOperation(plus),
        "=": Operation.Equals,
        "±": Operation.Opposite
    ]
    
    enum Operation {
        case Constant(Double)
        case UnaryOpetation((Double) -> Double)
        case BinaryOperation((Double,Double) -> Double)
        case Equals
        case Opposite
    }
    func performOperation(symbol: String){
        if let operation = operatoins[symbol] {
            print ("\(operation)")
            switch operation {
            case .Constant(let value): temp = value
            case .UnaryOpetation(let function): temp = function(temp)
            case .BinaryOperation(let function):
                if pending != nil{
                    print ("\(temp)")
                    temp = pending!.binaryFunction(pending!.firstOperand, temp)
                    pending = nil
                }

                pending = PendingBinaryOperationInfo(binaryFunction: function,firstOperand:temp)
            case .Equals:
                if pending != nil{
                    print ("\(temp)")
                    temp = pending!.binaryFunction(pending!.firstOperand, temp)
                    pending = nil
                }
            case .Opposite:
                temp = 0 - temp
            }
        }
    }
    
    var pending: PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFunction: (Double,Double) -> Double
        var firstOperand:Double
    }
    
    var result: Double {
        get {
            print ("\(temp)")
            return temp
        }
    }
}