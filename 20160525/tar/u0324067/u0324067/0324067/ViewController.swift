//
//  ViewController.swift
//  0324067
//
//  Created by nkfust12_2 on 2016/5/18.
//  Copyright © 2016年 nkfust12_2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var f: UILabel!
    
    var a = false
    
    @IBAction func x1(sender: UIButton) {
        
        let number = sender.currentTitle!
        if f.text == "0" || f.text == "0324067廖曾泓" || !a {
                f.text! = number
                a = true
        }else{
                f.text = "\(f.text!)\(number)"
        }
    }
    
    var displayValue: Double {
        get{
            return Double(f.text!)!
        }
        set{
            f.text = String(newValue)
        }
    }
    
    private var model = TestModel()
    
    @IBAction func clear(sender: UIButton) {

        f.text = "0"
    }
    
    @IBAction func showname(sender: UIButton) {
        f.text = "0324067廖曾泓"
    }
    
    @IBAction func qaq(sender: UIButton) {
        a = false
        if f.text != "0" {
            model.setOperand(displayValue)
        }
        if let mathSymbol = sender.currentTitle{
            model.performOperation(mathSymbol)
        }
        
        displayValue = model.result
        
        
    }

    

    
    
}

