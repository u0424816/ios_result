//
//  ViewController.swift
//  20160518
//
//  Created by creatceous on 2016/5/24.
//  Copyright © 2016年 u0324004. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var model = TestModel()
    private var isFirstNumber = true
    private var hasDecimal = false
    
    @IBOutlet weak var displayformula: UILabel!
    
    
    @IBOutlet weak var display: UILabel!
    var displayValue : Double{
        get{
            return  Double(display.text!)!
        }
    }
    
    @IBAction func clickNumber(sender: UIButton) {
        let number = sender.currentTitle!
        if(display.text == "0"){
            isFirstNumber = true
        }
        if(isFirstNumber){
            hasDecimal = false
            display.text = number
            isFirstNumber = false
        }else{
            display.text = display.text! + number
        }
    }
    
    @IBAction func clicksymbol(sender: UIButton) {
        let symbol = sender.currentTitle!
        if(display.text == model.displaySTID()){
            model.performOperation("C")
        }else{
            model.setOperand(displayValue)
        }
        model.performOperation(symbol)
        isFirstNumber = true
        display.text = model.result
        if(display.text != "不能除以0"){
            if(displayValue % 1 == 0){
                hasDecimal = false
            }else{
                hasDecimal = true
            }
        }
        displayformula.text = model.formular
    }
    
    @IBAction func clickA(sender: UIButton) {
        display.text = model.displaySTID()
        isFirstNumber = true
        hasDecimal = true
    }
    
    @IBAction func clickDecimal(sender: UIButton) {
        if(hasDecimal == false){
            display.text = display.text! + "."
            hasDecimal = true
            isFirstNumber = false
        }
    }
}

