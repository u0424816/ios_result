//
//  TestModel.swift
//  20160518
//
//  Created by creatceous on 2016/5/24.
//  Copyright © 2016年 u0324004. All rights reserved.
//

import Foundation


func positive_negative(number : Double) -> Double{
    return number * -1;
}
func multiply(op1: Double , op2 : Double) -> Double{
    return op1 * op2
}
func subtraction(op1: Double , op2 : Double) -> Double{
    return op1 - op2
}
func addition(op1: Double , op2 : Double) -> Double{
    return op1 + op2
}
func except(op1: Double , op2 : Double) -> Double{
    return op1 / op2
}

class TestModel{

    enum Operation{
        case Constant(Double)
        case UnaryOpetation((Double) -> Double)
        case BinaryOperation((Double , Double) -> Double)
        case Equals
        case Clear
    }
    
    struct PendingBinaryOperationInfo {
        var binaryFunction : (Double, Double) -> Double
        var firstOperand: Double
    }
    private var pending : PendingBinaryOperationInfo?
    private var tmp = 0.0
    var formular = ""
    var result : String{
        get{
            if(String(tmp) == "inf" || String(tmp) == "nan"){
                performOperation("C")
                return "不能除以0"
            }else{
                if(tmp % 1 == 0){
                    return String(Int(tmp))
                }else{
                    return String(tmp)
                }
            }
        }
    }

    var ishead : Bool = true
    var operations : Dictionary<String, Operation> = [
        "C" : Operation.Clear,
        "e" : Operation.Constant(M_E),
        "PI" : Operation.Constant(M_PI),
        "sqr" : Operation.UnaryOpetation(sqrt), //sqrt
        "cos" : Operation.UnaryOpetation(cos), // cos
        "x" : Operation.BinaryOperation(multiply),
        "=" : Operation.Equals,
        "+/-" : Operation.UnaryOpetation(positive_negative),
        "+" : Operation.BinaryOperation(addition),
        "-" : Operation.BinaryOperation(subtraction),
        "/" : Operation.BinaryOperation(except)
    ]
    
    func setOperand(operand : Double){
        tmp = operand
    }
    
    func performOperation(symbol : String){
        if let operation = operations[symbol]{
            switch operation{
            case Operation.Constant(let value): tmp = value
            case Operation.UnaryOpetation(let function): tmp = function(tmp)
            case Operation.BinaryOperation(let function) :
                formular = formular + result + symbol
                if(ishead == false){
                    if pending != nil{
                        tmp = pending!.binaryFunction(pending!.firstOperand, tmp)
                        pending = nil
                        ishead = true
                    }
                }
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand : tmp)
                ishead = false
            case Operation.Equals:
                if pending != nil{
                    formular = formular + result
                    formular = ""
                    tmp = pending!.binaryFunction(pending!.firstOperand, tmp)
                    pending = nil
                    ishead = true
                }
            case Operation.Clear :
                tmp = 0.0
                formular = ""
                pending = nil
                ishead = true
            }
            
            
        }
    }
    

    func displaySTID() -> String{
        return "0324004 : 李婉瑄"
    }
}
