//
//  TestModel.swift
//  calculator
//
//  Created by nkfust11 on 2016/5/18.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import Foundation
func operate(first:Double,second:Double,symbol: String) -> Double {
    switch symbol {
    case "+":
        return first + second
    case "-":
        return first - second
    case "×":
        return first * second
    default:
        return first / second
    }
}
func change(value:Double) -> Double{
    return value * (-1)
}
func setZero(value:Double) -> Double{
    return 0
}
class TestModel{
    private var temp : Double = 0
    private var name :String = "0324087 : 凃翰穎"
    private var total :String = ""
    private var together = clacul()
    func setOperand(operand: Double){
        temp=operand
        total += String(operand)
    }
    var operatons: Dictionary<String,operation> = [
        "兀": operation.Constant(M_PI),
        "e": operation.Constant(M_E),
        "±": operation.UaryOperation(change),
        "C": operation.UaryOperation(setZero),
        "√": operation.UaryOperation(sqrt),
        "+": operation.BinaryOperation(operate),
        "-": operation.BinaryOperation(operate),
        "×": operation.BinaryOperation(operate),
        "÷": operation.BinaryOperation(operate),
        "=": operation.Equals
    ]
    enum operation {
        case Constant(Double)
        case UaryOperation((Double) -> Double)
        case BinaryOperation((Double,Double,String) -> Double)
        case Equals
    }
    func performOperation(symbols: String){
        if let operation = operatons[symbols] {
            switch operation {
            case .Constant(let value): temp = value
            case .UaryOperation(let function): temp = function(temp)
            case .BinaryOperation(let function):
                if pending != nil {
                    temp = pending!.binaryFunction(pending!.firstOperand,temp,pending!.symbol)
                }
                pending = PendingBinaryOperationInfo(binaryFunction: function , firstOperand: temp,symbol: symbols)
                total += symbols
            case .Equals:
                if pending != nil {
                    temp = pending!.binaryFunction(pending!.firstOperand,temp,pending!.symbol)
                    pending = nil
                }else{
                    if (total != "" && total != String(temp)){
                        temp = together.setnumber(total)
                        total = String(temp)
                    }
                }
            }
        }
    }
    private var pending : PendingBinaryOperationInfo?
    struct PendingBinaryOperationInfo{
        var binaryFunction:((Double,Double,String) -> Double)
        var firstOperand:Double
        var symbol:String
    }
    var result:Double{
        get{
            return temp
        }
    }
}
