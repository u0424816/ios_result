//
//  Model.swift
//  Calculator
//
//  Created by Dien-Ju, Lin on 5/24/16.
//  Copyright © 2016 Dien-Ju, Lin. All rights reserved.
//

import Foundation

class Model{
    
    private var temp: Double = 0.0
	
    func setNumber(n: Double){
        temp = n
    }
	
	enum Op {
		case Constant(Double)
		case UnaryOperation((Double) -> Double)
		case BinaryOperation((Double, Double) -> Double)
		case Equals
	}
	
	struct PendingBinaryOperationInfo {
        var symbol: String
		var function: (Double, Double) -> Double
		var firstOperand: Double
	}
	
	var operation: PendingBinaryOperationInfo?
	
	var ops: Dictionary<String, Op> = [
		"π": Op.Constant(M_PI),
		"Exp": Op.Constant(M_E),
		"√": Op.UnaryOperation(sqrt),
		"Cos": Op.UnaryOperation(cos),
		"+": Op.BinaryOperation(+),
		"-": Op.BinaryOperation(-),
		"×": Op.BinaryOperation(*),
		"÷": Op.BinaryOperation(/),
		"=": Op.Equals
		
	]
	
    func setOp(symbol: String){
		if let op = ops[symbol] {
			switch op {
				case .Constant(let value):
					temp = value
				case .UnaryOperation(let foo):
					temp = foo(temp)
				case .BinaryOperation(let foo):
					if operation == nil {
                        operation = PendingBinaryOperationInfo(symbol: symbol, function: foo, firstOperand: temp)
					} else {
                        if (operation!.symbol == symbol){
                            let t = foo(operation!.firstOperand, temp)
                            operation?.firstOperand = t
                            print(t)
                        } else {
                            operation = PendingBinaryOperationInfo(symbol: symbol, function: foo, firstOperand: temp)
                        }
					}
				case .Equals:
					if operation != nil {
						temp = operation!.function(operation!.firstOperand, temp)
						operation = nil
					}
			}
		}
    }
    
    var displayNumber: Double{
        get{
            return temp
        }
    }
 
}