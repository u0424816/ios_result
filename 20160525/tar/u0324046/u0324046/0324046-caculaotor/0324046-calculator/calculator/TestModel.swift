//
//  TestModel.swift
//  calculator
//
//  Created by nkfust11 on 2016/5/18.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import Foundation
func multiply(op1: Double, op2: Double) -> Double {
  return op1 * op2
}
func add(op1: Double, op2: Double) -> Double {
    return op1 + op2
}

func gin(op1: Double, op2: Double) -> Double {
    return op1 - op2
}
func tru(op1: Double, op2: Double) -> Double {
    return op1 / op2
}



class TestModel{
    private var name :String = "0324046 - 徐琨淋"
    private var value : String="0"
    //private var temp : String="0"
    private var stringtype = false
    private var temp = 0.0
    
    var operations : Dictionary<String, Operation> = [
        "兀" : Operation.Constant(M_PI),
        "e"  : Operation.Constant(M_E),
        "√" : Operation.UnaryOpeation(sqrt),
        "cos" : Operation.UnaryOpeation(cos),
        "*" : Operation.BinaryOperation(multiply),
        "+" : Operation.BinaryOperation(add),
        "-" : Operation.BinaryOperation(gin),
        "/" : Operation.BinaryOperation(tru),
        "=" : Operation.Equals,
        "C" : Operation.Clean
    ]
    
    enum Operation{
        case Constant(Double)
        case UnaryOpeation((Double) -> Double)
        case BinaryOperation((Double,Double) -> Double)
        case Equals
        case Clean
    }
    
    func setOperand(operand: Double){
        temp = operand
    }
    
    func performOperation(symbol:String){
    
        if let operation = operations[symbol]{
            switch operation{
            case .Constant(let value): temp = value
            case .UnaryOpeation(let function): temp = function(temp)
            case . BinaryOperation (let function):pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand : temp)
               temp = 0.0
            case . Equals :
                if pending != nil{
              
                temp = pending!.binaryFunction(pending!.firstOperand, temp)
                    pending = nil
                }
            case .Clean: temp = 0
            }
        
        }
    
    
    }
    private var pending: PendingBinaryOperationInfo?
    
    struct  PendingBinaryOperationInfo {
        var binaryFunction: (Double,Double) -> Double
        var firstOperand: Double
    }

    var result:String{
        get{
            return (stringtype ? name: String(temp))
        }
    }
}
