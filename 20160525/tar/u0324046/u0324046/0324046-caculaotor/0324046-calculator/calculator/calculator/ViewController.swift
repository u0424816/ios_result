//
//  ViewController.swift
//  calculator
//
//  Created by nkfust11 on 2016/5/11.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var display: UILabel!
    var values:String{
        get{
            return display.text!//回傳字串        
        }
        set{
            display.text=String(newValue)
            totalnumber=String(newValue)
        
        
        }
    }
    private var model=TestModel()
    var totalnumber:String = "0"
    @IBAction func NumberPress(sender: UIButton) {
        let number:String = String(sender.currentTitle!)
        
        if(totalnumber=="0" || totalnumber == "0.0"){
            totalnumber=""
        }
        totalnumber+=number
        values=totalnumber
        
    }
    @IBAction func touchKey(sender: UIButton) {
        model.setOperand(Double(values)!)
        if let opp = sender.currentTitle {
            model.performOperation(String(opp))
        }
        values=model.result
}
    
    
}

