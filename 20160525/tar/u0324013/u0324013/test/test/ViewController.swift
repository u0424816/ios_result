//
//  ViewController.swift
//  test
//
//  Created by nkfust04 on 2016/5/4.
//  Copyright © 2016年 nkfust04. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var flag = false
    var CaculateId = ""
    var priviousNum = 0.0
    
    private var model = TestModel()
    
    
    
    var displayValue: Double {
        get {
            return Double(printLabel.text!)!
        }
        set {
            printLabel.text = String(newValue)
        }
        
        
    }
    
    
    @IBAction func Caculate(sender: UIButton) {
        //let buttonId = sender.currentTitle!
        
        if flag == true {
            model.setOperator(displayValue)
            flag = false
        }
        
        if let mathSymbol = sender.currentTitle{
            
            model.performOperation(mathSymbol)
        }
        
        displayValue = model.result
        
        /*
         flag = true
         
         switch buttonId{
         case "+" :
         CaculateId = "+"
         case "-" :
         CaculateId = "-"
         case "*" :
         CaculateId = "*"
         case "/" :
         CaculateId = "/"
         default :
         break
         }
         */
        
    }
    /*
    @IBAction func Result(sender: UIButton) {
        var ans = 0.0
        let currentNum = Double(printLabel.text!)!
        
        switch CaculateId{
        case "+":
            ans = priviousNum + currentNum
        case "-":
            ans = priviousNum - currentNum
        case "*":
            ans = priviousNum * currentNum
        case "/":
            ans = priviousNum / currentNum
        default:
            break
            
        }
        
        
        printLabel.text = String(ans)
        flag = true
        
    }
 */
    @IBAction func showMessage(sender: UIButton) {
        let msg = sender.currentTitle!
        if flag{
            printLabel.text! += msg
        }else if printLabel.text == "0" && msg == "0"{
            printLabel.text = "0"
        }else{
            printLabel.text = msg
            flag = true
        }
        
        if(msg == "C"){
            printLabel.text = "0"
            flag = false
        }else if (msg == "A"){
            printLabel.text = "0324013 薛傳穎"
            flag = false
        }
        
        
    }
    @IBOutlet weak var printLabel: UILabel!
    
}

