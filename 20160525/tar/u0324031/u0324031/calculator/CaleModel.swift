//
//  CaleModel.swift
//  calculator
//
//  Created by nkfust08 on 2016/5/18.
//  Copyright © 2016年 nkfust. All rights reserved.
//

import Foundation

func multiply(op1: Double, op2 :Double) -> Double {
    return op1 * op2
}
func add(op1: Double, op2 :Double) -> Double {
    return op1 + op2
}
func subtract(op1: Double, op2 :Double) -> Double {
    return op1 - op2
}
func divide(op1: Double, op2 :Double) -> Double {
    if op2 != 0{
        return op1 / op2
    }else{
        print("can not divide zero!")
        return 0
    }
}
func sign(operation: Double) -> Double {
    return -(operation)
}


func calcResult(calsStr: [String]) -> Double{
    print(calsStr)
    var Result: [String] = calsStr
    var index = 0
    while(Result.indexOf("X") != nil || Result.indexOf("/") != nil){
        switch Result[index] {
            case "X":
                Result[index] = String(Double(Result[index-1])! * Double(Result[index+1])!)
                Result.removeAtIndex(index-1)
                Result.removeAtIndex(index)
                index = index-1
            case "/":
                Result[index] = String(Double(Result[index-1])! / Double(Result[index+1])!)
                Result.removeAtIndex(index-1)
                Result.removeAtIndex(index)
                index = index-1
            default:
                break
        }
        index = index+1
    }
    index = 0
    while(Result.indexOf("+") != nil || Result.indexOf("-") != nil){
        switch Result[index] {
            case "+":
                Result[index] = String(Double(Result[index-1])! + Double(Result[index+1])!)
                Result.removeAtIndex(index-1)
                Result.removeAtIndex(index)
                index = index-1
            case "-":
                Result[index] = String(Double(Result[index-1])! - Double(Result[index+1])!)
                Result.removeAtIndex(index-1)
                Result.removeAtIndex(index)
                index = index-1
            default:
                break
        }
        index = index+1
    }
    return Double(Result[0])!
}

class CaleModel {
    
    private var calsStr: [String]=[]
    private var tmp = 0.0
    private var pending: PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFuncion: (Double, Double) -> Double
        var firstOperator: Double
    }
    
    func setDigit(digit: Double){
        tmp = digit
    }
    
    enum Operation {
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals([String] -> Double)
    }
    
    var operations: Dictionary<String, Operation> = [
        "π": Operation.Constant(M_PI),
        "℮": Operation.Constant(M_E) ,
        "C": Operation.Constant(0),
        "√": Operation.UnaryOperation(sqrt),
        "cos": Operation.UnaryOperation(cos),
        "±": Operation.UnaryOperation(sign),
        "+": Operation.BinaryOperation(add),
        "-": Operation.BinaryOperation(subtract),
        "X": Operation.BinaryOperation(multiply),
        "/": Operation.BinaryOperation(divide),
        "=": Operation.Equals(calcResult)
    ]
    
    func perforOperation(symbol: String){
        if let operation = operations[symbol]{
            calsStr.append(String(tmp))
            switch operation {
            case .Constant(let value): tmp = value
            case .UnaryOperation(let function): tmp = function(tmp)
            case .BinaryOperation(let function): 
                if pending == nil{
                    pending = PendingBinaryOperationInfo(binaryFuncion: function, firstOperator: tmp)
                }else{
                    tmp = pending!.binaryFuncion(pending!.firstOperator, tmp)
                    pending = nil
                    pending = PendingBinaryOperationInfo(binaryFuncion: function, firstOperator: tmp)
                }
                
            case .Equals(let function):
                print("\(calsStr)")
                tmp = function(calsStr)
                /*
                if pending != nil {
                    tmp = pending!.binaryFuncion(pending!.firstOperator, tmp)
                    pending = nil
                }
                */
                
            }
            calsStr.append(symbol)
            print(calsStr)
        }
        
        /*
        switch symbol {
        case "C": tmp = 0
        case "π": tmp = M_PI
        case "√": tmp = sqrt(tmp)
        case "℮": tmp = M_E
        default: break
        }
        */
    }
    var result: Double {
        get {
            return tmp
        }
    }
    
}