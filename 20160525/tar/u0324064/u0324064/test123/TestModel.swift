//
//  File.swift
//  test123
//
//  Created by nkfust02_2 on 2016/5/18.
//  Copyright © 2016年 nkfust02_2. All rights reserved.
//

import Foundation

func mutiply/* 乘 */ (op1:Double , op2:Double) -> Double{
    return op1 * op2
}
func add/* + */ (op1:Double , op2:Double) -> Double{
    return op1 + op2
}
func dec/* - */ (op1:Double , op2:Double) -> Double{
    return op1 - op2
}
func exc/* / */ (op1:Double , op2:Double) -> Double{
    return op1 / op2
}

func yyy/* / */ (op1:Double) -> Double{
    return op1 * (-1)
}


class TestModel{
    private var temp = 0.0
    
    
    func setOperand(operand:Double){
        temp = operand
    }
    
   
    
    var operations : Dictionary <String , Operation> = [
        "根"  : Operation.UnaryOpetation(sqrt), //sqrt,
        "cos" : Operation.UnaryOpetation(cos),// cos,
        "sin" : Operation.UnaryOpetation(sin),
        "E"   : Operation.Constant(M_E),
        "拍"  : Operation.Constant(M_PI),
        "*"   : Operation.BinaryOperation(mutiply),
        "/"   : Operation.BinaryOperation(exc),
        "+"   : Operation.BinaryOperation(add),
        "-"   : Operation.BinaryOperation(dec),
        "="   : Operation.Equals,
        "+/-"  : Operation.UnaryOpetation(yyy),    ]
    
    enum Operation{
        case Constant(Double)
        case UnaryOpetation(Double -> Double) //丟ＤＯＵＢＬＥ進來 丟ＤＯＵＢＬＥ出去
        case BinaryOperation((Double , Double) -> Double)
        case Equals
    }
    
    func performOperation(symbol:String){
        if let operation = operations[symbol]{
            switch operation {
            case .Constant(let value): temp = value
            case .UnaryOpetation(let fun): temp = fun(temp)
            case .BinaryOperation(let fun):
                if pending != nil{
                    temp = pending!.binaryFunction((pending?.firstOperand)!,temp)
                    pending = nil
                }
                pending = PendingBinaryOperationInfo(binaryFunction:fun , firstOperand : temp)
                
          
            case .Equals:
                if pending != nil{
                    temp = pending!.binaryFunction((pending?.firstOperand)!,temp)
                    pending = nil
                }
            }
        }
    }
    private var pending : PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo{
        var binaryFunction : (Double , Double) -> Double
        var firstOperand: Double
    }
    
    
  /*  func performOperation(symbol: String){
        switch symbol{
        case "拍" : temp = M_PI
        case "根" : temp = sqrt(temp)
        case "E"  : temp = M_E
        default:break
        }
    
    }&*/
    
    
    
    var result : Double{
        get{
             return temp
        }
    }
}