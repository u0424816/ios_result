//
//  TestModel.swift
//  calculator
//
//  Created by nkfust11 on 2016/5/18.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import Foundation
func change(value:Double) -> Double{
    return value * (-1)
}

func Clear() -> Double{
    return 0.0
}

func allClear() -> (Double,String){
    return (0.0,"")
}

func toPercent(value:Double) -> Double{
    return value/100
}

func toSin(value:Double) -> Double{
    var val = value % 360
    if(val < 0 ){
        val += 360
    }
    return sin(value * M_PI / 180.0)
}
func M_MR(value:Double) -> Double {
    return value
}
let factorial = Factorial().compute
class TestModel{
    private var number :Double = 0
    private var numbercount:Int = 0
    private var operationcount:Int = 0
    private var Mnumber :Double = 0
    private var name :String = ""
    private var total :String = ""
    private var together = clacul()
    func setOperand(operand:Double){
        number = operand
        total += String(number)
        numbercount += 1
    }
    func allClear(){
        number = 0.0
        total = ""
        name = ""
        numbercount = 0
        operationcount = 0
        pending = nil
    }
    var operatons: Dictionary<String,operation> = [
        "兀": operation.Constant(M_PI),
        "e": operation.Constant(M_E),
        "C": operation.Constant(Clear()),
        "MR": operation.UaryOperation(M_MR),
        "±": operation.UaryOperation(change),
        "sin": operation.UaryOperation(toSin),
        "%": operation.UaryOperation(toPercent),
        "n!": operation.UaryOperation(factorial),
        "AC": operation.AllClear(),
        "√": operation.UaryOperation(sqrt),
        "+": operation.BinaryOperation(+),
        "-": operation.BinaryOperation(-),
        "×": operation.BinaryOperation(*),
        "÷": operation.BinaryOperation(/),
        "M+": operation.Mad(+),
        "M-": operation.Mad(-),
        "MC": operation.Constant(Clear()),
        "A": operation.SetName("0324087 : 凃翰穎"),
        "=": operation.Equals
    ]
    enum operation {
        case Constant(Double)
        case UaryOperation((Double) -> Double)
        case BinaryOperation((Double,Double) -> Double)
        case AllClear()
        case Mad((Double,Double) -> Double)
        case SetName(String)
        case Equals
    }
    func performOperation(symbols: String){
        if(name != ""){
            name = "異常：無法計算"
        }
        if let operation = operatons[symbols] {
            switch operation {
            case .Constant(let value):
                if(symbols == "MC"){
                    Mnumber = value
                }else{
                    number = value
                }
            case .UaryOperation(let function):
                number = function(( symbols == "MR") ? Mnumber : number)
            case .BinaryOperation(let function):
                if pending != nil {
                    number = pending!.binaryFunction(pending!.firstOperand,number)
                }
                pending = PendingBinaryOperationInfo(binaryFunction: function , firstOperand: number)
                if(total == ""){
                    total = "0"
                    numbercount = 1
                }
                total += symbols
                operationcount += 1
            case .AllClear():allClear()
            case .Mad(let function): Mnumber = function(Mnumber,number)
            case .SetName(let str): name = str
            case .Equals:
                if pending != nil {
                    number = pending!.binaryFunction(pending!.firstOperand,number)
                    pending = nil
                }else{
                    if (total != "" && total != String(number) && (numbercount-operationcount) == 1){
                        number = together.setnumber(total)
                        total = String(number)
                        numbercount = 1
                        operationcount = 0
                    }
                }
            }
        }
    }
    private var pending : PendingBinaryOperationInfo?
    struct PendingBinaryOperationInfo{
        var binaryFunction:((Double,Double) -> Double)
        var firstOperand:Double
    }
    var result:(Double,String){
        get{
            return (number,name)
        }
    }
}
