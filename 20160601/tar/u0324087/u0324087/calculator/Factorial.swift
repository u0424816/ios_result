//
//  Factorial.swift
//  calculator
//
//  Created by 凃翰穎 on 2016/6/7.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import Foundation

class Factorial{
    var first : [Int] = [1]
    var second : Int = 0
    func multiply(){
        for i in 0..<first.count{
            first[i] *= second
        }
        for i in 1..<first.count{
            first[first.count - i - 1] += first[first.count - i] / 10
            first[first.count-i] %= 10
        }
        while(first[0] >= 10){
            first.insert(first[0] / 10 , atIndex:0)
            first[1] %= 10
        }
    }
    func compute(value:Double) -> Double {
        first = [1]
        second = 0
        var str = ""
        let n : Int = Int(value)
        for i in 0...n{
            if(i>1){
                second = i
                multiply()
            }
        }
        for j in first{
            str+=String(j)
        }
        return Double(str)!
    }
}