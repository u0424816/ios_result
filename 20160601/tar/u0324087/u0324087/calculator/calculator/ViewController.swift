//
//  ViewController.swift
//  calculator
//
//  Created by nkfust11 on 2016/5/11.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var display: UILabel!
    var entering = false
    var dot = false
    var values:(Double,String){
        get{
            return (Double(display.text!)!,display.text!)
        }
        set{
            if(newValue.1 != ""){
                display.text = newValue.1
            }else{
                display.text = String(newValue.0)
            }
             
        }
    }
    private var model = TestModel()
    
    @IBAction func NumberPress(sender: UIButton) {
        let number:String = String(sender.currentTitle!)
        if entering{
            if number != "."{
                display.text = display.text! + number
            }else{
                if !dot{
                    display.text = display.text! + number
                    dot = true
                }
            }
        }else{
            if number != "."{
                dot = false
                display.text = number
            }else{
                display.text = "0" + number
            }
        }
        entering = true
    }
    @IBAction func touchKey(sender: UIButton) {
        if entering{
            entering = false
            model.setOperand(values.0)
        }
        
        if let opp = sender.currentTitle {
            model.performOperation(String(opp))
        }
        
        values = model.result
        dot = true
    }
    }

