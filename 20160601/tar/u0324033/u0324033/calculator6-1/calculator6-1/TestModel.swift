//
//  TestModel.swift
//  testmethodo
//
//  Created by nkfust09 on 2016/5/18.
//  Copyright © 2016年 nkfust09. All rights reserved.
//

import Foundation

func M_plus(op:Double) -> Double {
    m_num+=op
    print("\(m_num)")
    return 0
}
func M_subtract(op:Double) -> Double {
    m_num-=op
    return 0
}

func MR(op:Double) -> Double {
    number.num=m_num
    return 0
}

func MC(op:Double) -> Double {
    m_num=0
    return 0
}

func ac(op : Double) -> Double {
    var ans=String(op)
    ans.removeAtIndex(ans.endIndex.predecessor())
    return Double(ans)!
    
}

func sign(op : Double) -> Double {
    return op * -1
}

func percent(op : Double) -> Double {
    return op/100
}

func  stratum(op : Double) -> Double {
    
    var c : [Int]=[1]
    var s :String=""
    for x in 0...Int(op){
        for i in 0..<c.count{
            c[i] = c[i] * (x==0 ? 1 : x)
        }
        for(var i = c.count-1 ; i>0 ;--i){ //進位判斷
            c[i-1] += c[i] / 10
            c[i] %= 10
        }
        while(c[0]>=10){ //最大位數進位判斷
            c.insert(c[0]/10 , atIndex:0)
            c[1] %= 10
        }
    }
    for i in 0..<c.count{ //列印
        s+=String(c[i])
    }
    return Double(s)!
}

func x(op :Double) ->Double {
    let count=String(number.num).characters.count
    print(count)
    
    if number.num != 0 && number.num != 0.0
    {
        let range=numstr.endIndex.advancedBy(-count) ..< numstr.endIndex
       numstr.removeRange(range)
    }
    
    return 0.0
}


func dot(op : Double) -> Double {
    
    return Double(String(Int(op))+".")!
    
}
var number=(num: 0.0 , name:"")
//var str=""
var numstr=""
var yn = 1
//var number = 0.0
var m_num=0.0
class TestModel{
    //private var sum = 0.0
    func setOperand(operand: Double) {
        number.num = operand
        print(operand)
        numstr+=String(operand)
    }
    
    var operation: Dictionary<String, Operation> = [
        "π" : Operation.Constant(M_PI),
        "e" : Operation.Constant(M_E),
        "√" : Operation.UaryOperation(sqrt),
        "sin" : Operation.UaryOperation(sin),
        "cos" : Operation.UaryOperation(cos),
        "tan" : Operation.UaryOperation(tan),
        "+" : Operation.BinaryOperation(+),
        "-" : Operation.BinaryOperation(-),
        "*" : Operation.BinaryOperation(*),
        "/" : Operation.BinaryOperation(/),
        "M+" : Operation.M_math(M_plus),
        "M-" : Operation.M_math(M_subtract),
        "MC" : Operation.M_math(MC),
        "MR" : Operation.M_math(MR),
        "=" : Operation.Equals,
        "C" : Operation.UaryOperation(x),
        "A" : Operation.Strtext("翁崇軒 0324033"),//未完成
        "%" : Operation.UaryOperation(percent),
        "n!" : Operation.UaryOperation(stratum),
        "+/-" : Operation.UaryOperation(sign),
        "AC" : Operation.Constant(0),
        "." : Operation.UaryOperation(dot),
    
    ]
    
    enum Operation {
        case Strtext(String)
        case Constant(Double)
        case M_math((Double)->Double)
        case UaryOperation( (Double) -> Double)
        case BinaryOperation((Double,Double) -> Double)
        case Equals
    }
    
    func performOperation(symbol : String) {
        
        if let operation = operation[symbol] {
            switch operation {
            case .Strtext(let str): number.name=str
            case .Constant(let value):
                number.num = value
                numstr+=String(number.num)
                print("\(numstr)")
                if symbol=="AC" {
                    numstr=""
                    
                    pending=nil
                }
            case .M_math(let function): function(number.num)
            case .UaryOperation(let function): number.num = function(number.num)
            case .BinaryOperation(let function):

                if pending != nil {
                    number.num = pending!.binaryFunction(pending!.firstOperand, number.num)
                    print(".")
                }
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: number.num)
                
                numstr+=symbol
            case .Equals:
                if pending != nil {
                    number.num = pending!.binaryFunction(pending!.firstOperand, number.num)
                    pending = nil
                }
                else
                {   if numstr != ""//不是空的再算
                    {
                        number.num=calculator(numstr)
                    }
                }
            }
        }
    }
    
    private var pending: PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFunction: (Double,Double) -> Double
        var firstOperand: Double
        
    }
    
    
    func calculator(numstr : String) -> Double {
        var S = Array(numstr.characters)
        var num = Array<Double!>()
        var str = Array<String!>()
        var sum : Double = 0  //Int Max = -9223372036854775808 ~ 9223372036854775807
        var x = ""
        var MD : Double
        var TF=true
        var error=false
        var negative=true
        var ascii=""
        for i in  0..<numstr.characters.count
        {
            ascii=String(S[i])
            if(S[i] == "+" || S[i] == "-" || S[i] == "*" || S[i] == "/"  )
            {
                if((S[0]=="-" || S[0]=="+" || S[0]=="*" || S[0]=="/") && negative==true )
                {
                    num.append(0)
                    negative=false
                }
                else
                {
                    num.append(Double(x))
                }
                str.append("\(S[i])")
                x=""
            }
            else if(Double(ascii)>=0 && Double(ascii)<=9 || ascii=="." )
            {
                x=x + "\(S[i])"
            }
            else
            {
                print("error")
                error=true
                break
            }
        }
        //---------------------------------------------------------------------------------------------------------
        num.append(Double(x))
        str.append("")
        sum=num[0]
        if(error==false)
        {
            for var y in 0..<str.count//有先後順序的運算
            {
                if((str[0]=="*" || str[0]=="/") && TF==true)//判斷第一個是否乘除
                {
                    if(str[0]=="*")
                    {
                        sum=sum*num[y+1]
                    }
                    else
                    {
                        sum=sum/num[y+1]
                    }
                    for(var i=1 ;i<str.count;++i)
                    {
                        if(str[y+i]=="*")
                        {
                            sum=sum*num[y+i+1]
                        }
                        else if(str[y+i]=="/")
                        {
                            sum=sum/num[y+i+1]
                        }
                        else
                        {
                            break
                        }
                    }
                    TF=false
                }
                else
                {
                    if(str[y]=="+" || str[y]=="-")//判斷加減
                    {
                        if(str[y+1]=="*" || str[y+1]=="/")//判斷下一個是否為乘or除，如果是就先做乘除
                        {
                            MD=num[y+1]
                            for(var i=1 ;i<str.count-y;++i)
                            {
                                if(str[y+i]=="*")
                                {
                                    MD=MD*num[y+i+1]
                                }
                                else if(str[y+i]=="/")
                                {
                                    MD=MD/num[y+i+1]
                                }
                                else
                                {
                                    break  
                                }
                            }
                            if(str[y]=="+")
                            {
                                sum=sum+MD
                            }
                            else if(str[y]=="-")
                            {
                                sum=sum-MD
                            }
                        }
                        else if(str[y]=="+")
                        {
                            sum=sum+num[y+1]
                        }
                        else if(str[y]=="-")
                        {
                            sum=sum-num[y+1]
                        }
                    }
                }
            } 
            
        }
        return sum
    }
    
    
    var result:Double {
        get {
            return number.num
        }
        
    }
 
}