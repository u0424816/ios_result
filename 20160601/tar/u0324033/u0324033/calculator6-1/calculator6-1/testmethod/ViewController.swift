//
//  ViewController.swift
//  testmethod
//
//  Created by nkfust09 on 2016/5/11.
//  Copyright © 2016年 nkfust09. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var temp="0"
    var use = false
    var dot = false
    @IBOutlet weak var display: UILabel!
    
    
    @IBAction func showMessage(sender: UIButton) {
 
        if !(sender.currentTitle=="." && dot==true)//判斷'.'是否存在
        {
            
            
            var digit = sender.currentTitle!
            if use {//判斷是不是輸入中
                let textCurrent = display.text!
                display.text = textCurrent + digit
                
            }else {
                if sender.currentTitle=="."
                {
                    digit="0."
                }
                display.text=digit
            }
            use=true
            
        }
        if sender.currentTitle=="."//判斷'.'存在
        {
            dot=true
        }
        
    }
    
    var displayValue: Double{
        get {
            return Double(display.text!)!
        }
        set {
            display.text=String(newValue)
        }
    }
    
    private var model = TestModel()
    
    @IBAction func math(sender: UIButton) {
        temp=""
        dot=false
        if use == true
        {
            model.setOperand(displayValue)
            use=false
        }
        if let mathSymbol = sender.currentTitle {
            model.performOperation(mathSymbol)
            
        }
        displayValue = model.result
        
    }
}

