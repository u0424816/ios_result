//
//  TestModel.swift
//  CalculatorBy59
//
//  Created by 王書睿 on 2016/6/7.
//  Copyright © 2016年 王書睿. All rights reserved.
//

import Foundation

func plus(op1: Double, op2: Double) -> Double
{
    return op1 + op2
}
func minus(op1: Double, op2: Double) -> Double
{
    return op1 - op2
}
func multiply(op1: Double, op2: Double) -> Double
{
    return op1 * op2
}
func divide(op1: Double, op2: Double) -> Double
{
    return op1 / op2
}
func Switch(op1: Double) -> Double
{
    return 0 - op1
}
func stratum(op1: Double) -> Double
{
    var sum = 1
    for a in 1...Int(op1)
    {
        sum *= a
    }
    return Double(sum)
}
func percent(op1: Double) -> Double
{
    return op1 / 100
}
var EqualCount = -1

class TestModel{
    
    private var number = 0.0
    var EqualTwice : Array<String>= []
    private var twice = DoubleEquals()
    
    
    func setOperand(operand: Double) {
        number = operand
        EqualTwice.append("\(Int(operand))")
        EqualCount += 1
    }
    
    var operations : Dictionary<String, Operation> =
        [
            "e": Operation.Constant(M_E),  //M_E
            "π": Operation.Constant(M_PI),  //M_PI
            "C": Operation.Constant(0.0),
            "√": Operation.UnaryOperation(sqrt),  //sqrt
            "Sin": Operation.UnaryOperation(sin),  //cos
            "%": Operation.UnaryOperation(percent),
            "±": Operation.UnaryOperation(Switch),
            "n!": Operation.UnaryOperation(stratum),
            "+": Operation.BinaryOperation(plus),//plus
            "-": Operation.BinaryOperation(minus),
            "*": Operation.BinaryOperation(multiply),
            "/": Operation.BinaryOperation(divide),
            "=": Operation.Equals,
            "AC": Operation.Clean,
            
            ]
    
    enum Operation
    {
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
        case Clean
        
    }
    
    func performOperation(symbol : String)
    {
        if(symbol == "+" || symbol == "-" || symbol == "*" || symbol == "/")
        {
            EqualTwice.append(symbol)
            EqualCount += 1
        }
        
        if(symbol == "C")
        {
          EqualTwice.removeAtIndex(EqualCount)
          EqualCount-1
        }
        
        if let operation = operations[symbol]
        {
            switch operation
            {
            case .Constant(let value): number = value;
            case .UnaryOperation(let function): number = function(number)
            case .BinaryOperation(let function):
                if pending != nil
                {
                    number = pending!.binaryFuction(pending!.firstOperand, number)
                    pending = nil
                }
                pending = PendingBinaryOperationInfo(binaryFuction: function, firstOperand: number)
            
            case .Equals:
                if pending != nil
                {
                    number = pending!.binaryFuction(pending!.firstOperand, number)
                    pending = nil
                }
                else
                {
                    if(EqualTwice != [])
                    {
                        number = twice.FirstAndSecond(EqualTwice)
                        EqualTwice = []
                    }
                                    }
            case .Clean: number = 0; pending = nil; EqualTwice = []
            }
        }
    }
    
    var pending: PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo
    {
        var binaryFuction: (Double, Double) -> Double
        var firstOperand: Double
    }
    
    var result: Double {
        get
        {
            return number
        }
        
    }
}