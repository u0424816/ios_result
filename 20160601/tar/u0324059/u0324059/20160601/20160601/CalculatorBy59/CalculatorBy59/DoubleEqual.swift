//
//  DoubleEqual.swift
//  CalculatorBy59
//
//  Created by 王書睿 on 2016/6/7.
//  Copyright © 2016年 王書睿. All rights reserved.
//

import Foundation


class DoubleEquals
{
    func FirstAndSecond(Str : [String]) -> Double
    {
        var INput : Array<String> = []
        for c in 0...Str.count-1
        {
            INput.append("\(Str[c])")
        }
        var sum : Double = 0
        var stack : Double = 0
        
        
        for a in 0...INput.count-1
        {
            if(a<INput.count)
            {
                if(INput[a] == "*" || INput[a] == "/")
                {
                    switch(INput[a])
                    {
                        case "*":
                            INput[a-1] = "\(Double(INput[a-1])! * (Double(INput[a+1])!))"
                            INput.removeAtIndex(a+1)
                            INput.removeAtIndex(a)
                            a-1
                        case "/":
                            INput[a-1] = "\(Double(INput[a-1])! / (Double(INput[a+1])!))"
                            INput.removeAtIndex(a+1)
                            INput.removeAtIndex(a)
                            a-1
                        default:
                            break
                    }
                } 
            }
            
        }
        
        if(INput[0] != "+" && INput[0] != "*" && INput[0] != "/")
        {
            if(INput[0] == "-")
            {
                sum -= Double(INput[1])!
                INput.removeAtIndex(0)
            }
            else
            {
                sum = Double(INput[0])!
            }
        }
        
        for b in 1..<INput.count
        {
            switch(INput[b])
            {
            case "+":
                stack = Double(INput[b+1])!
                sum += stack
                stack = 0
            case "-":
                stack = Double(INput[b+1])!
                sum -= stack
                stack = 0
            default :
                break
            }
        }
        return sum
    }
}