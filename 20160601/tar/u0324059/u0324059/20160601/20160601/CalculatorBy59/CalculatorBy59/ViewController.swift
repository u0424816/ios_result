//
//  ViewController.swift
//  CalculatorBy59
//
//  Created by 王書睿 on 2016/6/7.
//  Copyright © 2016年 王書睿. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var display: UILabel!
    
    var userIsInTheMiddleOfTyping = false
    var PointOnlyCanUseOneTime = true
    
    @IBAction func showMessage(sender: UIButton)
    {
        let digit = sender.currentTitle!
        
        if userIsInTheMiddleOfTyping
        {
            if(digit == "." && PointOnlyCanUseOneTime)
            {
                display.text = display.text! + digit
                PointOnlyCanUseOneTime = false
            }
            else if(digit != ".")
            {
                display.text = display.text! + digit
            }
        }
        else
        {
            if(digit == ".")
            {
                display.text = "0."
                PointOnlyCanUseOneTime = false
            }
            else
            {
                display.text = digit
            }
        }
        userIsInTheMiddleOfTyping = true
    }
    
    var displayValue: Double{
        get {
            return Double(display.text!)!
        }
        set {
            display.text=String(newValue)
        }
    }
    
    private var model = TestModel()
    
    @IBAction func math(sender: UIButton)
    {
        if userIsInTheMiddleOfTyping
        {
            model.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
            PointOnlyCanUseOneTime = true
        }
        if let mathematicalSymbol = sender.currentTitle
        {
            model.performOperation(mathematicalSymbol)
        }
        displayValue = model.result
    }
}

