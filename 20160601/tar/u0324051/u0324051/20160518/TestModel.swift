//
//  TestModel.swift
//  20160518
//
//  Created by nkfust12 on 2016/5/25.
//  Copyright © 2016年 nkfust12. All rights reserved.
//

import Foundation


class TestModel {
    
    private var temp = 0.0
    
    func setOperand(operand: Double) {
        temp = operand
    }
    
    var operatoins: Dictionary<String, Operation> = [
        "π": Operation.Constant(M_PI),
        "℮": Operation.Constant(M_E),
        "√": Operation.UnaryOperation(sqrt),
        "sin": Operation.UnaryOperation(sin),
        "cos": Operation.UnaryOperation(cos),
        "%": Operation.UnaryOperation({$0 * 0.01}),
        "×": Operation.BinaryOperation(*),
        "÷": Operation.BinaryOperation(/),
        "-": Operation.BinaryOperation(-),
        "+": Operation.BinaryOperation(+),
        "=": Operation.Equals,
        "±": Operation.UnaryOperation({0.0 - $0}),
        "x!": Operation.Level
    ]
    
    enum Operation {
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double,Double) -> Double)
        case Equals
        case Level
    }
    func performOperation(symbol: String){
        if let operation = operatoins[symbol] {
            print ("\(operation)")
            switch operation {
            case .Constant(let value): temp = value
            case .UnaryOperation(let function): temp = function(temp)
            case .BinaryOperation(let function):
                if pending != nil{
                    print ("\(temp)")
                    temp = pending!.binaryFunction(pending!.firstOperand, temp)
                    pending = nil
                }

                pending = PendingBinaryOperationInfo(binaryFunction: function,firstOperand:temp)
            case .Equals:
                if pending != nil{
                    print ("\(temp)")
                    temp = pending!.binaryFunction(pending!.firstOperand, temp)
                    pending = nil
                }
            case .Level:
                var i = 1
                
                while(temp > 1){
                    i *= Int(temp)
                    temp -= 1
                }
                temp = Double(i)
            default: break
            }
        }
    }
    
    var pending: PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFunction: (Double,Double) -> Double
        var firstOperand:Double
    }
    
    var result: Double {
        get {
            print ("\(temp)")
            return temp
        }
    }
}