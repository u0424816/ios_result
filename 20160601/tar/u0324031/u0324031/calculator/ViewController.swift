//
//  ViewController.swift
//  calculator
//
//  Created by nkfust08 on 2016/5/11.
//  Copyright © 2016年 nkfust. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var display: UILabel!
    
    var entering = false
    
    @IBAction func showMessage(sender: UIButton) {
        let digit = sender.currentTitle!
        if entering {
            let textCurrentInDisplay :String
            if display.text! == "0" {
                textCurrentInDisplay = ""
            }else{
                textCurrentInDisplay = display.text!
            }
            display.text = textCurrentInDisplay + digit
            print(display.text)
        }else{
            display.text = digit
        }
        entering = true
    }
    var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            display.text = String(newValue)
        }
    }
    private var model  = CaleModel()
    
    @IBAction func keyTouchspci(sender: UIButton) {
        if entering {
            print(displayValue)
            model.setDigit(displayValue)
            entering = false
        }
        if let mathSymbol = sender.currentTitle {
            model.perforOperation(mathSymbol)
        }
        displayValue = model.result
    }
    
    @IBAction func touchKeyA(sender: AnyObject) {
        display.text = "0324031廖家生"
    }
    
    
}



