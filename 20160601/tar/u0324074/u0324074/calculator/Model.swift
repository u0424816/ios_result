//
//  Model.swift
//  calculator
//
//  Created by 郭哲宇 on 5/23/16.
//  Copyright © 2016 mis. All rights reserved.
//

import Foundation

class Model {
    var n = "0"
    var n2 = "0"
    var click = 0
    var operations: Dictionary<String,Op> = [
        "π":Op.Constant(M_PI),// M_PI
        "sqrt":Op.UnaryOp(sqrt),// sqrt
        "e":Op.Constant(M_E),// M_E
        "C":Op.Constant(0.0),// C
        "+":Op.BinaryOp(+),// +
        "-":Op.BinaryOp(-),// -
        "*":Op.BinaryOp(*),// x
        "/":Op.BinaryOp(/),// /
        "=":Op.Equals,
        "AC":Op.Name("0324074 guo")//A
    ]
    
    enum Op {
        case Constant(Double)
        case UnaryOp((Double) -> Double)
        case BinaryOp((Double,Double) -> Double)
        case Equals
        case Name(String)
    }
    
    func setDigit(num:String){
        if(Double(n) == nil){n="0"}
            if(Double(n)!%1==0){
                n=String(Int(n+num)!)
            }else{
                n=String(Double(n+num)!)
            }
    }
    
    func setOper(op:String){
        if(Double(n) == nil)
        {n="0"}
        if let operation = operations[op] {
            switch operation {
            case .Constant(let value):n=String(value)
            case .UnaryOp(let function):n=String(function(Double(n)!))
            case .BinaryOp(let function):
                n2+=n+op
                if pending != nil {
                    n = String(pending!.binaryFunction(pending!.firstOperand,Double(n)!))
                    pending = nil
                }
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: Double(n)!)
                n="0"
                click=0
            case .Equals:
                click+=1
                if(click==1){n2+=n}
                if pending != nil {
                    if(click==2){
                        n=calc(n2)
                        n2="0"
                        pending = nil
                    }else if(click==1){
                        n = String(pending!.binaryFunction(pending!.firstOperand,Double(n)!))
                    }
                }
            case .Name(let value):n=value;n2="0"
            }
        }
        
        print(n2)
    }
    private var pending:PendingBinaryOperationInfo?
    
    struct PendingBinaryOperationInfo {
        var binaryFunction: (Double,Double) -> Double
        var firstOperand:Double
    }
        func split(input : String) -> [String]{
        var tempNum = ""
        var splitArray = [String]()
        for index in input.characters {
            if(index != "+" && index != "-" && index != "*" && index != "/" && index != "(" && index != ")"){
                tempNum+=String(index)
            }else{
                if(tempNum != ""){
                    splitArray.append(tempNum)
                    tempNum = ""
                }
                splitArray.append(String(index))
            }
        }
        splitArray.append(tempNum)
        return splitArray
    }
    func toPostfix(input : [String]) -> [String]{
        var postfixArray = [String]()
        var operatorTemp = [String]()
        for index in input {
            if(index == ")"){
                while(operatorTemp.count-1>=0 && operatorTemp[operatorTemp.count-1] != "(") {
                    postfixArray.append(operatorTemp.removeAtIndex(operatorTemp.count-1))
                }
                operatorTemp.removeAtIndex(operatorTemp.count-1)
            }else if(index == "("){
                operatorTemp.append(index)
            }else if(index == "+" || index == "-"){
                while( operatorTemp.count-1>=0 && operatorTemp[operatorTemp.count-1] != "("){
                    postfixArray.append(operatorTemp.removeAtIndex(operatorTemp.count-1))
                }
                
                operatorTemp.append(index)
            }else if(index == "*" || index == "/"){
                while(operatorTemp.count >= 1 && (operatorTemp[operatorTemp.count-1] == "*" || operatorTemp[operatorTemp.count-1] == "/")){
                    postfixArray.append(operatorTemp.removeAtIndex(operatorTemp.count-1))
                }
                operatorTemp.append(index)
            }else{
                postfixArray.append(index)
            }
        }
        while(!operatorTemp.isEmpty){
            postfixArray.append(operatorTemp.removeAtIndex(operatorTemp.count-1))
        }
        return postfixArray
    }
    func postfixCalc(input : [String]) -> [String] {
        var result = input
        var doubleN1 = 0.0
        var doubleN2 = 0.0
        var location = 0
        var temp3th = ""
        var isError2 = false
        while(result.count != 1){
            doubleN1=Double(result[location])!
            doubleN2=Double(result[location+1])!
            temp3th = String(result[location+2])
            switch temp3th {
            case "+" :
                result[location]=String(doubleN1 + doubleN2)
                result.removeAtIndex(location+2)
                result.removeAtIndex(location+1)
                location = 0
                break
            case "-" :
                result[location]=String(doubleN1 - doubleN2)
                result.removeAtIndex(location+2)
                result.removeAtIndex(location+1)
                location = 0
                break
            case "*" :
                result[location]=String(doubleN1 * doubleN2)
                result.removeAtIndex(location+2)
                result.removeAtIndex(location+1)
                location = 0
                break
            case "/" :
                if(doubleN2 == 0 ){
                    isError2 = true
                    break
                }
                result[location]=String(doubleN1 / doubleN2)
                result.removeAtIndex(location+2)
                result.removeAtIndex(location+1)
                location = 0
                break
            default :
                location+=1
            }
            if(isError2){
                result[0] = "error"
                break
            }
        }
        return result
    }
    func calc (num:String) -> String {
    return postfixCalc(toPostfix(split(num)))[0]
    }
}