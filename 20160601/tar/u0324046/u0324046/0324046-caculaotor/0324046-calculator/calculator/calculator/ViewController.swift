//
//  ViewController.swift
//  calculator
//
//  Created by nkfust11 on 2016/5/11.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var userIsInTheMiddleOfTyping = false
    var PointOnlyCanUseOneTime = true
    @IBOutlet weak var display: UILabel!
   
    var displayvalue:Double{
        get{
            return Double(display.text!)!//回傳字串
        }
        set{
            display.text=String(newValue)
        
        }
    }
    private var model=TestModel()
  
   
    @IBAction func touchDigit(sender: UIButton) {
        let digit = sender.currentTitle!
  
        if userIsInTheMiddleOfTyping{
       let textCurrentlyInDisplay = display.text!
            display.text = textCurrentlyInDisplay + digit
        
    }
        else {
        display.text = digit
        }
        userIsInTheMiddleOfTyping=true
 
}

    @IBAction func performOprtation(sender: UIButton) {
        if userIsInTheMiddleOfTyping{
            model.setOperand(displayvalue)
            userIsInTheMiddleOfTyping=false
            PointOnlyCanUseOneTime = true
        }
        if let mathematicalSymbol = sender.currentTitle{
            model.performOperation(mathematicalSymbol)
        }
        displayvalue = model.result
        
        
        
}
}
