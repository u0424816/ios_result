//
//  TestModel.swift
//  calculator
//
//  Created by nkfust11 on 2016/5/18.
//  Copyright © 2016年 nkfust11. All rights reserved.
//

import Foundation

func multiply(op1: Double, op2: Double) -> Double {
  return op1 * op2
}
func add(op1: Double, op2: Double) -> Double {
    return op1 + op2
}

func gin(op1: Double, op2: Double) -> Double {
    return op1 - op2
}
func tru(op1: Double, op2: Double) -> Double {
    return op1 / op2
}

func factorial(num: Double) ->Double{
     let a = Int(num)
    var number : [Int] = [1]
    var str : String = ""
    for x in 0...a{
        for i in 0..<number.count{
            number[i] = number[i] * ( x == 0 ? 1 : x)
        }
        for(var i = number.count-1 ; i > 0 ; i-=1) {
            number[i-1] += number[i] / 10
            number[i] %= 10
        }
       
        
       
    }
    for i in 0...number.count-1{
            str += String(number[i])
        }
  return Double(str)!
    
   }
func sindegree(degree: Double) -> Double{

 return sin(degree*M_PI/180.0)

}
/*

func newmath (strInput: String) -> Double{
    
    var everycharacters = Array(strInput.characters)//每個字元切割
    var opera = Array<String>() //運算子
    var numInt = Array<Int>()  //運算元
    var numStr  = ""              //兩位數以上串接字串
    var operfirst = Array<Int>()
   
    
    
    for char in everycharacters { //運算元跟運算子分別存在不同陣列
        switch (char) {
        case "+","-","*","/" :
            
            opera.append(String(char))
       
            if (char == "*" || char == "/"){
                operfirst.insert(opera.count-1,atIndex:0)
            }
            numStr=""
            
        default :
            numStr += String(char)
        }
      
       
        numInt.append(Int(numStr)!)
        
        
    }
   
 
    var correct = true
    
    for  index in operfirst{
        switch(opera[index]) {
        case "*" :
            numInt[index] *= numInt[index+1]
        case "/" :
            if numInt[index+1] == 0{
                
                correct = false
            }
            else{numInt[index] /= numInt[index+1]
                
                correct = true
            }
        default :
            break
        }
        numInt.removeAtIndex(index+1)
        opera.removeAtIndex(index)
    }
    
    for var index = 0 ; index < opera.count ; ++index {
        switch(opera[index]) {
        case "+" :
            numInt[0] += numInt[index+1]
        case "-" :
            numInt[0] -= numInt[index+1]
            
        default :
            break
        }
    }

return Double(numInt[0])
}*/


class TestModel{
    private var name :String = "0324046 - 徐琨淋"
    private var value : String="0"
    private var stringtype = false
    private var temp = 0.0
    private var number = ""
    
    var operations : Dictionary<String, Operation> = [
        "兀" : Operation.Constant(M_PI),
        "e"  : Operation.Constant(M_E),
        "√" : Operation.UnaryOpeation(sqrt),
        "sin" : Operation.UnaryOpeation(sindegree),
        "n!" : Operation.UnaryOpeation(factorial),
        "*" : Operation.BinaryOperation(multiply),
        "+" : Operation.BinaryOperation(add),
        "-" : Operation.BinaryOperation(gin),
        "/" : Operation.BinaryOperation(tru),
        "=" : Operation.Equals,
        "C" : Operation.Constant(0.0),
        "AC": Operation.Clean
    ]
    
    enum Operation{
        case Constant(Double)
        case UnaryOpeation((Double) -> Double)  //丟過去一個值 回傳一個
        case BinaryOperation((Double,Double) -> Double)
        case Equals
        case Clean
    }
    var mathsentence : String = ""
    func setOperand(operand: Double){
        
        temp = operand
        mathsentence += "\(Int(operand))"
         print(mathsentence)
    }

    func performOperation(symbol:String){
    
        if let operation = operations[symbol]{
            switch operation{
            case .Constant(let value): temp = value
            case .UnaryOpeation(let function): temp = function(temp)
                print(symbol)
            case . BinaryOperation (let function):
                if pending != nil{
                    temp = pending!.binaryFunction(pending!.firstOperand, temp)
                    pending = nil
                }
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand : temp)
            mathsentence += symbol
                print(mathsentence)
            //temp = 0.0
            case . Equals :
                if pending != nil{
                temp = pending!.binaryFunction(pending!.firstOperand, temp)
                pending = nil
                }
                else{
                
            //   temp = newmath(mathsentence)
                
                
                }
            case .Clean: temp = 0;pending=nil;mathsentence = ""
                
            
            }
        
        }
    
    }
    private var pending: PendingBinaryOperationInfo?
    
    struct  PendingBinaryOperationInfo {
        var binaryFunction: (Double,Double) -> Double
        var firstOperand: Double
        
    }

    var result:Double{
        get{
            return temp
        }
    }
}
