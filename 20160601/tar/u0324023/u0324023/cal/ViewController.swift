//
//  ViewController.swift
//  cal
//
//  Created by nkfust08_2 on 2016/5/11.
//  Copyright © 2016年 nkfust08_2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var display: UILabel!
    
    var nowinput = false
    
    @IBAction func button1(sender: UIButton) {
        let digit = sender.currentTitle!
        if nowinput{
            let textCurrentInDisplay = display.text!
            display.text = textCurrentInDisplay + digit
            print("touched \(digit)")
            
            /*if msg == "C"{
                display.text = ""
            }
            if msg == "A"{
                display.text = "0324023 劉明憲"
            }*/
        }else{
            display.text = digit
        }
        nowinput = true
    }
    var displayValue: Double{
        get{
            return Double(display.text!)!
        }
        set{
            display.text = String(newValue)
        }
    }
    private var model = TestModel()
    @IBAction func touchKeyC(sender:UIButton){
        if nowinput{
        
        model.setOperand(displayValue)
            nowinput = false
        }
        if let mathSymbol = sender.currentTitle{
            model.performOperation(mathSymbol)

        }
        displayValue = model.result
    }
    
}

