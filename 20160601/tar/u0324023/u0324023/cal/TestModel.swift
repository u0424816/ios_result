//
//  TestModel.swift
//  cal
//
//  Created by nkfust08_2 on 2016/5/18.
//  Copyright © 2016年 nkfust08_2. All rights reserved.
//

import Foundation

func multiply(op1: Double, op2: Double)->Double{
    return op1 * op2
}
func add(op1: Double, op2: Double)->Double{
    return op1 + op2
}
func subtract(op1: Double,op2: Double)->Double{
    return op1 - op2
}
func divide(op1:Double,op2: Double)->Double{
    return op1/op2
}
func trans(op1: Double)-> Double{
        return op1*(-1)
    
}

class TestModel{
    private var temp = 0.0
    func setOperand(operand: Double){
        temp = operand
    }
    var operations:Dictionary<String,Operation>=[
        "π":Operation.Constant(M_PI),
        "ℯ":Operation.Constant(M_E),
        "C":Operation.Constant(0),
        "√":Operation.UnaryOperation(sqrt),
        "cos":Operation.UnaryOperation(cos),
        "±":Operation.UnaryOperation(trans),
        "+":Operation.BinaryOperation(add),
        "-":Operation.BinaryOperation(subtract),
        "x":Operation.BinaryOperation(multiply),
        "/":Operation.BinaryOperation(divide),
        "=":Operation.Equals

    ]
    enum Operation{
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double,Double)->Double)
        case Equals
    }
    private var pending : PendingBinaryOperationInfo?
    struct PendingBinaryOperationInfo {
        var binaryFunction :((Double,Double)->Double)
        var firstOperand : Double
    }
    func performOperation(symbol: String){
        /*switch symbol{
            case "π":temp = M_PI
            case "√":temp = sqrt(temp)
            case "C":temp = 0
            case "ℯ":temp = M_E
            default:break
        }*/
        if let operation = operations[symbol]{
            switch operation {
            case .Constant(let value): temp = value
            case .UnaryOperation(let function): temp = function(temp)
            case .BinaryOperation(let function):
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: temp)
            case .Equals:
                if pending != nil{
                    temp = pending!.binaryFunction(pending!.firstOperand,temp)
                }
            }
        }
    }
    
    
    var result:Double{
        get{
            return temp
        }
    }
}
